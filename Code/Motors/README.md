# Motor

This is the code for the Motors' controller PCB.

Copyright (C) 2020  Técnico Solar Boat

This repository and its contents  is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.
The content of this repository is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/.
You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
or via our facebook page at https://fb.com/tecnico.solarboat


⚠️ **NEVER CONNECT TWO DIFFERENT VESCs WITH SERIAL TO THE SAME MICROCONTROLLER, YOU SHOULD USE COMM_FORWARD_CAN**

This is the code for the TSB dual motor controller powered by two [VESC 75V/300A](https://www.trampaboards.com/single-vesc-75v-300a-in-cnc-t6-silicone-sealed-aluminum-box--the-most-powerful-vedder-electronic-speed-controller-ever-p-26284.html).

This code uses the VescUart [Library](https://github.com/RollingGecko/VescUartControl) by RollingGecko. This library have been modified in order to be able to control two motors and also to make it possible to use the COMM_GET_VALUES_SELECTIVE command. It was last updated to work with the VESC firmware version 3.62. 

The following files are derectlly forked from the [VESC Firmware](https://github.com/vedderb/bldc/tree/dfc3ed23196d8ba8d0c8b345edfb8a862da3fa48)
*  datatypes.h (This file is really useful to understand some of the VESC configurations)
*  crc.cpp
*  crc.h
*  buffer.cpp (can_tsb library)
*  buffer.h   (can_tsb library)


## Details about the VESC UART communicatation protocol

It is a uint8_t byte stream.

```
First byte 0x02 for payload length <= 255 bytes >> next byte is for the payload length 

First byte 0x03 for payload length > 255 bytes  >> next 2 byte for the payload length

The following 2 bytes after the payload are the checksum. (see crc.h)

The byte stream it terminated with a 0x03.
```

## Details about VescUart::getVescValuesSelective
The function accounts for the lenght that the response message should have, in case the MASK from COMM_GET_VALUES_SELECTIVE gets changed de following line of code should be updated accordingly:

```if (lenPayload == 77 || lenPayload == 29)``` 

The 29 is related to the specific mask (defined at Config.h) that is being used and the 77 is related to the ```VescUart::getVescValues```command, that outputs all the data from the VESC. In case the VESC are updated to a newer version that send more data this may also need to be updated.



## Details about COMM_GET_VALUES_SELECTIVE

This command lets one choose which data the VESC outputs.

The structure of the sent command shoud be ```{ COMM_GET_VALUES_SELECTIVE, (uint32_t)mask}```.

The mask is a 32 bit unsigned integer in which the less significant bit corresponds to the first data sent by the VESC which is temp_fet_filtered.

The structer of all the data sent by the VESC can be seen in the following [LINK](https://github.com/vedderb/bldc/blob/dfc3ed23196d8ba8d0c8b345edfb8a862da3fa48/commands.c#L250)

## Details about COMM_FORWARD_CAN
This mode enables a master VESC (connected to the micro via Serial) to communicate with other VESCs over a CAN network.

To use it just append to the begging of the sent command ```{COMM_FORWARD_CAN , SLAVE_VESC_CAN_ID}``` and the the rest of the command should have the normal structure.

Settings needed App Settings > General
Master VESC:

![Master Configuration](images/master_config.png)


Slave VESC:

![Slave Configuration](images/slave_config.png)
