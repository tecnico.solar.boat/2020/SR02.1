/*
    Copyright (C) 2020  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/


#include <Arduino.h>
#include "VescUart.h"
#include "datatypes.h"
#include "Config.h"
#include "mctrl_torquedo.h"
#include "Datalogger_TSB.h"
#include "PID_v1.h"


int time_flag=0;
int reverse_flag=0;
bool killswitch_flag = true;
bool dualMotorStartFlag = false;
float motor_current = 0;

//Define Variables for the speed PID
double PIDdesiredspeed, PIDactualspeed, PIDcurrent;
float MAXIMUM_SPEED = 6.5;
float maximum_current = MAXIMUM_CURRENT_SINGLE;

IntervalTimer Motor_timer;
IntervalTimer Blink;
IntervalTimer vesc_datatimer;
IntervalTimer log_timer;

TSB_CAN tsb_can;

// Log related stuff
TSB_SD SdCard(SPI_SCK, SD_CS_PIN); // For Teensy 3.2 SPI_SCK and SD_CS_PIN must be specified, SCK defaults to 13, SD_CD_PIN defaults to SS
TSB_Datalogger Motor_File("Motor",SdCard.sd_insert); // This is specifies the system name, it will be the prefix of the file name

VescUart M1_UART;

TSB_TORQUEDO MCTRL_THROTTLE_class;

//Create the PID class
PID speedPID(&PIDactualspeed, &PIDcurrent, &PIDdesiredspeed,Kp,Ki,Kd, DIRECT);

long rpmM1 = 0, rpmM2 = 0;

void send_blink(){
    noInterrupts();
    digitalWriteFast(LED_BUILTIN, !digitalReadFast(LED_BUILTIN));
    interrupts();
}



void logData(){
  	Motor_File.writeToFile(String(M1_UART.motor1_data.inpVoltage) + ";" + String(M1_UART.motor1_data.NTC_TEMP_MOS_MAX) + ";"
	+ String(M1_UART.motor1_data.NTC_TEMP_MOS1) + ";" + String(M1_UART.motor1_data.NTC_TEMP_MOS2) + ";"
	+ String(M1_UART.motor1_data.NTC_TEMP_MOS3) + ";" + String(M1_UART.motor1_data.temp_motor_filtered) + ";"
	+ String(M1_UART.motor1_data.avgMotorCurrent) + ";" + String(M1_UART.motor1_data.avgInputCurrent) + ";"
	+ String(M1_UART.motor1_data.rpm/5) + ";" + String(M1_UART.motor1_data.dutyCycleNow) + ";"
	+ String(M1_UART.motor1_data.faultCode) + ";" + String(M1_UART.motor1_data.id) + ";"
    + String(M1_UART.motor2_data.inpVoltage) + ";" + String(M1_UART.motor2_data.NTC_TEMP_MOS_MAX) + ";"
	+ String(M1_UART.motor2_data.NTC_TEMP_MOS1) + ";" + String(M1_UART.motor2_data.NTC_TEMP_MOS2) + ";"
	+ String(M1_UART.motor2_data.NTC_TEMP_MOS3) + ";" + String(M1_UART.motor2_data.temp_motor_filtered) + ";"
	+ String(M1_UART.motor2_data.avgMotorCurrent) + ";" + String(M1_UART.motor2_data.avgInputCurrent) + ";"
	+ String(M1_UART.motor2_data.rpm/5) + ";" + String(M1_UART.motor2_data.dutyCycleNow) + ";"
	+ String(M1_UART.motor2_data.faultCode) + ";" + String(M1_UART.motor2_data.id) + ";" 
    + String(motor_current) + ";" + String(tsb_can.screen.current_threshold) + ";" 
    + String(PIDdesiredspeed) + ";" + String(PIDactualspeed) + ";" + String(PIDcurrent));
}

void get_vescs_data(){
    noInterrupts();
    if(!killswitch_flag){
        M1_UART.getVescValuesSelective(1, COMM_GET_VALUES_SELECTIVE_MASK);
        delayMicroseconds(100);
        M1_UART.getVescValuesSelective(2, COMM_GET_VALUES_SELECTIVE_MASK);
    }
    interrupts();
}

void kill_Switch_handler(){
    noInterrupts();
    if(!digitalReadFast(VESC_PIN)){
        killswitch_flag = true; //Kill Switch have benn removed (VESCs are powered down)
        vesc_datatimer.end(); // Stop trying to get data from VESCs
    }
    else{
        vesc_datatimer.begin(get_vescs_data,(MSG_TIME-0.01)*1000000); // VESCs were powered on GET DATA
    }
    interrupts();
}

void dual_Motor_alg(float current){
    if (rpmM2 < rpmM1-100 && dualMotorStartFlag == false){
        M1_UART.setCurrent(current,1);
        M1_UART.setCurrent(CURRENT_INIT,2);
    } else{
        dualMotorStartFlag = true;
        // transition phase - motor 1 stays at threshold value, motor 2 increases with asked_current-threshold as new increment over CURRENT INIT
        if ((current - tsb_can.screen.current_threshold + CURRENT_INIT) < tsb_can.screen.current_threshold){
            M1_UART.setCurrent(tsb_can.screen.current_threshold,1);
            M1_UART.setCurrent(current - tsb_can.screen.current_threshold + CURRENT_INIT,2);
        } 
        // when motor 2 reaches threshold both motors divide power equally
        else{
            M1_UART.setCurrent(current/2,1);
            M1_UART.setCurrent(current/2,2);
        }
    }
}

void setup() {
    // General pins stuff
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWriteFast(LED_BUILTIN, HIGH);

    Serial.begin(9600);

    // VESCs Stuff
    M1_UART.setDebugPort(&Serial);
    M1_UART.setSerialPort(&MOTOR1);
	MOTOR1.begin(115200);
    pinMode(VESC_PIN, INPUT_PULLDOWN); // Pin to sense if VESCs have been turned off. USE the 3.3V OUTPUT and put 1kOhm in series.
    attachInterrupt(VESC_PIN, kill_Switch_handler, CHANGE); // Attach a change interrup to VESC_PIN

    //set resolution of analogRead for motor temperature readings
    analogReadResolution(12);
    analogReadAveraging(16);

    // Throttle stuff
    RS485SERIAL.begin(19200); //set rs485 throttle serial
    pinMode(DIRPIN, OUTPUT);
    //Prepare throttle
    MCTRL_THROTTLE_class.PrepareThrottleRequest();

    //CAN begin
    Can0.begin(1000000);
    //Handler for CAN communication
    Can0.attachObj(&tsb_can);
    tsb_can.attachGeneralHandler();
    // Init CAN messages
    init_messages();

    // Log inicializations
    Motor_File.initLog("Motor;Time;Date;M1_input_voltage;M1_temp_mos_max;M1_temp_mos_1;M1_temp_mos_2;M1_temp_mos_3;M1_temp_motor; \
M1_current_motor;M1_current_in;M1_rpm;M1_duty_cycle;M1_fault_code;M1_vesc_id;M2_input_voltage;M2_temp_mos_max;M2_temp_mos_1;M2_temp_mos_2;M2_temp_mos_3;M2_temp_motor; \
M2_current_motor;M2_current_in;M2_rpm;M2_duty_cycle;M2_fault_code;M2_vesc_id;asked_current;dual_motor_threshold;PIDdesiredspeed;PIDactualspeed;PIDcurrent");

    // CAN Messages data
    Motor_timer.begin(send_messages, MSG_TIME*1000000);

    //GET VESCS data
    vesc_datatimer.begin(get_vescs_data,(MSG_TIME-0.01)*1000000);

    // Start logging
	log_timer.begin(logData, 0.1*1e6);

    //prepare and start PID
    speedPID.SetOutputLimits(0,MAXIMUM_CURRENT_SINGLE);
    speedPID.SetSampleTime(100); //sample time in ms
    speedPID.SetMode(AUTOMATIC);

    // Serial.println("Reason for last Reset: ");

    // if (RCM_SRS1 & RCM_SRS1_SACKERR)   Serial.println("Stop Mode Acknowledge Error Reset");
    // if (RCM_SRS1 & RCM_SRS1_MDM_AP)    Serial.println("MDM-AP Reset");
    // if (RCM_SRS1 & RCM_SRS1_SW)        Serial.println("Software Reset");                   // reboot with SCB_AIRCR = 0x05FA0004
    // if (RCM_SRS1 & RCM_SRS1_LOCKUP)    Serial.println("Core Lockup Event Reset");
    // if (RCM_SRS0 & RCM_SRS0_POR)       Serial.println("Power-on Reset");                   // removed / applied power
    // if (RCM_SRS0 & RCM_SRS0_PIN)       Serial.println("External Pin Reset");               // Reboot with software download
    // if (RCM_SRS0 & RCM_SRS0_WDOG)      Serial.println("Watchdog(COP) Reset");              // WDT timed out
    // if (RCM_SRS0 & RCM_SRS0_LOC)       Serial.println("Loss of External Clock Reset");
    // if (RCM_SRS0 & RCM_SRS0_LOL)       Serial.println("Loss of Lock in PLL Reset");
    // if (RCM_SRS0 & RCM_SRS0_LVD)       Serial.println("Low-voltage Detect Reset");
    // Serial.println();

    // Setup completed star blinking
    Blink.priority(255);
    Blink.begin(send_blink, 1*1000000);

    // // Setup WDT
    // noInterrupts();                                         // don't allow interrupts while setting up WDOG
    // WDOG_UNLOCK = WDOG_UNLOCK_SEQ1;                         // unlock access to WDOG registers
    // WDOG_UNLOCK = WDOG_UNLOCK_SEQ2;
    // delayMicroseconds(1);                                   // Need to wait a bit..

    // // we will use 10 second WDT timeout (e.g. you must reset it in < 10 sec or a boot occurs)
    // WDOG_TOVALH = 0x044A;
    // WDOG_TOVALL = 0xA200;

    // // This sets prescale clock so that the watchdog timer ticks at 7.2MHz
    // WDOG_PRESC  = 0x400;

    // // Set options to enable WDT. You must always do this as a SINGLE write to WDOG_CTRLH
    // WDOG_STCTRLH |= WDOG_STCTRLH_ALLOWUPDATE |
    //     WDOG_STCTRLH_WDOGEN | WDOG_STCTRLH_WAITEN |
    //     WDOG_STCTRLH_STOPEN | WDOG_STCTRLH_CLKSRC;
    // interrupts(); 
}
	
void loop() {

    //read throttle
    motor_current = MCTRL_THROTTLE_class.getDesiredCurrent()*maximum_current/1000.0;
    if(motor_current <= 0) reverse_flag = 1; else reverse_flag = 0;

    //check for dual motor state
    if ( motor_current < tsb_can.screen.current_threshold ){
        dualMotorStartFlag = false;
    }

    // Compute PID limits based on current_threshold
    if (tsb_can.screen.current_threshold > MAXIMUM_CURRENT_SINGLE)
    {
        speedPID.SetOutputLimits(0,MAXIMUM_CURRENT_SINGLE);
        maximum_current = MAXIMUM_CURRENT_SINGLE;
    }
    else{
       speedPID.SetOutputLimits(0,MAXIMUM_CURRENT_DUAL);
       maximum_current = MAXIMUM_CURRENT_DUAL;
    }
    
    //check screen flag for PID or not PID
    //if(canMessageHandler.screen.pid_state)

    /*for a PID to control current depending on the desired speed we need 3 things
    - first read actual speed from CAN bus
    - second define speed range and translate throttle to a desired speed
    - third compute PID to output new current value to send to motors*/
    PIDactualspeed = tsb_can.foils.veloc;
    //get desired speed from throttle (convert motor current to speed value)
    if(reverse_flag == 0) {
        PIDdesiredspeed = motor_current*MAXIMUM_SPEED/maximum_current;
    } else PIDdesiredspeed = 0; //do not try to use the PID if going in reverse
    speedPID.Compute();
    

    if (killswitch_flag == false ){
        
        switch(reverse_flag){
            case 0: // If going forward - here with PID
                if(tsb_can.screen.pid_state) {
                    if( abs(PIDcurrent) >= tsb_can.screen.current_threshold ){ // Asked current bigger than dual motor threshold
                        dual_Motor_alg(PIDcurrent);
                    }else{ // Only single motor required
                        M1_UART.setCurrent(PIDcurrent,1);
                        M1_UART.setCurrent(0,2);
                    }
                } else {
                    if( abs(motor_current) >= tsb_can.screen.current_threshold ){ // Asked current bigger than dual motor threshold
                        dual_Motor_alg(motor_current);
                    }else{ // Only single motor required
                        M1_UART.setCurrent(motor_current,1);
                        M1_UART.setCurrent(0,2);
                    }
                }
                break;
            case 1: // If going in reverse divide current by 3
                M1_UART.setCurrent(motor_current/3,1);
                M1_UART.setCurrent(motor_current/3,2);
                break;
        }
    }
    else{
        if( motor_current == 0 ){
            killswitch_flag = false; // Throttle have gone to 0 reset killswitch_flag
            M1_UART.setCurrent(0,1);
            M1_UART.setCurrent(0,2);
        }
    } 

    if ( tsb_can.screen.newFile == true ){
		noInterrupts();
		Motor_File.createNewFile(); // Creates a new log file
		tsb_can.screen.newFile = false;
		Serial.println("new file created");
		interrupts();
	}

    if (tsb_can.screen.pid_speed_setpoint != MAXIMUM_SPEED){
        MAXIMUM_SPEED = tsb_can.screen.pid_speed_setpoint;
    }      

     tsb_can.check_valid_data();

    
    // // Reset whatch dog timer
    // noInterrupts();
    // WDOG_REFRESH = 0xA602;
    // WDOG_REFRESH = 0xB480;
    // interrupts();
}
