/*
    Copyright (C) 2020  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#ifndef _CONFIG_h
#define _CONFIG_h

// Motor Stuff
#define MOTOR1 Serial1
#define DEBUGSERIAL Serial
#define MOTOR1_ID 0x01
#define MOTOR2_ID 0x02
#define VESC_PIN 9 // Pin to sense if VESCs have been turned off. USE the 3.3V OUTPUT and put 1kOhm in series.
#define SPI_SCK 14
#define SD_CS_PIN 10

/* Choose which data the VESC send over serial, Structure defined here: 
https://github.com/vedderb/bldc/blob/dfc3ed23196d8ba8d0c8b345edfb8a862da3fa48/commands.c#L250

This mask retrives the following data:
Expected response lenght 29 bytes, 24 data + stat + end + lenght + 2 crc
avg_motor_current
avg_input_current
duty_cycle_now
rpm
INPUT_VOLTAGE
fault
id
MOS temperatures
*/
#define COMM_GET_VALUES_SELECTIVE_MASK 0x000681CC 

//Data aquisition stuff
#define MSG_TIME 0.08F // Time to send data to CAN BUS and get data from VESCS

// Throttle stuff
#define MAXIMUM_CURRENT_SINGLE 115.0
#define MAXIMUM_CURRENT_DUAL 230.0 
#define MINIMUM_CURRENT 0.0
//initial current of M2 for dual motor algorithm
#define CURRENT_INIT 9.0
//max boat speed for throttle in speed control
extern float MAXIMUM_SPEED; // m/s
// #define MAXIMUM_SPEED 6.4 // m/s

//define gains for speed PID
#define Kp 80
#define Ki 7
#define Kd 15


// Dual motor starup algorithm
extern long rpmM1;
extern long rpmM2;
extern float maximum_current;


#endif
