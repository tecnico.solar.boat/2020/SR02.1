// This file should contain code to communicate with a Torquedo Remote Throttle but 
// given that we have a Non Disclose Agreement signed with Torquedo sensitive 
// information was removed.

/*
    Copyright (C) 2020  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/



#include <Arduino.h>

#define RS485SERIAL Serial3
#define THROTTLE_SCALE 1.0
#define DIRPIN 			5

#define PACKET_START	
#define PACKET_END		
#define INFO			
#define ID_REMOTE1		

#define MAX_THROTTLE_ERRORS	3


class TSB_TORQUEDO
{
	private:
		uint8_t throttle_error_count=0;
		int16_t throttle=0;
		float lastthrottle;
		uint8_t throttle_zero_count = 0;


		uint8_t uart_txbuf[5];
		uint8_t AddToCRC(uint8_t b, uint8_t crc);
		uint8_t CRC_calc(uint8_t *buf, uint8_t bufsize);
	public:
		bool ThrottleErrorFlag;
		
		void PrepareThrottleRequest();
		float getDesiredCurrent();
};