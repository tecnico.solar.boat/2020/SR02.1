/*
    Copyright (C) 2020  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#ifndef _VESCUART_h
#define _VESCUART_h

#include <Arduino.h>
#include "../../include/datatypes.h"
#include "../../include/Config.h"
#include "crc.h"
#include <CAN_TSB.h>

extern float motor_current;

void init_messages();
void send_messages();
void get_vescs_data();
const char* fault_to_string(mc_fault_code fault);

class VescUart
{
	/** Struct to store the telemetry data returned by the VESC */
	struct dataPackage {
		float temp_fet_filtered;
		float temp_motor_filtered;
		float avgMotorCurrent;
		float avgInputCurrent;
		float avgId;
		float avgIq;
		float dutyCycleNow;
		long rpm;
		float inpVoltage;
		float ampHours;
		float ampHoursCharged;
		float wattHours;
		float wattHoursCharged;
		long tachometer;
		long tachometerAbs;
		mc_fault_code faultCode;
		float pid_pos_now;
		uint8_t id;
		float NTC_TEMP_MOS1;
		float NTC_TEMP_MOS2;
		float NTC_TEMP_MOS3;
		float NTC_TEMP_MOS_MAX;
		float avgVd;
		float avgVq;
	};

	/** Struct to hold the nunchuck values to send over UART */
	struct nunchuckPackage {
		int	valueX;
		int	valueY;
		bool upperButton; // valUpperButton
		bool lowerButton; // valLowerButton
	};


	public:
		/**
		 * @brief      Class constructor
		 */
		VescUart(void);

		/** Variabel to hold measurements returned from VESC */
		dataPackage data;
		dataPackage motor1_data;
		dataPackage motor2_data;

		/** Variabel to hold nunchuck values */
		nunchuckPackage nunchuck; 

		/**
		 * @brief      Set the serial port for uart communication
		 * @param      port  - Reference to Serial port (pointer) 
		 */
		void setSerialPort(HardwareSerial* port);

		/**
		 * @brief      Set the serial port for debugging
		 * @param      port  - Reference to Serial port (pointer) 
		 */
		void setDebugPort(Stream* port);

		/**
		 * @brief      Sends a command to VESC and stores the returned data
		 * @param      num - Choose VESC 1 or 2 (master or slave)
		 * @return     True if successfull otherwise false
		 */
		bool getVescValues(int num);

		/**
		 * @brief      Sends a command to VESC and stores the returned data
		 * @param      num - Choose VESC 1 or 2 (master or slave)
		 * @param      mask - COM_GET_VALUES_SELECTIVE mask
		 * @return     True if successfull otherwise false
		 */
		bool getVescValuesSelective(int num, uint32_t mask);

		/**
		 * @brief      Sends values for joystick and buttons to the nunchuck app
		 */
		void setNunchuckValues(void);

		/**
		 * @brief      Set the current to drive the motor
		 * @param      current  - The current to apply
		 * @param      motor  - Choose VESC 1 or 2 (master or slave)
		 */
		void setCurrent(float current, int motor);

		/**
		 * @brief      Set the current to brake the motor
		 * @param      brakeCurrent  - The current to apply
		 */
		void setBrakeCurrent(float brakeCurrent);

		/**
		 * @brief      Set the rpm of the motor
		 * @param      rpm  - The desired RPM (actually eRPM = RPM * poles)
		 */
		void setRPM(float rpm);

		/**
		 * @brief      Set the duty of the motor
		 * @param      duty  - The desired duty (0.0-1.0)
		 */
		void setDuty(float duty);

		/**
		 * @brief      Help Function to print struct dataPackage over Serial for Debug
		 */
		void printVescValues(void);

	private: 

		/** Variabel to hold the reference to the Serial object to use for UART */
		HardwareSerial* serialPort = NULL;

		/** Variabel to hold the reference to the Serial object to use for debugging. 
		  * Uses the class Stream instead of HarwareSerial */
		Stream* debugPort = NULL;

		/**
		 * @brief      Packs the payload and sends it over Serial
		 *
		 * @param      payload  - The payload as a unit8_t Array with length of int lenPayload
		 * @param      lenPay   - Length of payload
		 * @return     The number of bytes send
		 */
		int packSendPayload(uint8_t * payload, int lenPay);

		/**
		 * @brief      Receives the message over Serial
		 *
		 * @param      payloadReceived  - The received payload as a unit8_t Array
		 * @return     The number of bytes receeived within the payload
		 */
		int receiveUartMessage(uint8_t * payloadReceived);

		/**
		 * @brief      Verifies the message (CRC-16) and extracts the payload
		 *
		 * @param      message  - The received UART message
		 * @param      lenMes   - The lenght of the message
		 * @param      payload  - The final payload ready to extract data from
		 * @return     True if the process was a success
		 */
		bool unpackPayload(uint8_t * message, int lenMes, uint8_t * payload);

		/**
		 * @brief      Extracts the data from the received payload
		 *
		 * @param      message  - The payload to extract data from
		 * @return     True if the process was a success
		 */
		bool processReadPacket(uint8_t * message);

		/**
		 * @brief      Help Function to print uint8_t array over Serial for Debug
		 *
		 * @param      data  - Data array to print
		 * @param      len   - Lenght of the array to print
		 */
		void serialPrint(uint8_t * data, int len);

};

#endif