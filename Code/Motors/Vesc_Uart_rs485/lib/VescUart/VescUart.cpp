/*
    Copyright (C) 2020  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include "VescUart.h"
#include <HardwareSerial.h>

CAN_message_t motor1_current;
CAN_message_t motor1_duty_rpm_voltage;
CAN_message_t motor1_temperatures;
CAN_message_t motor1_fault_id_asked_current;
CAN_message_t motor2_current;
CAN_message_t motor2_duty_rpm_voltage;
CAN_message_t motor2_temperatures;
CAN_message_t motor2_fault_id;


VescUart::VescUart(void){
	nunchuck.valueX         = 127;
	nunchuck.valueY         = 127;
	nunchuck.lowerButton  	= false;
	nunchuck.upperButton  	= false;
}

void VescUart::setSerialPort(HardwareSerial* port)
{
	serialPort = port;
}

void VescUart::setDebugPort(Stream* port)
{
	debugPort = port;
}

int VescUart::receiveUartMessage(uint8_t * payloadReceived) {

	// Messages <= 255 starts with "2", 2nd byte is length
	// Messages > 255 starts with "3" 2nd and 3rd byte is length combined with 1st >>8 and then &0xFF

	uint16_t counter = 0;
	uint16_t endMessage = 256;
	bool messageRead = false;
	uint8_t messageReceived[256];
	uint16_t lenPayload = 0;
	
	uint32_t timeout = millis() + 100; // Defining the timestamp for timeout (100ms before timeout)

	while ( millis() < timeout && messageRead == false) {

		while (serialPort->available()) {

			messageReceived[counter++] = serialPort->read();

			if (counter == 2) {

				switch (messageReceived[0])
				{
					case 2:
						endMessage = messageReceived[1] + 5; //Payload size + 2 for sice + 3 for SRC and End.
						lenPayload = messageReceived[1];
					break;

					case 3:
						// ToDo: Add Message Handling > 255 (starting with 3)
						if( debugPort != NULL ){
							//debugPort->println("Message is larger than 256 bytes - not supported");
						}
					break;

					default:
						if( debugPort != NULL ){
							//debugPort->println("Unvalid start bit");
						}
					break;
				}
			}

			if (counter >= sizeof(messageReceived)) {
				break;
			}

			if (counter == endMessage && messageReceived[endMessage - 1] == 3) {
				messageReceived[endMessage] = 0;
				if (debugPort != NULL) {
					//debugPort->println("End of message reached!");
				}
				messageRead = true;
				break; // Exit if end of message is reached, even if there is still more data in the buffer.
			}
		}
	}
	if(messageRead == false && debugPort != NULL ) {
		//debugPort->println("Timeout");
	}
	
	bool unpacked = false;

	if (messageRead) {
		unpacked = unpackPayload(messageReceived, endMessage, payloadReceived);
		// for (int i = 0; i < lenPayload; i++)
		// {
		// 	Serial.print((String)messageReceived[i] + " ");
		// }
		// Serial.println();
	}

	if (unpacked) {
		// Message was read
		return lenPayload; 
	}
	else {
		// No Message Read
		return 0;
	}
}


bool VescUart::unpackPayload(uint8_t * message, int lenMes, uint8_t * payload) {

	uint16_t crcMessage = 0;
	uint16_t crcPayload = 0;

	// Rebuild crc:
	crcMessage = message[lenMes - 3] << 8;
	crcMessage &= 0xFF00;
	crcMessage += message[lenMes - 2];

	// Extract payload:
	memcpy(payload, &message[2], message[1]);

	crcPayload = crc16(payload, message[1]);

	if (crcPayload == crcMessage) {
		return true;
	}else{
		return false;
	}
}


int VescUart::packSendPayload(uint8_t * payload, int lenPay) {

	uint16_t crcPayload = crc16(payload, lenPay);
	int count = 0;
	uint8_t messageSend[256];

	if (lenPay <= 256)
	{
		messageSend[count++] = 2;
		messageSend[count++] = lenPay;
	}
	else
	{
		messageSend[count++] = 3;
		messageSend[count++] = (uint8_t)(lenPay >> 8);
		messageSend[count++] = (uint8_t)(lenPay & 0xFF);
	}

	memcpy(&messageSend[count], payload, lenPay);

	count += lenPay;
	messageSend[count++] = (uint8_t)(crcPayload >> 8);
	messageSend[count++] = (uint8_t)(crcPayload & 0xFF);
	messageSend[count++] = 3;
	messageSend[count] = '\0';

	// Sending package
	serialPort->write(messageSend, count);

	// Returns number of send bytes
	return count;
}

bool VescUart::processReadPacket(uint8_t * message) {

	COMM_PACKET_ID packetId;
	int32_t ind = 0;

	packetId = (COMM_PACKET_ID)message[0];
	message++; // Removes the packetId from the actual message (payload)

	switch (packetId){
		case COMM_GET_VALUES: // Structure defined here: https://github.com/vedderb/bldc/blob/dfc3ed23196d8ba8d0c8b345edfb8a862da3fa48/commands.c#L250
		case COMM_GET_VALUES_SELECTIVE:{
			uint32_t mask = 0xFFFFFFFF;
			if (packetId == COMM_GET_VALUES_SELECTIVE) {
				mask = buffer_get_uint32(message, &ind);
			}

			if (mask & ((uint32_t)1 << 0)) {
				data.temp_fet_filtered	= buffer_get_float16(message, 1e1, &ind);
			}
			if (mask & ((uint32_t)1 << 1)) {
				data.temp_motor_filtered = buffer_get_float16(message, 1e1, &ind);
			}
			if (mask & ((uint32_t)1 << 2)) {
				data.avgMotorCurrent 	= buffer_get_float32(message, 1e2, &ind);
			}
			if (mask & ((uint32_t)1 << 3)) {
				data.avgInputCurrent 	= buffer_get_float32(message, 1e2, &ind);
			}
			if (mask & ((uint32_t)1 << 4)) {
				data.avgId				= buffer_get_float32(message, 1e2, &ind);
			}
			if (mask & ((uint32_t)1 << 5)) {
				data.avgIq				= buffer_get_float32(message, 1e2, &ind);
			}
			if (mask & ((uint32_t)1 << 6)) {
				data.dutyCycleNow 		= buffer_get_float16(message, 1e3, &ind);
			}
			if (mask & ((uint32_t)1 << 7)) {
				data.rpm 				= buffer_get_int32(message, &ind);
			}
			if (mask & ((uint32_t)1 << 8)) {
				data.inpVoltage 		= buffer_get_float16(message, 1e1, &ind);
			}
			if (mask & ((uint32_t)1 << 9)) {
				data.ampHours 			= buffer_get_float32(message, 1e4, &ind);
			}
			if (mask & ((uint32_t)1 << 10)) {
				data.ampHoursCharged 	= buffer_get_float32(message, 1e4, &ind);
			}
			if (mask & ((uint32_t)1 << 11)) {
				data.wattHours			= buffer_get_float32(message, 1e4, &ind);
			}
			if (mask & ((uint32_t)1 << 12)) {
				data.wattHoursCharged	= buffer_get_float32(message, 1e4, &ind);
			}
			if (mask & ((uint32_t)1 << 13)) {
				data.tachometer 		= buffer_get_int32(message, &ind);
			}
			if (mask & ((uint32_t)1 << 14)) {
				data.tachometerAbs 		= buffer_get_int32(message, &ind);
			}
			if (mask & ((uint32_t)1 << 15)) {
				data.faultCode			= (mc_fault_code)message[ind++];
			}
			if (mask & ((uint32_t)1 << 16)) {
				data.pid_pos_now		= buffer_get_float32(message, 1e6, &ind);
			}
			if (mask & ((uint32_t)1 << 17)) {
				data.id					= message[ind++];
			}
			if (mask & ((uint32_t)1 << 18)) {
				data.NTC_TEMP_MOS1		= buffer_get_float16(message, 1e1, &ind);
				data.NTC_TEMP_MOS2		= buffer_get_float16(message, 1e1, &ind);
				data.NTC_TEMP_MOS3		= buffer_get_float16(message, 1e1, &ind);	
			}
			if (mask & ((uint32_t)1 << 19)) {
				data.avgVd				= buffer_get_float32(message, 1e3, &ind);
			}
			if (mask & ((uint32_t)1 << 20)) {
				data.avgVq				= buffer_get_float32(message, 1e3, &ind);
			}

			switch(data.id){
				case MOTOR1_ID:
					ind = 0;
					buffer_append_float32(motor1_current.buf, data.avgMotorCurrent, 1e2, &ind);
					buffer_append_float32(motor1_current.buf, data.avgInputCurrent, 1e2, &ind);

					ind = 0;
					buffer_append_float16(motor1_duty_rpm_voltage.buf, data.dutyCycleNow, 1e3, &ind);
					buffer_append_int32(motor1_duty_rpm_voltage.buf, data.rpm, &ind);
					buffer_append_float16(motor1_duty_rpm_voltage.buf, data.inpVoltage, 1e1, &ind);
					rpmM1 = data.rpm;

					ind = 2;
					buffer_append_float16(motor1_temperatures.buf, data.NTC_TEMP_MOS1, 1e1, &ind);
					buffer_append_float16(motor1_temperatures.buf, data.NTC_TEMP_MOS2, 1e1, &ind);
					buffer_append_float16(motor1_temperatures.buf, data.NTC_TEMP_MOS3, 1e1, &ind);
					
					motor1_fault_id_asked_current.buf[0] = data.faultCode;
					motor1_fault_id_asked_current.buf[1] = data.id;

					motor1_data = data;
					motor1_data.NTC_TEMP_MOS_MAX = data.NTC_TEMP_MOS1;
					motor1_data.NTC_TEMP_MOS_MAX = max(motor1_data.NTC_TEMP_MOS_MAX, data.NTC_TEMP_MOS2);
					motor1_data.NTC_TEMP_MOS_MAX = max(motor1_data.NTC_TEMP_MOS_MAX, data.NTC_TEMP_MOS3);
					//printVescValues();		
				break;

				case MOTOR2_ID:
					ind = 0;
					buffer_append_float32(motor2_current.buf, data.avgMotorCurrent, 1e2, &ind);
					buffer_append_float32(motor2_current.buf, data.avgInputCurrent, 1e2, &ind);

					ind = 0;
					buffer_append_float16(motor2_duty_rpm_voltage.buf, data.dutyCycleNow, 1e3, &ind);
					buffer_append_int32(motor2_duty_rpm_voltage.buf, data.rpm, &ind);
					buffer_append_float16(motor2_duty_rpm_voltage.buf, data.inpVoltage, 1e1, &ind);
					rpmM2 = data.rpm;

					ind = 2;
					buffer_append_float16(motor2_temperatures.buf, data.NTC_TEMP_MOS1, 1e1, &ind);
					buffer_append_float16(motor2_temperatures.buf, data.NTC_TEMP_MOS2, 1e1, &ind);
					buffer_append_float16(motor2_temperatures.buf, data.NTC_TEMP_MOS3, 1e1, &ind);

					motor2_fault_id.buf[0] = data.faultCode;
					motor2_fault_id.buf[1] = data.id;
					
					motor2_data = data;
					motor2_data.NTC_TEMP_MOS_MAX = data.NTC_TEMP_MOS1;
					motor2_data.NTC_TEMP_MOS_MAX = max(motor2_data.NTC_TEMP_MOS_MAX, data.NTC_TEMP_MOS2);
					motor2_data.NTC_TEMP_MOS_MAX = max(motor2_data.NTC_TEMP_MOS_MAX, data.NTC_TEMP_MOS3);
					//printVescValues();		
				break;
			}

		}break;
		default:
			return false;
		break;
	}
return false;
}

bool VescUart::getVescValues(int num) {
	return getVescValuesSelective(1, 0xFFFFFFFF);
}

bool VescUart::getVescValuesSelective(int num, uint32_t mask){
	// IF USING DIFFERENT MASK the if (lenPayload == 77 || lenPayload == 29) NEEDS TO BE CHANGED
	uint8_t payload[256];
	if(num == 1){  // When talking to the master Vesc
		uint8_t command[5] = { COMM_GET_VALUES_SELECTIVE};
		int32_t ind = 1;
		buffer_append_uint32(command, mask, &ind);
		packSendPayload(command, 5);
	}
	if(num == 2){  //When talking to a slave Vesc
		uint8_t command[7] = {COMM_FORWARD_CAN , MOTOR2_ID, COMM_GET_VALUES_SELECTIVE};
		int32_t ind = 3;
		buffer_append_uint32(command, mask, &ind);
		packSendPayload(command, 7);
 	}
	delay(1); //needed, otherwise data is not read
	int lenPayload = receiveUartMessage(payload);
	// Serial.println(lenPayload);
	if (lenPayload == 77 || lenPayload == 29) {
		bool read = processReadPacket(payload); //returns true if sucessful
		return read;
	}
	else
	{
		return false;
	}
}

void VescUart::setNunchuckValues() {
	int32_t ind = 0;
	uint8_t payload[11];

	payload[ind++] = COMM_SET_CHUCK_DATA;
	payload[ind++] = nunchuck.valueX;
	payload[ind++] = nunchuck.valueY;
	buffer_append_bool(payload, nunchuck.lowerButton, &ind);
	buffer_append_bool(payload, nunchuck.upperButton, &ind);
	
	// Acceleration Data. Not used, Int16 (2 byte)
	payload[ind++] = 0;
	payload[ind++] = 0;
	payload[ind++] = 0;
	payload[ind++] = 0;
	payload[ind++] = 0;
	payload[ind++] = 0;

	packSendPayload(payload, 11);
}

void VescUart::setCurrent(float current, int motor) {
	int32_t index = 0;
	uint8_t payload[7];
	if(motor == 1){  // When talking to the master Vesc
		payload[index++] = COMM_SET_CURRENT;
	}
	if(motor == 2){  //When talking to a slave Vesc
		payload[index++] = {COMM_FORWARD_CAN}; //Forwarding CAN 
		payload[index++] = {MOTOR2_ID};        //Sending CAN id
		payload[index++] = {COMM_SET_CURRENT}; //Setting desire current
 	}
	buffer_append_int32(payload, (int32_t)(current * 1000), &index);
	packSendPayload(payload, 7);
}

void VescUart::setBrakeCurrent(float brakeCurrent) {
	int32_t index = 0;
	uint8_t payload[5];

	payload[index++] = COMM_SET_CURRENT_BRAKE;
	buffer_append_int32(payload, (int32_t)(brakeCurrent * 1000), &index);

	packSendPayload(payload, 5);
}

void VescUart::setRPM(float rpm) {
	int32_t index = 0;
	uint8_t payload[5];

	payload[index++] = COMM_SET_RPM ;
	buffer_append_int32(payload, (int32_t)(rpm), &index);

	packSendPayload(payload, 5);
}

void VescUart::setDuty(float duty) {
	int32_t index = 0;
	uint8_t payload[5];

	payload[index++] = COMM_SET_DUTY;
	buffer_append_int32(payload, (int32_t)(duty * 100000), &index);

	packSendPayload(payload, 5);
}

void VescUart::serialPrint(uint8_t * data, int len) {
	if(debugPort != NULL){
		for (int i = 0; i <= len; i++)
		{
			debugPort->print(data[i]);
			debugPort->print(" ");
		}

		debugPort->println("");
	}
}

void VescUart::printVescValues() {
	if(debugPort != NULL){
		debugPort->print("temp_fet_filtered: "); 	debugPort->println(data.temp_fet_filtered);
		debugPort->print("temp_motor_filtered: "); 	debugPort->println(data.temp_motor_filtered);
		debugPort->print("avgMotorCurrent: "); 	debugPort->println(data.avgMotorCurrent);
		debugPort->print("avgInputCurrent: "); 	debugPort->println(data.avgInputCurrent);
		debugPort->print("avgId: "); 	debugPort->println(data.avgId);
		debugPort->print("avgIq: "); 	debugPort->println(data.avgIq);
		debugPort->print("dutyCycleNow: "); 	debugPort->println(data.dutyCycleNow);
		debugPort->print("rpm: "); 				debugPort->println(data.rpm);
		debugPort->print("inputVoltage: "); 	debugPort->println(data.inpVoltage);
		debugPort->print("ampHours: "); 		debugPort->println(data.ampHours);
		debugPort->print("ampHoursCharges: "); 	debugPort->println(data.ampHoursCharged);
		debugPort->print("wattHours: "); 	debugPort->println(data.wattHours);
		debugPort->print("wattHoursCharged: "); 	debugPort->println(data.wattHoursCharged);
		debugPort->print("tachometer: "); 		debugPort->println(data.tachometer);
		debugPort->print("tachometerAbs: "); 	debugPort->println(data.tachometerAbs);
		debugPort->print("Fault Code: "); 	debugPort->println(fault_to_string(data.faultCode));
		debugPort->print("pid_pos_now: "); 	debugPort->println(data.pid_pos_now);
		debugPort->print("ID: "); 	debugPort->println(data.id);
		debugPort->print("NTC_TEMP_MOS1: "); 	debugPort->println(data.NTC_TEMP_MOS1);
		debugPort->print("NTC_TEMP_MOS2: "); 	debugPort->println(data.NTC_TEMP_MOS2);
		debugPort->print("NTC_TEMP_MOS3: "); 	debugPort->println(data.NTC_TEMP_MOS3);
		debugPort->print("avgVq: "); 	debugPort->println(data.avgVq);
		debugPort->print("avgVd: "); 	debugPort->println(data.avgVd);
		debugPort->println();
		debugPort->println();
	}
}

const char* fault_to_string(mc_fault_code fault) {
	switch (fault) {
	case FAULT_CODE_NONE: return "FAULT_CODE_NONE"; break;
	case FAULT_CODE_OVER_VOLTAGE: return "FAULT_CODE_OVER_VOLTAGE"; break;
	case FAULT_CODE_UNDER_VOLTAGE: return "FAULT_CODE_UNDER_VOLTAGE"; break;
	case FAULT_CODE_DRV: return "FAULT_CODE_DRV"; break;
	case FAULT_CODE_ABS_OVER_CURRENT: return "FAULT_CODE_ABS_OVER_CURRENT"; break;
	case FAULT_CODE_OVER_TEMP_FET: return "FAULT_CODE_OVER_TEMP_FET"; break;
	case FAULT_CODE_OVER_TEMP_MOTOR: return "FAULT_CODE_OVER_TEMP_MOTOR"; break;
	case FAULT_CODE_GATE_DRIVER_OVER_VOLTAGE: return "FAULT_CODE_GATE_DRIVER_OVER_VOLTAGE"; break;
	case FAULT_CODE_GATE_DRIVER_UNDER_VOLTAGE: return "FAULT_CODE_GATE_DRIVER_UNDER_VOLTAGE"; break;
	case FAULT_CODE_MCU_UNDER_VOLTAGE: return "FAULT_CODE_MCU_UNDER_VOLTAGE"; break;
	case FAULT_CODE_BOOTING_FROM_WATCHDOG_RESET: return "FAULT_CODE_BOOTING_FROM_WATCHDOG_RESET"; break;
	case FAULT_CODE_ENCODER_SPI: return "FAULT_CODE_ENCODER_SPI"; break;
	case FAULT_CODE_ENCODER_SINCOS_BELOW_MIN_AMPLITUDE: return "FAULT_CODE_ENCODER_SINCOS_BELOW_MIN_AMPLITUDE"; break;
	case FAULT_CODE_ENCODER_SINCOS_ABOVE_MAX_AMPLITUDE: return "FAULT_CODE_ENCODER_SINCOS_ABOVE_MAX_AMPLITUDE"; break;
    case FAULT_CODE_FLASH_CORRUPTION: return "FAULT_CODE_FLASH_CORRUPTION";
    case FAULT_CODE_HIGH_OFFSET_CURRENT_SENSOR_1: return "FAULT_CODE_HIGH_OFFSET_CURRENT_SENSOR_1";
    case FAULT_CODE_HIGH_OFFSET_CURRENT_SENSOR_2: return "FAULT_CODE_HIGH_OFFSET_CURRENT_SENSOR_2";
    case FAULT_CODE_HIGH_OFFSET_CURRENT_SENSOR_3: return "FAULT_CODE_HIGH_OFFSET_CURRENT_SENSOR_3";
    case FAULT_CODE_UNBALANCED_CURRENTS: return "FAULT_CODE_UNBALANCED_CURRENTS";
	default: return "FAULT_UNKNOWN"; break;
	}
}

void init_messages(){
        motor1_current.ext = 0;
        motor1_current.id = 0x603;
        motor1_current.len = 8;

        motor1_duty_rpm_voltage.ext = 0;
        motor1_duty_rpm_voltage.id = 0x60B;
        motor1_duty_rpm_voltage.len = 8;

        motor1_temperatures.ext = 0;
        motor1_temperatures.id = 0x613;
        motor1_temperatures.len = 8;

        motor1_fault_id_asked_current.ext = 0;
        motor1_fault_id_asked_current.id = 0x61B;
        motor1_fault_id_asked_current.len = 6;

        motor2_current.id = 0x623;
		motor2_current.ext = 0;
        motor2_current.len = 8;

        motor2_duty_rpm_voltage.ext = 0;
        motor2_duty_rpm_voltage.id = 0x62B;
        motor2_duty_rpm_voltage.len = 8;

        motor2_temperatures.ext = 0;
        motor2_temperatures.id = 0x633;
        motor2_temperatures.len = 8;

        motor2_fault_id.ext = 0;
        motor2_fault_id.id = 0x63B;
        motor2_fault_id.len = 2;

}

void send_messages(){
	float aux1 = analogRead(A10)*(3.3 / 4096.0);
	float aux2 = analogRead(A11)*(3.3 / 4096.0);
	float temp1 = 883.8*pow(((3300.0*aux1)/(3.3-aux1)),(-0.1737))-93.83;
	float temp2 = 883.8*pow(((3300.0*aux2)/(3.3-aux2)),(-0.1737))-93.83;
	int32_t ind = 0;
	buffer_append_float16(motor1_temperatures.buf, temp1, 1e1, &ind);
	ind = 0;
	buffer_append_float16(motor2_temperatures.buf, temp2, 1e1, &ind);
	ind = 2;
	buffer_append_int16(motor1_fault_id_asked_current.buf, motor_current, &ind);
	buffer_append_float16(motor1_fault_id_asked_current.buf, MAXIMUM_SPEED, 1e1, &ind);

	Can0.write(motor1_current);
	Can0.write(motor1_duty_rpm_voltage);
	Can0.write(motor1_temperatures);
	Can0.write(motor1_fault_id_asked_current);
	Can0.write(motor2_current);
	Can0.write(motor2_duty_rpm_voltage);
	Can0.write(motor2_temperatures);
	Can0.write(motor2_fault_id);	
 }