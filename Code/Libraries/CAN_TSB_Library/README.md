# TSB CAN Library Code

This is the code for the CAN cummunication bettween modules inside the boat.

Copyright (C) 2020  Técnico Solar Boat

This repository and its contents  is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.
The content of this repository is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/.
You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
or via our facebook page at https://fb.com/tecnico.solarboat


# Communication protocol

> Brief description of how the TSB CAN Library works.

#### Index

- [Message structure](#Mensagens)
- [ID Structure](#ID)
- [Code](#Código)

<a name="Mensagens"/>

## Message structure
![JC](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5e/CAN-Bus-frame_in_base_format_without_stuffbits.svg/709px-CAN-Bus-frame_in_base_format_without_stuffbits.svg.png)

On the code, only ID, DL (message lenght) and DB (message content) need to be defined.

<a name="ID"/>

### ID Structure

| Priority bits |  Data ID | Source ID |
|:-------------:|:--------:|:---------:|
|    2 bits     |  6 bits  |  3 bits   |


#### Priority bits

- Defines the message priority
- 00 maximum priority
- 11 lowest priority

#### Data ID

- Defines the message content

#### Source ID

- Defines which node sent the message
- Together with the Data ID defines which message was sent

| Source ID (3 bits) |             Node             |
|:------------------:|:----------------------------:|
|       0x01         |  [Solar BMS](#BMS_MSG)       |
|       0x02         |  [Foils](#FOILS_MSG)         |
|       0x03         |  [Motor](#MOTOR_MSG)         |
|       0x04         |  [CAN WiFi](#CANWIFI_MSG)    |
|		0x05		         |	[Screen](#SCREEN_MSG)	      |
|		0x06		         |  [Current Shunt](#SHUNT_MSG) |

<br/>

<a name="BMS_MSG"/>

## Solar BMS Messages:
### status (CAN ID: 0x609)
**LEN = 6**

|  Offset |   Type    |        Name       |     Scale     |           Description            |
|:-------:|:---------:|:-----------------:|:-------------:|:--------------------------------:|
|    0    |  uint8_t  |       Status      |      -        | Status of the battery            | 
|    1    |  uint16_t |    LTC Status     |      -        | Status of the LTC                |
|    3    |  uint8_t  |  Cells Balancing0 |      -        | Which cells are balancing [1:9]  |
|    4    |  uint8_t  |  Cells Balancing1 |      -        | Which cells are balancing [10:12]|
|    5    |  uint8_t  |     Warnings      |      -        | Warnings                         |

### SOC_Voltage_Currents (CAN ID: 0x611)
**LEN = 8**

|  Offset |   Type   |        Name       |       Scale        |        Description          |
|:-------:|:--------:|:-----------------:|:------------------:|:---------------------------:|
|    0    | uint16_t |  State of Charge  |  SOC / 100         | Battery State of Charge     |
|    2    | uint16_t |      Voltage      |  V * 20 * 0.0001   | Battery pack voltage        |
|    4    | int16_t  |      Current      |  A / 10            | Battery pack current        |
|    6    | uint16_t |  Solar Current    |  A / 10            | Solar Panels current        |


### cellvoltages0(CAN ID: 0x619)
**LEN = 8**

|  Offset |   Type   |     Name      |     Scale     |        Description        |
|:-------:|:--------:|:-------------:|:-------------:|:-------------------------:|
|    0    | uint16_t |   voltage1    | V / 10000     | cell voltage              |
|    2    | uint16_t |   voltage2    |               | cell voltage              |
|    4    | uint16_t |   voltage3    |               | cell voltage              |
|    6    | uint16_t |   voltage4    |               | cell voltage              |

### cellvoltages1(CAN ID: 0x621)
**LEN = 8**

|  Offset |   Type   |     Name      |     Scale     |        Description        |
|:-------:|:--------:|:-------------:|:-------------:|:-------------------------:|
|    0    | uint16_t |   voltage5    | V / 10000     | cell voltage              |
|    2    | uint16_t |   voltage6    |               | cell voltage              |
|    4    | uint16_t |   voltage7    |               | cell voltage              |
|    6    | uint16_t |   voltage8    |               | cell voltage              |

### cellvoltages2(CAN ID: 0x629)
**LEN = 8**

|  Offset |   Type   |     Name      |     Scale     |        Description        |
|:-------:|:--------:|:-------------:|:-------------:|:-------------------------:|
|    0    | uint16_t |   voltage9    | V / 10000     | cell voltage              |
|    2    | uint16_t |   voltage10   |               | cell voltage              |
|    4    | uint16_t |   voltage11   |               | cell voltage              |
|    6    | uint16_t |   voltage12   |               | cell voltage              |

### SolarPanelVoltages0(CAN ID: 0x631)
**LEN = 8**

|  Offset |   Type   |   Name   |     Scale     |        Description        |
|:-------:|:--------:|:--------:|:-------------:|:-------------------------:|
|    0    | uint16_t |   SP1    | V / 1000      | solar arre 1 voltage      |
|    2    | uint16_t |   SP2    |               | solar arre 2 voltage      |
|    4    | uint16_t |   SP3    |               | solar arre 3 voltage      |
|    6    | uint16_t |   SP4    |               | solar arre 4 voltage      |

### SolarPanelVoltages1(CAN ID: 0x639)
**LEN = 2**

|  Offset |   Type   |   Name   |     Scale     |        Description        |
|:-------:|:--------:|:--------:|:-------------:|:-------------------------:|
|    0    | uint16_t |   SP5    | V / 1000      | solar arre 5 voltage      |

### temperatures0 (CAN ID: 0x641)
**LEN = 8**

|  Offset |   Type   |           Name          |     Scale        |        Description       |
|:-------:|:--------:|:-----------------------:|:----------------:|:------------------------:|
|    0    |  uint8_t |   Cell 1 temperature    |  round to units  | temperature              |
|    1    |  uint8_t |   Cell 2 temperature    |                  | temperature              |
|    2    |  uint8_t |   Cell 3 temperature    |                  | temperature              |
|    3    |  uint8_t |   Cell 4 temperature    |                  | temperature              |
|    4    |  uint8_t |   Cell 5 temperature    |                  | temperature              |
|    5    |  uint8_t |   Cell 6 temperature    |                  | temperature              |
|    6    |  uint8_t |   Cell 7 temperature    |                  | temperature              |
|    7    |  uint8_t |   Cell 8 temperature    |                  | temperature              |

### temperatures1 (CAN ID: 0x649)
**LEN = 8**

|  Offset |   Type   |           Name          |     Scale        |        Description       |
|:-------:|:--------:|:-----------------------:|:----------------:|:------------------------:|
|    0    |  uint8_t |   Cell 9 temperature    |  round to units  | temperature              |
|    1    |  uint8_t |   Cell 10 temperature   |                  | temperature              |
|    2    |  uint8_t |   Cell 11 temperature   |                  | temperature              |
|    3    |  uint8_t |   Cell 12 temperature   |                  | temperature              |
|    4    |  uint8_t |   Cells internal        |                  | temperature              |
|    5    |  uint8_t |   Not Used              |                  | temperature              |
|    6    |  uint8_t |   Not Used              |                  | temperature              |
|    7    |  uint8_t |   Not Used              |                  | temperature              |

### temperatures2 (CAN ID: 0x651)
**LEN = 4**

|  Offset |   Type   |        Name       |     Scale        |        Description       |
|:-------:|:--------:|:-----------------:|:----------------:|:------------------------:|
|    0    |  uint8_t |   Ambient Temp    | round to units   | temperature              |
|    1    |  uint8_t |   HeatSink 1      |                  | temperature              |
|    2    |  uint8_t |   HeatSink 2      |                  | temperature              |
|    3    |  uint8_t |   LTC temp        |                  | temperature              |

<a name="FOILS_MSG"/>

## Foils Messages:
### AHRSData0 (CAN ID: 0x602)
**LEN = 8**

|  Offset |    Type    |        Name       |          Scale           |        Description             |
|:-------:|:----------:|:-----------------:|:------------------------:|:------------------------------:|
|    0    |  float16   | veloc             | 			1e2			  |  Boat velocity in m/s          |
|    2    |  float16   | pitch             | 			1e2			  |  Boat Pitch in deg             |
|    4    |  float16   | roll              | 			1e2		      |  Boat Roll in deg              |
|    6    |  float16   | yaw               | 			1e2	   	      |  Boat Yaw in deg               |


### AHRSData1 (CAN ID: 0x60A)
**LEN = 8**

|  Offset |    Type    |        Name       |          Scale           |                  Description                  |
|:-------:|:----------:|:-----------------:|:------------------------:|:---------------------------------------------:|
|    0    |  float32   | lat               | 						  |  Boat Latitude in decimal degrees format (DD) |
|    4    |  float32   | lon               | 						  |  Boat Longitud in decimal degrees format (DD) |



### US_angle_state_mode (CAN ID: 0x612)
**LEN = 8**

|  Offset |    Type    |        Name       |          Scale           |        Description             |
|:-------:|:----------:|:-----------------:|:------------------------:|:------------------------------:|
|    0    |  float16   | dist              | 			1e2			  |  Distance to water in meters   |
|    2    |  float16   | left_angle        | 			1e2			  |  Angle of the Left foil        |
|    4    |  float16   | right_angle       | 			1e2		      |  Angle of the Right foil       |
|    6    |  uint8_t   | state             | 				   	      |  Controller state              |
|    7    |  uint8_t   | mode              | 				   	      |  Controller mode               |

### GyroX_GyroY_backangle_setpoints (CAN ID: 0x61A)
**LEN = 8**

|  Offset |    Type    |        Name       |          Scale           |           Description            |
|:-------:|:----------:|:-----------------:|:------------------------:|:--------------------------------:|
|    0    |  float16   | gyroX             | 			1e2			  |  Rate of turn around X in rad/s  |
|    2    |  float16   | gyroY             | 			1e2			  |  Rate of turn around Y in rad/s  |
|    4    |  float16   | back_angle        | 			1e2			  |  Angle of the Rear foil          |
|    6    |  uint8_t   | Height Reference  | 			1e0		    |  Height Reference used by the controller in Heave or Full mode |
|    7    |  int8_t    | Angle Reference   | 			1e1		    |  Front Foils Angle Reference |

<a name="MOTOR_MSG"/>

## Motor Controller PCB Messages:
Scale is scale argument you need to pass to the  decoding function see: **[buffer.c](https://gitlab.com/tecnico.solar.boat/2020/electrical-systems/-/blob/main/Code/Libraries/CAN_TSB_Library/buffer.cpp)**
### motor1_current (CAN ID: 0x603)
**LEN = 8**

|  Offset |    Type    |        Name       |          Scale           |        Description         |
|:-------:|:----------:|:-----------------:|:------------------------:|:--------------------------:|
|    0    |  float     |  motor_current    |			1e2		      | Motor 1 Current 	       |
|    4    |  float     |  input_current    |            1e2           | VESC Motor 1 Input Current |


### motor1_duty_rpm_voltage (CAN ID: 0x60B)
**LEN = 8**

|  Offset |    Type    |        Name       |          Scale           |        Description         |
|:-------:|:----------:|:-----------------:|:------------------------:|:--------------------------:|
|    0    |  float     |  duty_cycle   	   |			1e3           | VESC Motor 1 duty cycle    |
|    2    |  float     |  erpm    		   |            1e0           | Motor 1 erpm      		   |
|    6    |  float     |  voltage 	       |            1e1           | VESC Motor 1 input voltage |


### motor1_temperatures (CAN ID: 0x613)
**LEN = 8**

|  Offset |    Type    |        Name       |          Scale           |        Description         |
|:-------:|:----------:|:-----------------:|:------------------------:|:--------------------------:|
|    0    |  float     | temp_motor		     |		      	1e1           | Motor 1 Temperature        |
|    2    |  float     | temp_mos_1    	   |            1e1           | VESC Motor 1 Mos 1 Temp	   |
|    4    |  float     | temp_mos_2        |            1e1           | VESC Motor 1 Mos 2 Temp    |
|    6    |  float     | temp_mos_3	       |            1e1           | VESC Motor 1 Mos 3 Temp    |


### motor1_fault_id_asked_current (CAN ID: 0x61B)
**LEN = 6**

|  Offset |    Type    |        Name       |          Scale           |        Description         |
|:-------:|:----------:|:-----------------:|:------------------------:|:--------------------------:|
|    0    |  uint8_t   | fault 			       |					                | VESC Motor 1 Fault code    |
|    1    |  uint8_t   | id          	     |                          | VESC Motor 1 id      	     |
|    2    |  int16_t   | asked_current     |                          | Throttle asked current     |
|    4    |  int16_t   | pid_speed         |            1e1           | Throttle PID speed         |


### motor2_current (CAN ID: 0x623)
**LEN = 8**

|  Offset |    Type    |        Name       |          Scale           |        Description         |
|:-------:|:----------:|:-----------------:|:------------------------:|:--------------------------:|
|    0    |  float     |  motor_current    |			1e2     	  | Motor 2 Current 	       |
|    4    |  float     |  input_current    |            1e2           | VESC Motor 2 Input Current |


### motor2_duty_rpm_voltage (CAN ID: 0x62B)
**LEN = 8**

|  Offset |    Type    |        Name       |          Scale           |        Description         |
|:-------:|:----------:|:-----------------:|:------------------------:|:--------------------------:|
|    0    |  float     |  duty_cycle   	   |			1e3     	  | VESC Motor 2 duty cycle    |
|    2    |  float     |  erpm    		   |            1e0           | Motor 2 erpm      		   |
|    6    |  float     |  voltage 	       |            1e1           | VESC Motor 2 input voltage |


### motor2_temperatures (CAN ID: 0x633)
**LEN = 8**

|  Offset |    Type    |        Name       |          Scale           |        Description         |
|:-------:|:----------:|:-----------------:|:------------------------:|:--------------------------:|
|    0    |  float     | temp_motor		   |			1e1           | Motor 2 Temperature        |
|    2    |  float     | temp_mos_1    	   |            1e1           | VESC Motor 2 Mos 1 Temp	   |
|    4    |  float     | temp_mos_2        |            1e1           | VESC Motor 2 Mos 2 Temp    |
|    6    |  float     | temp_mos_3	       |            1e1           | VESC Motor 2 Mos 3 Temp    |


### motor2_fault_id (CAN ID: 0x63B)
**LEN = 2**

|  Offset |    Type    |        Name       |          Scale           |        Description         |
|:-------:|:----------:|:-----------------:|:------------------------:|:--------------------------:|
|    0    |  uint8_t   | fault 			   |					      | VESC Motor 2 Fault code    |
|    1    |  uint8_t   | id          	   |                          | VESC Motor 2 id      	   |


<a name="CANWIFI_MSG"/>

## CAN WiFi Messages:
### time (CAN ID: 0x0C)
**LEN = 4**

|  Offset |   Type   |        Name       |          Scale           |        Description       |
|:-------:|:--------:|:-----------------:|:------------------------:|:------------------------:|
|    0    | uint32_t |  time             | 						    | seconds since 1/1/1970   |


<a name="SCREEN_MSG"/>

## Screen Messages:
### current_threshold (CAN ID: 0x605)
**LEN = 5**

|  Offset |   Type   |        Name       |          Scale           |        Description             |
|:-------:|:--------:|:-----------------:|:------------------------:|:------------------------------:|
|    0    |  int16_t |   current_threshold    | 						     | Dual motor current threshold  |
|    2    |  float16 |   pid_speed_reference  |       1e1        | Speed reference for the speed pid controller |
|    4    |  uint8_t |   pid_state            |       1e0        | PID ON / OFF   |

### new_sd_card_file (CAN ID: 0x60D)
Content does not matter, just send message with empty buffer.

### foils_controller (CAN ID: 0x615)
**LEN = 6**

|  Offset |   Type   |           Name          |     Scale        |        Description       |
|:-------:|:--------:|:-----------------------:|:----------------:|:------------------------:|
|    0    |  uint8_t |   controller_state      |                  | Controller ON / OFF      |
|    1    |  uint8_t |   homeing               |                  | Homing                   |
|    2    |  uint8_t |   mode_setpoint         |                  | Controller Mode          |
|    3    |  int8_t  |   angle_setpoin         |        1e1       | Front Foils Angle        |
|    4    |  uint8_t |   dist_setpoint         |                  | Boat hight               |
|    5    |  uint8_t |   back_foil_state       |        1e0       | Rear Foil State: 0 - Manual ; 1 - Auto (Active) |

### toogle_solar_relay (CAN ID: 0x61D)
Content does not matter, just send message with empty buffer.


<a name="SHUNT_MSG"/>

## IVT-S Current Shunt (Isabellenhütte) Messages:

For more information regarding this sensor messages check [this documents](https://gitlab.com/tecnico.solar.boat/2020/electrical-systems/-/blob/main/Datasheets/Isabellenhutte%20-%20IVT-Series%20Current%20Shunt/Datasheet_IVT-S.pdf). Be aware that we are not using the defualt CAN ID's shown on Isabellenhütte documents.

### IVT_Msg_Command (CAN ID: 0x406)
**LEN = 8**

Function commands, SET and GET commands A command-ID-byte is included for identification.

### IVT_Msg_Debug (CAN ID: 0x510)
**LEN = 8**

The ID of this message cannot be changed and it is reserved for Isabellenhütte internal use. So we should not use this id in our messages.

### IVT_Msg_Response (CAN ID: 0x606)
**LEN = 8**

Response to SET and GET command messages A response-ID-byte is included for identification.

### IVT_Msg_Result_I (CAN ID: 0x60E)
**LEN = 6**

Current messurment in mA.

### IVT_Msg_Result_U1 (CAN ID: 0x616)
**LEN = 6**

Voltage 1 in mV. **This is the voltage channel used to compute the Power and the Energy.**

### IVT_Msg_Result_U2 (CAN ID: 0x61E)
**LEN = 6**

Voltage 2 in mV.

### IVT_Msg_Result_U3 (CAN ID: 0x626)
**LEN = 6**

Voltage 3 in mV.

### IVT_Msg_Result_T (CAN ID: 0x62E)
**LEN = 6**

Temperature in ºC the scale is 0.1 ºC.

### IVT_Msg_Result_W (CAN ID: 0x636)
**LEN = 6**

Power (refering to current and voltage U1) in W.

### IVT_Msg_Result_As (CAN ID: 0x63E)
**LEN = 6**

Current counter in As.

### IVT_Msg_Result_Wh (CAN ID: 0x646)
**LEN = 6**

Energy counter (refering to current and voltage U1) in Wh.

**All IVT_Msg_Result messages share the following structure:**

<img src="/Auxiliary%20Files/IVT-S_Results.png"  width="60%">

MuxID description for IVT_Msg_Result:

<img src="/Auxiliary%20Files/IVT-S_MuxID.png"  width="40%">


---

<a name="Código"/>

## Code

- **[CAN_TSB](https://gitlab.com/tecnico.solar.boat/2020/electrical-systems/-/tree/main/Code/Libraries/CAN_TSB_Library)**


#### Requirements

- **[VSCode](https://code.visualstudio.com/)** ou **[Atom](https://atom.io/)**
- **[Platformio](https://platformio.org/)**
- **[Collin80's Flexcan](https://github.com/collin80/FlexCAN_Library)**


### Example

```c++
TSB_CAN canMessageHandler;
CAN_message_t to_send; // One for each CAN ID that needs to be sent 

void initCAN_Messages(){ // Inicialise the message fixed parameters, do it for all messages
  to_send.ext = 0;
  to_send.id = 0x0C;
  to_send.len = 4;
}

void sendCAN_Messages(){ // Call this function preodically to send the messages use IntervalTimer
  // Lets show how it works for sending the time
  unsigned long time_now = now(); // Get current time
  int32_t ind = 0; 
  buffer_append_uint32(to_send.buf, time_now, &ind); // Populate the message data 
  Can0.write(to_send); // Send message (call this for every message to be sent)
}

void setup(void){
  // Setup the CAN
  initCAN_Messages(); // Call the function to inicialise all you CAN Messages
  Can0.begin(1000000); // Inicialise CAN BUS with 1 Mb/s speed
  Can0.attachObj(&canMessageHandler);
  canMessageHandler.attachGeneralHandler();
}

void loop(void)
{
  // If you want to print something that is avalible on the CAN Bus check the CAN_TSB.h to see the structer used.
  // As an example lets print the battery current
  Serial.println(canMessageHandler.bms.current);
}
```

---

