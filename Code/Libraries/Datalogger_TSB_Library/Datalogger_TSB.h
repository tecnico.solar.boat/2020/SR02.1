/*
    Copyright (C) 2020  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#ifndef _DATALOGGER_TSB_H_
#define _DATALOGGER_TSB_H_


#include <Arduino.h>
#include <TimeLib.h>
#include <SdFat.h>



class TSB_SD{
    private:
        #if defined(__MK64FX512__) || defined(__MK66FX1M0__) // For Teensy 3.5/3.6 use faster SdFatSdioEX
            #define useEX true
            SdFatSdioEX sdEx;
        #else // For others use SdFat
            #define useEX false
            SdFat sd;
        #endif
    public:
        bool sd_insert = true;
        //TSB_SD();
        #if defined(__MK64FX512__) || defined(__MK66FX1M0__) // For Teensy 3.5/3.6 use faster SdFatSdioEX
            /**
             * TSB_SD constructor for Teensy 3.5/3.6 since these Teensy models have a builtin SD Card
             * slot there is no need to specify wihic pins should be used.
             */
            TSB_SD();
        #else // For others use SdFat
            /**
             * TSB_SD constructor for the Teensy 3.2
             * 
             * @param  {uint8_t} SCK=13         : Specifies the Teensy SPI SCK pin used, which by default is the PIN 13
             * @param  {uint8_t} CHIP_SELECT=SS : Specifies the SPI CS pin for the SD Card, which by default is the SS PIN aka PIN 10
             */
            TSB_SD(uint8_t SCK=13, uint8_t CHIP_SELECT=SS);
        #endif
};

class TSB_Datalogger{
    private:
        String logname;
        String System_Name;
        String timeString;
        String dateString;
        String header;
        int entryId = 0;
        SdFile file;
        bool sd_insert;
        /**
         * Inicializes the file name varible in the class
         */
        void initFileName();
        /**
         * Retrives the current time from the Teensy RTC
         */
        void getTime();
        /**
         * Declared to be used as the argumment of the setSyncProvider Teensy builtin function
         * @return {time_t}  : Current time in time_t fomrat
         */
        static time_t getTeensy3Time();

    public:
        /**
         * TSB_Datalogger constructor
         * 
         * @param  {String} system_name : specifies the system name, it will be the prefix of the file name
         * @param  {bool} sd_insert     : variable to tell if the SD Card is really inserted it comes from the TSB_SD class
         */
        TSB_Datalogger(String system_name, bool sd_insert);
        /**
         * Inicializes the CSV file, this should only be called once
         * 
         * @param  {String} header : specifies the firs line of the CSV file and thus it's format
         */
        void initLog(String header);
        /**
         * Writes the data to the file, this function should be calles preiodically with an IntervalTimer
         * so that the data gests logged.
         * 
         * @param  {String} data : String to be added to the file, this string should 
         * alredy include the necessary semicolons so that the CSV file is properlly formated 
         */
        void writeToFile(String data);
        /**
         * This functions simply creates a new file, to be used for example when the boat enters on the water,
         * via a button press on the LCD.
         */
        void createNewFile();

};


#endif