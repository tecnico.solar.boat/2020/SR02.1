/*
    Copyright (C) 2020  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/
var fault_id = getVariableValue("motor1_fault");

switch(fault_id) {
    case 0:
        setVariableValue("motor1_fault_string","No fault");
        break;
    case 1:
        setVariableValue("motor1_fault_string","Over Voltage");
        break;
    case 2:
        setVariableValue("motor1_fault_string","Under Voltage");
        break;
    case 3:
        setVariableValue("motor1_fault_string","DRV");
        break;
    case 4:
        setVariableValue("motor1_fault_string","ABS Over Current");
        break;
    case 5:
        setVariableValue("motor1_fault_string","Temperature Mosfet");
        break;
    case 6:
        setVariableValue("motor1_fault_string","Temperature Motor");
        break;
    case 7:
        setVariableValue("motor1_fault_string","Gate Diver Over Voltage");
        break;
    case 8:
        setVariableValue("motor1_fault_string","Gate Diver Under Voltage");
        break;
    case 9:
        setVariableValue("motor1_fault_string","MCU Under Voltage");
        break;
    case 10:
        setVariableValue("motor1_fault_string","Booting from watchdog reset");
        break;
    case 11:
        setVariableValue("motor1_fault_string","Encoder SPI");
        break;
    case 12:
        setVariableValue("motor1_fault_string","Encoder SINCOS Below MIN Amplitude");
        break;
    case 13:
        setVariableValue("motor1_fault_string","Encoder SINCOS Above MIN Amplitude");
        break;
    case 14:
        setVariableValue("motor1_fault_string","Flash Corruption");
        break;
    case 15:
        setVariableValue("motor1_fault_string","High OFFSET Current Sensor 1");
        break;
    case 16:
        setVariableValue("motor1_fault_string","High OFFSET Current Sensor 2");
        break;
    case 17:
        setVariableValue("motor1_fault_string","High OFFSET Current Sensor 3");
        break;
    case 18:
        setVariableValue("motor1_fault_string","Unbalanced Currents");
        break;
    default:
        setVariableValue("motor1_fault_string","Undefined Fault");
        break;
}