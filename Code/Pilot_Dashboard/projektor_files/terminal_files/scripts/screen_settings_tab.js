/*
    Copyright (C) 2020  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/
var display_backlight = getVariableValue("display_backlight");

var buttons_backlight = getVariableValue("backlight_mode");

setVariableValue("@DispBacklightIntensity",display_backlight);

if(buttons_backlight) setVariableValue("@KeybBacklightIntensity",100);
else setVariableValue("@KeybBacklightIntensity",0);