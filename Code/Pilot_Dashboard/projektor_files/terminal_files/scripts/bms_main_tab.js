/*
    Copyright (C) 2020  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/
//Status Decode
var charging = getVariableValue("charging");
var discharging = getVariableValue("discharging");
var regenerating = getVariableValue("regenerating");
var balancing = getVariableValue("balancing");

if(charging) setVariableValue("status_string","Charging");
if(discharging) setVariableValue("status_string","Discharging");
if(regenerating) setVariableValue("status_string","Regenerating");
if(balancing) setVariableValue("status_string","Balancing");

if(!(charging || discharging || regenerating || balancing)) setVariableValue("status_string","OFF");

//max cell temperature calculation
var cell_temperatures = [];
var aux_string = "battery_cell_temperature";

for(i = 0; i < 12; i++){
    cell_temperatures[i] = getVariableValue(aux_string + String(i+1));
}

var max_cell_temperature = Math.max.apply(Math, cell_temperatures);
setVariableValue("max_cell_temperature",max_cell_temperature);
