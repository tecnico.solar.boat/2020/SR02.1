/*
    Copyright (C) 2020  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/
var current_threshold = getVariableValue("screen_current_threshold");
var pid_speed = getVariableValue("screen_pid_speed");
var pid_state = getVariableValue("screen_pid_state");

if(pid_state===1){
    setVariableValue("screen_pid_state",0);
    pid_state=0;
}else if(pid_state===0){
    setVariableValue("screen_pid_state",1);
    pid_state=1;
}

var result = sendCANMessage(1, 0x605, 5, (current_threshold >> 8) & 0xFF, current_threshold & 0xFF, (pid_speed >> 8) & 0xFF, pid_speed & 0xFF, pid_state);

if (result){
    setVariableValue("@AlarmShow",0x8002);
    setVariableValue("sent_message_info_string","CAN message sent successfully!");
}
else{
    setVariableValue("@AlarmShow",0x8002);
    setVariableValue("sent_message_info_string","Failed to send CAN message!");
}

