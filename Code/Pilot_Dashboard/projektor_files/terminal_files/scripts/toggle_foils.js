/*
    Copyright (C) 2020  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/
var angle = getVariableValue("screen_foils_angle_setpoint");
var water_distance = getVariableValue("screen_foils_water_distance_setpoint");
var backfoil_state = getVariableValue("screen_foils_backfoil_state");

setVariableValue("screen_foils_controller_state",0x01);
var result = sendCANMessage(1, 0x615, 6, 0x01, 0x00, 0x00, angle, water_distance, backfoil_state);
setVariableValue("screen_foils_controller_state",0x00);

if (result){
    setVariableValue("@AlarmShow",0x8002);
    setVariableValue("sent_message_info_string","CAN message sent successfully!");
}
else{
    setVariableValue("@AlarmShow",0x8002);
    setVariableValue("sent_message_info_string","Failed to send CAN message!");
}

