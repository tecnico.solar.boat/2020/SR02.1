/*
    Copyright (C) 2020  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/
// Here you can add your JavaScript code.

//time routine
var time = getVariableValue("time");
time = time + 1;
setVariableValue("time",time);

//energy routine
var joules = getVariableValue("joules");
var voltage = getVariableValue("battery_voltage") * 0.002;
var current = getVariableValue("battery_current") * 0.1;
var energy = getVariableValue("energy");

joules = joules + (voltage * current);
energy = (joules / 36000);
setVariableValue("energy",energy);
setVariableValue("joules",joules);

var over_current  = getVariableValue("over_current");
var over_voltage = getVariableValue("over_voltage");
var over_temperature = getVariableValue("over_temperature");
var under_voltage = getVariableValue("under_voltage");
var over_current_minimized = getVariableValue("over_current_minimized");
var over_voltage_minimized = getVariableValue("over_voltage_minimized");
var under_voltage_minimized = getVariableValue("under_voltage_minimized");
var over_temperature_minimized = getVariableValue("over_temperature_minimized");
var alarms_active = getVariableValue("alarms_active");

//Alarms
if(over_current && !over_current_minimized){ 
    setVariableValue("current_warning_string","Over Current");
    setVariableValue("@AlarmShow",0x8001);
    setVariableValue("over_current_minimized",1);
}else if(!over_current){
    setVariableValue("current_warning_string","");
    setVariableValue("over_current_minimized",0);
}
   
if(over_voltage && !over_voltage_minimized){ 
    setVariableValue("voltage_warning_string","Over Voltage");
    setVariableValue("@AlarmShow",0x8001);
    setVariableValue("over_voltage_minimized",1);
}else if(!over_voltage){
    if(!under_voltage) setVariableValue("voltage_warning_string","");
    setVariableValue("over_voltage_minimized",0);
}

if(under_voltage && !under_voltage_minimized){ 
    setVariableValue("voltage_warning_string","Under Voltage");
    setVariableValue("@AlarmShow",0x8001);
    setVariableValue("under_voltage_minimized",1);
}else if(!under_voltage){
    if(!over_voltage) setVariableValue("voltage_warning_string","");
    setVariableValue("under_voltage_minimized",0);
}

if(over_temperature && !over_temperature_minimized){ 
    setVariableValue("temperature_warning_string","Over Temperature"); 
    setVariableValue("@AlarmShow",0x8001);
    setVariableValue("over_temperature_minimized",1);
}else if(!over_temperature){
    setVariableValue("temperature_warning_string","");
    setVariableValue("over_temperature_minimized",0);
}

if(over_current || over_voltage || under_voltage || over_temperature){
    setVariableValue("alarms_active",1);
}else{
    setVariableValue("alarms_active",0);
    setVariableValue("@AlarmShow",0x01);
}
