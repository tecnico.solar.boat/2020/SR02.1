/*
    Copyright (C) 2020  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/
var state = getVariableValue("foils_state");
var mode = getVariableValue("foils_mode");

switch(state){
    case 0:
        setVariableValue("foils_state_string","OFF");
        break;
    case 1:
        setVariableValue("foils_state_string","HOMING");
        break;
    case 2:
        setVariableValue("foils_state_string","READY");
        break;
    case 3:
        setVariableValue("foils_state_string","ON");
        break;
    case 4:
        setVariableValue("foils_state_string","ERROR");
        break;
    default:
        setVariableValue("foils_state_string","Undefined State");
        break;
}

switch(mode){
    case 0:
        setVariableValue("foils_mode_string","MANUALMODE");
        break;
    case 1:
        setVariableValue("foils_mode_string","ROLLMODE");
        break;
    case 2:
        setVariableValue("foils_mode_string","HEAVEMODE");
        break;
    case 3:
        setVariableValue("foils_mode_string","FULLMODE");
        break;
    default:
        setVariableValue("foils_mode_string","Undefined Mode");
        break;
}
