/*
    Copyright (C) 2020  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include <Arduino.h>
// Correntes estão mal
/*
Status:
  Charging
  Balancing
  Regenerating
  Motor, Solar, Charger - Closed
LTC Status:
  Nao esta a enviar
Cells Ballancing:
  1,2,9,10,11,12
Warning:
  Over Current
  Over Temperature
  Under Voltage
*/
byte status[] = {0xAC, 0x01,0x01, 0x06, 0x7D, 0x00, 0x00, 0x03, 0x0F ,0x0B, 0x7C, 0xAD};

/*
SOC: 65.32
Voltage: 45.122
Current: 30 A
Solar Current: 30.1 A
*/
byte SOC_Voltage_Currents[] = {0xAC, 0x01,0x02, 0x08, 0x19,0x84, 0x58,0x21, 0x01,0x2F, 0x01,0x2D, 0xED, 0xAD};

/*
SOC: 65.32
Voltage: 45.122
Current: -201.5 A
Solar Current: 30.1 A
*/
//byte SOC_Voltage_Currents[] = {0xAC, 0x01,0x02, 0x08, 0x19,0x84, 0x58,0x21, 0xF8,0x21, 0x01,0x2D, 0x1A, 0xAD};

/*
3,5670
3,5671
3,5672
3,5673
*/
byte cellVoltage0[] = {0xAC, 0x01,0x03, 0x08, 0x8B,0x56, 0x8B,0x57, 0x8B,0x58, 0x8B,0x59, 0x0A, 0xAD};
/*
3,5670
3,5671
3,5672
3,5673
*/
byte cellVoltage1[] = {0xAC, 0x01,0x04, 0x08, 0x8B,0x56, 0x8B,0x57, 0x8B,0x58, 0x8B,0x59, 0x0D, 0xAD};
/*
3,5670
3,5671
3,5672
3,5673
*/
byte cellVoltage2[] = {0xAC, 0x01,0x05, 0x08, 0x8B,0x56, 0x8B,0x57, 0x8B,0x58, 0x8B,0x59, 0x0C, 0xAD};

/* 22,123 22,124 22,125 22,126 22,127 */
byte SolarPanelVoltages0[] = {0xAC, 0x01,0x06, 0x08, 0x56,0x6B, 0x56,0x6C, 0x56,0x6D, 0x56,0x6E, 0x0B, 0xAD};
byte SolarPanelVoltages1[] = {0xAC, 0x01,0x07, 0x02, 0x56,0x6F, 0x3D ,0xAD};

/* Cells: 17 18 .... 28
    Cells Internal: 29
    Ambient: 17
    Heat Sink 1: 18
    Heat Sink 2: 19
    LTC: 20
*/
byte temperatures0[] = {0xAC, 0x01,0x08, 0x08, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x09, 0xAD};
byte temperatures1[] = {0xAC, 0x01,0x09, 0x08, 0x19, 0x1A, 0x1B, 0x1C, 0x1D, 0x00, 0x00, 0x00, 0x19, 0xAD};
byte temperatures2[] = {0xAC, 0x01,0x0A, 0x04, 0x11, 0x12, 0x13, 0x14, 0x0B, 0xAD};

/*
Over Voltage cutoff: 4,2
Under Voltage Cutoff: 3
Fully charged voltage: 4,2
Fully Discharged: 3
Discharge over current: 33.6
Charge over current: 300
Over heat: 50
Allowed disbalancing 50
Minimum cell for balance: 3.5
Allow ballancing: False
*/
byte values1[] = {0xAC, 0x01, 0x0C, 0x08, 0xA4, 0x10, 0x75, 0x30, 0x88, 0xB8, 0x01, 0xF4, 0x31, 0xAD};
byte values2[] = {0xAC, 0x01, 0x0D, 0x04, 0xA4, 0x10, 0x75, 0x30, 0xF9, 0xAD};
byte values3[] = {0xAC, 0x01, 0x0E, 0x06, 0x01, 0x50, 0x0B, 0xB8, 0x32, 0x00, 0xD9, 0xAD};


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  delay(5000);
}

void loop() {
  // put your main code here, to run repeatedly:
  
  // Send status
  for(size_t i = 0; i < sizeof(status); i++)
  {
    Serial.write(status[i]);
  }
  delay(10); // Be gentil for testing
  
  // Send SOC_Voltage_Currents
  for(size_t i = 0; i < sizeof(SOC_Voltage_Currents); i++)
  {
    Serial.write(SOC_Voltage_Currents[i]);
  }
  delay(10); // Be gentil for testing

  // Send cellVoltage0
  for(size_t i = 0; i < sizeof(cellVoltage0); i++)
  {
    Serial.write(cellVoltage0[i]);
  }
  delay(10); // Be gentil for testing
  
  // Send cellVoltage1
  for(size_t i = 0; i < sizeof(cellVoltage1); i++)
  {
    Serial.write(cellVoltage1[i]);
  }
  delay(10); // Be gentil for testing
  
  // Send cellVoltage2
  for(size_t i = 0; i < sizeof(cellVoltage2); i++)
  {
    Serial.write(cellVoltage2[i]);
  }
  delay(10); // Be gentil for testing
  
  // Send SolarPanelVoltages0
  for(size_t i = 0; i < sizeof(SolarPanelVoltages0); i++)
  {
    Serial.write(SolarPanelVoltages0[i]);
  }
  delay(10); // Be gentil for testing

  // Send SolarPanelVoltages0
  for(size_t i = 0; i < sizeof(SolarPanelVoltages1); i++)
  {
    Serial.write(SolarPanelVoltages1[i]);
  }
  delay(10); // Be gentil for testing

  // Send temperatures0
  for(size_t i = 0; i < sizeof(temperatures0); i++)
  {
    Serial.write(temperatures0[i]);
  }
  delay(10); // Be gentil for testing

  // Send temperatures1
  for(size_t i = 0; i < sizeof(temperatures1); i++)
  {
    Serial.write(temperatures1[i]);
  }
  delay(10); // Be gentil for testing

  // Send temperatures2
  for(size_t i = 0; i < sizeof(temperatures2); i++)
  {
    Serial.write(temperatures2[i]);
  }
  delay(10); // Be gentil for testing

  // Send values1
  for(size_t i = 0; i < sizeof(values1); i++)
  {
    Serial.write(values1[i]);
  }
  delay(10); // Be gentil for testing

  // Send values2
  for(size_t i = 0; i < sizeof(values2); i++)
  {
    Serial.write(values2[i]);
  }
  delay(10); // Be gentil for testing

  // Send values3
  for(size_t i = 0; i < sizeof(values3); i++)
  {
    Serial.write(values3[i]);
  }
  delay(10); // Be gentil for testing


  delay(1000); // Be gentil for testing
}