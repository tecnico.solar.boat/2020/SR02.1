# BMS GUI

This is the code for the BMS GUI.

Copyright (C) 2020  Técnico Solar Boat

This repository and its contents  is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.
The content of this repository is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/.
You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
or via our facebook page at https://fb.com/tecnico.solarboat


⚠️ This GUI code is for the [BMS V1 PCB](https://gitlab.com/tecnicosb/tsb-es/2019/bms), the one that connects to the battery with Pogo Pins and has 90º degress corners, and with the [BMS Wired V1 PCB](https://gitlab.com/tecnicosb/tsb-es/2021/bms-wired-v1)  ⚠️


To include the libraries inside the application package on macOS, after compiling the app with QT, one should go to Terminal and do the following command:

```
cd /path/to/build/directory
macdeployqt BMS_GUI.app
```


