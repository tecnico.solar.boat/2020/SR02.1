/*
    Copyright (C) 2020  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#ifndef LQR_H
#define LQR_H

#define MAX_COM_FOILANGLE 4
#define MIN_COM_FOILANGLE 0
#define DIF_FOILANGLE 3
#define MAX_MOTOR_POSITION 310
#define MIN_MOTOR_POSITION 15
#define ZSEARCH_CURRENT 500
#define ZSEARCH_RPM 30
#define MAX_CURRENT 3000
#define MAX_RPM 2000

#define MAX_REAR_FOILANGLE 5
#define MIN_REAR_FOILANGLE 0
#define MAX_REARMOTOR_POSITION -10
#define MIN_REARMOTOR_POSITION -150
#define DEFAULT_REARANGLE 3
#define ZSEARCH_REARCURRENT 1200
#define ZSEARCH_REARRPM 100
#define MAX_REARCURRENT 1200
#define MAX_REARRPM 500

#define FOILS_START_SPEED 2 //basically roll start speed
#define HEAVE_START_SPEED 4
#define HEAVE_STOP_SPEED 3.5
#define FOILS_STOP_SPEED 2 //basically roll stop speed

#define DEFAULT_ANGLE 2.5
#define INITIAL_AOA_HEAVE 3.0 //initial aoa for heave controller (integrator output)

#define DIST_US_TO_CM 150 //distance between ultrasonic and center of mass in centimeters

#define CONTROL_RATE 0.05 //in s

#define ROLLREFGAIN 0.6
#define EMA_ALPHA 0.2
#define MAX_ROLL 5


#include "Xsens.h"
#include "Senix.h"
#include "PID_v1.h"

extern XsensMsg AHRS;

class LQRController
{
    public:
        LQRController();

        float com_mode();
        float dif_mode();

        void full_mode();
        void heave_mode();
        void roll_mode();

        void reset();
        void reset_heave();

        float rads_to_degs(float rads);

        //foils angles
        volatile float angle_left;
	    volatile float angle_right;
        float angle_rear;

        volatile float heave;
        float h_ref;
        float roll_reference;
        float boat_speed;
        float yawrate_filt;


        volatile float auto_com_angle;
        volatile float auto_dif_angle;

        bool ready;
        bool foils_enabled;
        bool heave_enabled;
        bool flag_first_loop;

        bool homing_done;

        uint8_t mode;
        uint8_t state;

        bool ultrasonic_connected;


    private:

        //globals for the com_mode controller
        float KX_PREV;
        float KZ_PREV;
        float ALFA;
        float SATURATION;
        float LAST_TIME_COM;
        float Kh[6];
        float K_sat_com;

        //globals for the dif_mode controller
        float KROLL_PREV;
        float KROLLDOT_PREV;
        float ALFA_DIF;
        float SATURATION_DIF;
        float LAST_TIME_DIF;
        float Kr[3];
        float K_sat_dif;

};




#endif
