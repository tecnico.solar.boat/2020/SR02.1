/*
    Copyright (C) 2020  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include "LQR.h"

LQRController::LQRController(){

	//globals for the com_mode controller
	KX_PREV = 0.0;
	KZ_PREV = 0.0;
	ALFA = INITIAL_AOA_HEAVE; // initial condition of integrator
	SATURATION = 0.0;
	LAST_TIME_COM = 0.0;

	Kh[0] = 5.3; //pitch
	Kh[1] = 0.02;  //gyro
	Kh[2] = 7.7; //speed
	Kh[3] =-8.9; //zspeed
	Kh[4] =-14; //heave (ultrasonic)
	Kh[5] =-10; //error
	K_sat_com = 1; //saturation

	//globals for the dif_mode controller
	KROLL_PREV = 0.0;
	KROLLDOT_PREV = 0.0;
	ALFA_DIF = 0.0;
	SATURATION_DIF = 0.0;
	LAST_TIME_DIF = 0.0;

	Kr[0]=0.8; //roll
	Kr[1]=0.25; //gyro
	Kr[2]=1; //error
	K_sat_dif = 0.5; //saturation

	angle_left=DEFAULT_ANGLE;
	angle_right=DEFAULT_ANGLE;
	angle_rear = DEFAULT_REARANGLE;

	heave=0;

	h_ref=0;
	roll_reference=0;
	yawrate_filt = 0;
	boat_speed = 0;

	ready=false;
	foils_enabled=false;
	heave_enabled=false;
	flag_first_loop = false;

	homing_done=false;

	auto_com_angle = 0.0;
	auto_dif_angle = 0.0;

	mode=0;
	state=0;

	ultrasonic_connected=false;

	

}

float LQRController::com_mode() {
	float com_foil=0.0;
    float Kz=0.0;
    float Kx=0.0;
    float Ke=0.0;
    float u_dot=0.0;
	float deriv_Kz=0.0;
	float deriv_Kx=0.0;
	
	float delta_t = (millis() - LAST_TIME_COM)/1000;	
	if (delta_t > 0.1) delta_t = 0.1;
	LAST_TIME_COM = millis();


	Kz = heave/100*Kh[4]; //heave values are negative (downpointing referencial) and in cm
	Kx = (-1)*AHRS.pitch*Kh[0] + (-1)*rads_to_degs(AHRS.gyrY)*Kh[1] + boat_speed*Kh[2] + (-1)*AHRS.velZ*Kh[3];
	Ke = (h_ref/100 - heave/100)*Kh[5];


	// calculate derivatives
	deriv_Kx = (Kx-KX_PREV)/delta_t;
	deriv_Kz = (Kz-KZ_PREV)/delta_t;

	if(flag_first_loop) { // for first loop ignore derivatives, they may be infinite
		deriv_Kx = 0;
		deriv_Kz = 0;
		flag_first_loop = false;
	}


	// calculate alfa
	u_dot = Ke - deriv_Kx - deriv_Kz - SATURATION*K_sat_com;
	ALFA = ALFA + u_dot * delta_t;  // alfa = int(u_dot);


	// saturation block
	if(ALFA > MAX_COM_FOILANGLE) {
	    com_foil=MAX_COM_FOILANGLE;
	}else if (ALFA < MIN_COM_FOILANGLE) {
	    com_foil=MIN_COM_FOILANGLE;
	}else {
	    com_foil=ALFA;
	}

	// update global variables
	SATURATION = ALFA - com_foil;
	KX_PREV = Kx;
	KZ_PREV = Kz;

	return com_foil;
}



float LQRController::dif_mode() {
	float dif_val=0;
    float Kroll=0;
    float Krolldot=0;
    float Ke=0;
    float u_dot=0;

	float delta_t = (millis() - LAST_TIME_DIF)/1000;	
	if (delta_t > 0.1) delta_t = 0.1;
	LAST_TIME_DIF = millis();

	//filter yaw rate and compute roll reference
	yawrate_filt = (rads_to_degs(AHRS.gyrZ)*EMA_ALPHA) + (1-EMA_ALPHA)*yawrate_filt;
	roll_reference = -ROLLREFGAIN*yawrate_filt;
	//limit roll ref saturation
	if(roll_reference > MAX_ROLL) {
	    roll_reference= MAX_ROLL;
	}else if (roll_reference < -MAX_ROLL) {
	    roll_reference= -MAX_ROLL;
	}
	
	// multiply gains
	Kroll = AHRS.roll*Kr[0];
	Krolldot = rads_to_degs(AHRS.gyrX)*Kr[1];
	Ke = (roll_reference - AHRS.roll)*Kr[2]; // reference = 0.0;


	// calculate alfa
	u_dot = Ke - (Kroll-KROLL_PREV)/delta_t - (Krolldot-KROLLDOT_PREV)/delta_t - SATURATION_DIF*K_sat_dif; // u = Ke - deriv(Kroll) - deriv(Krolldot) - sat;
	ALFA_DIF = ALFA_DIF + u_dot * delta_t;  // alfa = int(u_dot);

	// saturation block
	if(ALFA_DIF > DIF_FOILANGLE) {
	    dif_val= DIF_FOILANGLE;
	}else if (ALFA_DIF < -DIF_FOILANGLE) {
	    dif_val= -DIF_FOILANGLE;
	}else {
	    dif_val=ALFA_DIF;
	}

	// update global variables
	SATURATION_DIF = ALFA_DIF - dif_val;
	KROLL_PREV = Kroll;
	KROLLDOT_PREV = Krolldot;

	return dif_val;
}



void LQRController::full_mode(){
	auto_com_angle = com_mode();
	auto_dif_angle = dif_mode();
}

void LQRController::heave_mode(){
	auto_com_angle = com_mode();
}

void LQRController::roll_mode(){
	auto_dif_angle = dif_mode();
}


void LQRController::reset(){
	KX_PREV=0;
	KZ_PREV=0;
	SATURATION=0;
	ALFA=INITIAL_AOA_HEAVE;

	KROLL_PREV=0;
	KROLLDOT_PREV=0;
	SATURATION_DIF=0;
	ALFA_DIF=0;

	auto_com_angle = DEFAULT_ANGLE;
	auto_dif_angle = 0;

	angle_left=DEFAULT_ANGLE;
	angle_right=DEFAULT_ANGLE;

}

void LQRController::reset_heave(){
	KX_PREV=0;
	KZ_PREV=0;
	SATURATION=0;
	ALFA=INITIAL_AOA_HEAVE;

	auto_com_angle=DEFAULT_ANGLE;
}

float LQRController::rads_to_degs(float rads){
	return rads*180/PI;
}




