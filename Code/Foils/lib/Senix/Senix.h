/*
    Copyright (C) 2020  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#ifndef _Senix_h
#define _Senix_h

#include <Arduino.h>

#define SERIAL1_DIR	5
#define SenixSerial Serial1
#define TIMEOUT_SENSOR 80

uint16_t CRC16_check(uint8_t *nData, uint16_t wLength);
float readSensor(uint8_t sensor_id);
//float readSensor2();

extern uint8_t tx_msg1[];            // Reads distance from Sensor with addr 0x01
extern uint8_t tx_msg2[];            // Reads distance from Sensor with addr 0x02

extern float distleft;
extern float distright;



#endif