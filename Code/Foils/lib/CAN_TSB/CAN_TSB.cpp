/*
    Copyright (C) 2020  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/


#include "CAN_TSB.h"

int32_t ind = 0;

TSB_CAN::TSB_CAN()
{
	this->bms.cellvoltages = new float[12]();
	this->bms.solar_voltages = new float[5]();
	this->bms.temperatures = new float[17]();

	//initialize array of valid nodes
	for(int i=0; i<7; i++) this->received_time[i] = 0;
	//this->can_validate_time = 0;
	//this->validate_state = 0; //0 - put flags invalid 1- check if flags are invalid
}

void TSB_CAN::send_Message(uint32_t Id, uint8_t len, uint8_t *data)
{
	CAN_message_t msg;
	msg.ext = 0;
	msg.id = Id;
	msg.len = len;
	for(int i=0;i<msg.len;i++){
	msg.buf[i] = data[i];
	}
	Can0.write(msg);
}

void TSB_CAN::receive_Message(CAN_message_t &frame)
{
	//validate received node id into valid nodes array 
	if((frame.id & 7) >= 0 && (frame.id & 7) <= 6) this->received_time[frame.id & 7] = millis();

	switch(frame.id) {
		//*****************
		//****** BMS ******
		//*****************
		case 0x609:
			this->bms.status = frame.buf[0];
			this->bms.ltc_status = (frame.buf[1] << 8) | frame.buf[2];
			this->bms.cells_balanc = (frame.buf[4] << 8) | frame.buf[3];
			this->bms.warnings = frame.buf[5];
		break; 
		case 0x611:
			this->bms.soc = ((frame.buf[0]<<8) | frame.buf[1]) / 100.0;
			this->bms.voltage = (frame.buf[2]<<8) | frame.buf[3];
			this->bms.voltage = this->bms.voltage *20.0/10000.0;
			this->bms.current = (int16_t)((frame.buf[4]<<8) | frame.buf[5]) / 10.0;
			this->bms.solar_current = (int16_t)((frame.buf[6]<<8) | frame.buf[7]) / 10.0;
		break;
		case 0x619:
			for(int i=0; i<4; i++){
				this->bms.cellvoltages[i] = (uint16_t)((frame.buf[i*2]<<8) | frame.buf[i*2+1])/10000.0;
			}
		break;
		case 0x621:
			for(int i=0; i<4; i++){
				this->bms.cellvoltages[i+4] = (uint16_t)((frame.buf[i*2]<<8) | frame.buf[i*2+1])/10000.0;
			}
		break;
		case 0x629:
			for(int i=0; i<4; i++){
				this->bms.cellvoltages[i+8] = (uint16_t)((frame.buf[i*2]<<8) | frame.buf[i*2+1])/10000.0;
			}
		break;
		case 0x631:
			for(int i=0; i<4; i++){
				this->bms.solar_voltages[i] = (uint16_t)((frame.buf[i*2]<<8) | frame.buf[i*2+1])/1000.0;
			}
		break;
		case 0x639:
			this->bms.solar_voltages[4] = (uint16_t)((frame.buf[0]<<8) | frame.buf[1])/1000.0;
		break;
		case 0x641:
			for(int i=0; i<8; i++){
				this->bms.temperatures[i] = frame.buf[i];
			}
		break;
		case 0x649:
			for(int i=0; i<5; i++){
				this->bms.temperatures[i+8] = frame.buf[i];
			}
		break;
		case 0x651:
			for(int i=0; i<4; i++){
				this->bms.temperatures[i+13] = frame.buf[i];
			}
		break;


		//*********************
		//******* Foils *******
		//*********************
		case 0x602:
			ind = 0;
			this->foils.veloc = buffer_get_float16(frame.buf,1e2,&ind);
			this->foils.pitch = buffer_get_float16(frame.buf,1e2,&ind);
			this->foils.roll = buffer_get_float16(frame.buf,1e2,&ind);
			this->foils.yaw = buffer_get_float16(frame.buf,1e2,&ind);
		break;
		case 0x60A:
			ind = 0;
			this->foils.lat = buffer_get_float32_auto(frame.buf,&ind);
			this->foils.lon = buffer_get_float32_auto(frame.buf,&ind);
		break;
		case 0x612:
			ind = 0;
			this->foils.dist = buffer_get_float16(frame.buf,1e2,&ind);
			this->foils.left_angle = buffer_get_float16(frame.buf,1e2,&ind);
			this->foils.right_angle = buffer_get_float16(frame.buf,1e2,&ind);
			this->foils.state = frame.buf[ind++];
			this->foils.mode = frame.buf[ind++];
		break;
		case 0x61A:
			ind = 0;
			this->foils.gyroX = buffer_get_float16(frame.buf,1e2,&ind);
			this->foils.gyroY = buffer_get_float16(frame.buf,1e2,&ind);
			this->foils.back_angle = buffer_get_float16(frame.buf,1e2,&ind);
			this->foils.height_ref = frame.buf[ind++];
			this->foils.angle_ref = frame.buf[ind++]/10;
		break;


		//*********************
		//****** Motores ******
		//*********************
		case 0x603:
			ind = 0;
			this->motor1.motor_current = buffer_get_float32(frame.buf,1e2,&ind);
			this->motor1.input_current = buffer_get_float32(frame.buf,1e2,&ind);
		break;
		case 0x60B:
			ind = 0;
			this->motor1.duty_cycle = buffer_get_float16(frame.buf, 1e3, &ind);
			this->motor1.rpm = buffer_get_float32(frame.buf, 1e0, &ind);
			this->motor1.voltage = buffer_get_float16(frame.buf,1e1,&ind);
		break;
		case 0x613:
			ind = 0;
			this->motor1.temp_motor = buffer_get_float16(frame.buf, 1e1, &ind);
			this->motor1.temp_mos_1 = buffer_get_float16(frame.buf, 1e1, &ind);
			this->motor1.temp_mos_2 = buffer_get_float16(frame.buf, 1e1, &ind);
			this->motor1.temp_mos_3 = buffer_get_float16(frame.buf, 1e1, &ind);
			// This next message does not came from the CAN bus
			this->motor1.temp_mos_max = this->motor1.temp_mos_1;
			this->motor1.temp_mos_max = max(this->motor1.temp_mos_max, this->motor1.temp_mos_2);
			this->motor1.temp_mos_max = max(this->motor1.temp_mos_max, this->motor1.temp_mos_3);
		break;
		case 0x61B:
			this->motor1.fault = frame.buf[0];
			this->motor1.id = frame.buf[1];
			ind = 2;
			this->throttle.asked_current = buffer_get_int16(frame.buf, &ind);
			this->throttle.pid_speed = buffer_get_float16(frame.buf, 1e1, &ind);
		break;
		case 0x623:
			ind = 0;
			this->motor2.motor_current = buffer_get_float32(frame.buf,1e2,&ind);
			this->motor2.input_current = buffer_get_float32(frame.buf,1e2,&ind);
		break;
		case 0x62B:
			ind = 0;
			this->motor2.duty_cycle = buffer_get_float16(frame.buf, 1e3, &ind);
			this->motor2.rpm = buffer_get_float32(frame.buf, 1e0, &ind);
			this->motor2.voltage = buffer_get_float16(frame.buf,1e1,&ind);
		break;
		case 0x633:
			ind = 0;
			this->motor2.temp_motor = buffer_get_float16(frame.buf, 1e1, &ind);
			this->motor2.temp_mos_1 = buffer_get_float16(frame.buf, 1e1, &ind);
			this->motor2.temp_mos_2 = buffer_get_float16(frame.buf, 1e1, &ind);
			this->motor2.temp_mos_3 = buffer_get_float16(frame.buf, 1e1, &ind);
			// This next message does not came from the CAN bus
			this->motor2.temp_mos_max = this->motor2.temp_mos_1;
			this->motor2.temp_mos_max = max(this->motor2.temp_mos_max, this->motor2.temp_mos_2);
			this->motor2.temp_mos_max = max(this->motor2.temp_mos_max, this->motor2.temp_mos_3);
		break;
		case 0x63B:
			this->motor2.fault = frame.buf[0];
			this->motor2.id = frame.buf[1];
		break;
		

		//***********************
		//****** CAN WiFi  ******
		//***********************
		case 0x0C:
			{
				if (this->receivedTime == false){
					ind = 0;
					uint32_t time = buffer_get_uint32(frame.buf,&ind);
					// set the Time library to use Teensy 3.0's RTC to keep time
					setSyncProvider(getTeensy3Time);
					if (timeStatus()!= timeSet) {
						Serial.println("Unable to sync with the RTC");
					} 
					else {
						Serial.println("RTC has set the system time");
					}
					const signed long DEFAULT_TIME = 1357041600; // Jan 1 2013
					if( time < DEFAULT_TIME) { // check the value is a valid time (greater than Jan 1 2013)
						time = 0L; // return 0 to indicate that the time is not valid
					}
					else{
						this->receivedTime = true;
					}
					Teensy3Clock.set(time); // set the RTC
					setTime(time);
					this->screen.newFile = true;
				}
			}
		break;


		//**********************
		//******* Screen *******
		//**********************
		case 0x605:
			ind = 0;
			this->screen.current_threshold = buffer_get_int16(frame.buf, &ind);
			this->screen.pid_speed_setpoint = buffer_get_float16(frame.buf,1e1,&ind);
			this->screen.pid_state = frame.buf[ind++];
		break;
		case 0x60D:
			// #ifdef  _DATALOGGER_TSB_H_
			this->screen.newFile = true;
        	// #endif
		break;
		case 0x615:
			//#ifdef NEMAS_CONTROLLER_H
			ind = 0;
			this->screen.controller_state = frame.buf[ind++];
			this->screen.homing = frame.buf[ind++];
			this->screen.mode_setpoint = frame.buf[ind++];
			this->screen.angle_setpoint = frame.buf[ind++]/10.0;
			this->screen.dist_setpoint = frame.buf[ind++];
			this->screen.back_foil_state = frame.buf[ind++];
			//#endif
		break;
		case 0x61D:
			this->screen.solarRelay = true;
		break;
		
		
		//*********************
		//******* Shunt *******
		//*********************
		case 0x60E:
			if (frame.buf[0] == 0x00){
				ind = 2;
				this->shunt.current.value = buffer_get_int32(frame.buf, &ind);
				this->shunt.current.OCS = ( ( frame.buf[1] >> 4 ) >> 0) & 0x01;
				this->shunt.current.this_result = ( ( frame.buf[1] >> 4 ) >> 1) & 0x01;
				this->shunt.current.any_result = ( ( frame.buf[1] >> 4 ) >> 2) & 0x01;
				this->shunt.current.system = ( ( frame.buf[1] >> 4 ) >> 3) & 0x01;
			}
		break;
		case 0x616:
			if (frame.buf[0] == 0x01){
				ind = 2;
				this->shunt.voltage1.value = buffer_get_int32(frame.buf, &ind);
				this->shunt.voltage1.OCS = ( ( frame.buf[1] >> 4 ) >> 0) & 0x01;
				this->shunt.voltage1.this_result = ( ( frame.buf[1] >> 4 ) >> 1) & 0x01;
				this->shunt.voltage1.any_result = ( ( frame.buf[1] >> 4 ) >> 2) & 0x01;
				this->shunt.voltage1.system = ( ( frame.buf[1] >> 4 ) >> 3) & 0x01;
			}
		break;
		case 0x61E:
			if (frame.buf[0] == 0x02){
				ind = 2;
				this->shunt.voltage2.value = buffer_get_int32(frame.buf, &ind);
				this->shunt.voltage2.OCS = ( ( frame.buf[1] >> 4 ) >> 0) & 0x01;
				this->shunt.voltage2.this_result = ( ( frame.buf[1] >> 4 ) >> 1) & 0x01;
				this->shunt.voltage2.any_result = ( ( frame.buf[1] >> 4 ) >> 2) & 0x01;
				this->shunt.voltage2.system = ( ( frame.buf[1] >> 4 ) >> 3) & 0x01;
			}
		break;
		case 0x626:
			if (frame.buf[0] == 0x03){
				ind = 2;
				this->shunt.voltage3.value = buffer_get_int32(frame.buf, &ind);
				this->shunt.voltage3.OCS = ( ( frame.buf[1] >> 4 ) >> 0) & 0x01;
				this->shunt.voltage3.this_result = ( ( frame.buf[1] >> 4 ) >> 1) & 0x01;
				this->shunt.voltage3.any_result = ( ( frame.buf[1] >> 4 ) >> 2) & 0x01;
				this->shunt.voltage3.system = ( ( frame.buf[1] >> 4 ) >> 3) & 0x01;
			}
		break;
		case 0x62E:
			if (frame.buf[0] == 0x04){
				ind = 2;
				this->shunt.temperature.value = buffer_get_int32(frame.buf, &ind) / 10;
				this->shunt.temperature.OCS = ( ( frame.buf[1] >> 4 ) >> 0) & 0x01;
				this->shunt.temperature.this_result = ( ( frame.buf[1] >> 4 ) >> 1) & 0x01;
				this->shunt.temperature.any_result = ( ( frame.buf[1] >> 4 ) >> 2) & 0x01;
				this->shunt.temperature.system = ( ( frame.buf[1] >> 4 ) >> 3) & 0x01;
			}
		break;
		case 0x636:
			if (frame.buf[0] == 0x05){
				ind = 2;
				this->shunt.power.value = buffer_get_int32(frame.buf, &ind);
				this->shunt.power.OCS = ( ( frame.buf[1] >> 4 ) >> 0) & 0x01;
				this->shunt.power.this_result = ( ( frame.buf[1] >> 4 ) >> 1) & 0x01;
				this->shunt.power.any_result = ( ( frame.buf[1] >> 4 ) >> 2) & 0x01;
				this->shunt.power.system = ( ( frame.buf[1] >> 4 ) >> 3) & 0x01;
			}
		break;
		case 0x63E:
			if (frame.buf[0] == 0x06){
				ind = 2;
				this->shunt.As.value = buffer_get_int32(frame.buf, &ind);
				this->shunt.As.OCS = ( ( frame.buf[1] >> 4 ) >> 0) & 0x01;
				this->shunt.As.this_result = ( ( frame.buf[1] >> 4 ) >> 1) & 0x01;
				this->shunt.As.any_result = ( ( frame.buf[1] >> 4 ) >> 2) & 0x01;
				this->shunt.As.system = ( ( frame.buf[1] >> 4 ) >> 3) & 0x01;
			}
		break;
		case 0x646:
			if (frame.buf[0] == 0x07){
				ind = 2;
				this->shunt.Wh.value = buffer_get_int32(frame.buf, &ind);
				this->shunt.Wh.OCS = ( ( frame.buf[1] >> 4 ) >> 0) & 0x01;
				this->shunt.Wh.this_result = ( ( frame.buf[1] >> 4 ) >> 1) & 0x01;
				this->shunt.Wh.any_result = ( ( frame.buf[1] >> 4 ) >> 2) & 0x01;
				this->shunt.Wh.system = ( ( frame.buf[1] >> 4 ) >> 3) & 0x01;
			}
		break;

	}
}


void TSB_CAN::printFrame(CAN_message_t &frame, int mailbox)
{
	 Serial.print("ID: ");
	 Serial.print(frame.id, HEX);
	 Serial.print(" Data: ");
	 for (int c = 0; c < frame.len; c++) 
	 {
			Serial.print(frame.buf[c], HEX);
			Serial.write(' ');
	 }
	 Serial.write('\r');
	 Serial.write('\n');
}

bool TSB_CAN::frameHandler(CAN_message_t &frame, int mailbox, uint8_t controller)
{
		//printFrame(frame, mailbox);
		receive_Message(frame);
		return true;
}

StaticJsonDocument<1024> BMS_Data::toJson(){
	StaticJsonDocument<1024> payload;

	JsonArray cellVoltagesJSON = payload.createNestedArray("cellVoltages");
	JsonArray solarVoltagesJSON = payload.createNestedArray("solarVoltages");
	JsonArray temperaturesJSON = payload.createNestedArray("temperatures");

	for (size_t i = 0; i < 12; i++)
	{
		cellVoltagesJSON.add(cellvoltages[i]);
	}
	for (size_t i = 0; i < 5; i++)
	{
		solarVoltagesJSON.add(solar_voltages[i]);
	}
	for (size_t i = 0; i < 17; i++)
	{
		temperaturesJSON.add(temperatures[i]);
	}
	
	payload["status"] = status;
	payload["ltc_status"] = ltc_status;
	payload["cells_balanc"] = cells_balanc;
	payload["warnings"] = warnings;
	payload["soc"] = soc;
	payload["voltage"] = voltage;
	payload["current"] = current;
	payload["solar_current"] = solar_current;


	return payload;
}

StaticJsonDocument<512> Foils_Data::toJson(){
		StaticJsonDocument<512> payload;

		payload["speed"] = veloc;
		payload["pitch"] = pitch;
		payload["roll"] = roll;
		payload["yaw"] = yaw;
		payload["lat"] = lat;
		payload["lon"] = lon;
		payload["dist"] = dist;
		payload["left_angle"] = left_angle;
		payload["right_angle"] = right_angle;
		payload["mode"] = mode;
		return payload;

}

StaticJsonDocument<512> Motor_Data::toJson(){
	StaticJsonDocument<512> payload;
	payload["motor_current"] = motor_current;
	payload["input_current"] = input_current;
	payload["dutycycle"] = duty_cycle;
	payload["rpm"] = rpm;
	payload["voltage"] = voltage;
	payload["temp_motor"] = temp_motor;
	payload["temp_mos_max"] = temp_mos_max;
	payload["temp_mos_1"] = temp_mos_1;
	payload["temp_mos_2"] = temp_mos_2;
	payload["temp_mos_3"] = temp_mos_3;
	payload["fault"] = fault;
	payload["id"] = id;
	return payload;
}

StaticJsonDocument<256> Throttle_Data::toJson(){
	StaticJsonDocument<256> payload;
	payload["asked_current"] = asked_current;
	return payload;
}

time_t TSB_CAN::getTeensy3Time()
{
  return Teensy3Clock.get();
}

void TSB_CAN::check_valid_data(){

	for(int i=0; i<7; i++){
		if((millis()-this->received_time[i] > 2000)) reset_data(i);
	}	

	/*

	if(this->can_validate_time==0){
		this->can_validate_time=millis();
		return;
	}

	if((millis()-this->can_validate_time > 3000) && validate_state==0){
		for(int i=0; i<7; i++)	this->valid[i] = false;
		this->can_validate_time=millis();
		this->validate_state=1;
		return;
	}
	
	if((millis()-this->can_validate_time > 4000) && validate_state==1){
		//if data is not valid put to zero
		for(int i=0; i<7; i++){
			if(!(this->valid[i])) reset_data(i);
		}	
		this->can_validate_time=millis();
		this->validate_state=0;
		return;
	}
	*/
}

void TSB_CAN::reset_data(uint8_t id){
	switch(id) {
		//*****************
		//****** BMS ******
		//*****************
		case 0x01:
			this->bms.status = 0;
			this->bms.ltc_status = 0;
			this->bms.cells_balanc = 0;
			this->bms.warnings = 0;
			this->bms.soc = 0;
			this->bms.voltage = 0;
			this->bms.voltage = 0;
			this->bms.current = 0;
			this->bms.solar_current = 0;
			for(int i=0; i<4; i++){
				this->bms.cellvoltages[i] = 0;
			}
			for(int i=0; i<4; i++){
				this->bms.cellvoltages[i+4] = 0;
			}
			for(int i=0; i<4; i++){
				this->bms.cellvoltages[i+8] = 0;
			}
			for(int i=0; i<4; i++){
				this->bms.solar_voltages[i] = 0;
			}
			this->bms.solar_voltages[4] = 0;
			for(int i=0; i<8; i++){
				this->bms.temperatures[i] = 0;
			}
			for(int i=0; i<5; i++){
				this->bms.temperatures[i+8] = 0;
			}
			for(int i=0; i<4; i++){
				this->bms.temperatures[i+13] = 0;
			}
		break;
		//*********************
		//******* Foils *******
		//*********************
		case 0x02:
			this->foils.veloc = 0;
			this->foils.pitch = 0;
			this->foils.roll = 0;
			this->foils.yaw = 0;
			this->foils.lat = 0;
			this->foils.lon = 0;
			this->foils.dist = 0;
			this->foils.left_angle = 0;
			this->foils.right_angle = 0;
			this->foils.state = 0;
			this->foils.mode = 0;
			this->foils.gyroX = 0;
			this->foils.gyroY = 0;
		break;
		//*********************
		//****** Motores ******
		//*********************
		case 0x03:
			this->motor1.motor_current = 0;
			this->motor1.input_current = 0;
			this->motor1.duty_cycle = 0;
			this->motor1.rpm = 0;
			this->motor1.voltage = 0;
			this->motor1.temp_motor = 0;
			this->motor1.temp_mos_1 = 0;
			this->motor1.temp_mos_2 = 0;
			this->motor1.temp_mos_3 = 0;
			this->motor1.temp_mos_max = 0;
			this->motor1.temp_mos_max = 0;
			this->motor1.temp_mos_max = 0;
			this->motor1.fault = 0;
			this->motor1.id = 0;
			this->throttle.asked_current = 0;
			this->motor2.motor_current = 0;
			this->motor2.input_current = 0;
			this->motor2.duty_cycle = 0;
			this->motor2.rpm = 0;
			this->motor2.voltage = 0;
			this->motor2.temp_motor = 0;
			this->motor2.temp_mos_1 = 0;
			this->motor2.temp_mos_2 = 0;
			this->motor2.temp_mos_3 = 0;
			this->motor2.temp_mos_max = 0;
			this->motor2.temp_mos_max = 0;
			this->motor2.temp_mos_max = 0;
			this->motor2.fault = 0;
			this->motor2.id = 0;
		break;
		//***********************
		//****** CAN WiFi  ******
		//***********************
		case 0x04:
		break;
		//**********************
		//******* Screen *******
		//**********************
		case 0x05:
			this->screen.current_threshold = 0;
			this->screen.newFile = 0;
			this->screen.controller_state = 0;
			this->screen.homing = 0;
			this->screen.mode_setpoint = 0;
			this->screen.angle_setpoint = 0;
			this->screen.dist_setpoint = 0;
		break;
		//*********************
		//******* Shunt *******
		//*********************
		case 0x06:
			this->shunt.current.value = 0;
			this->shunt.current.OCS = 0;
			this->shunt.current.this_result = 0;
			this->shunt.current.any_result = 0;
			this->shunt.current.system = 0;
			this->shunt.voltage1.value = 0;
			this->shunt.voltage1.OCS = 0;
			this->shunt.voltage1.this_result = 0;
			this->shunt.voltage1.any_result = 0;
			this->shunt.voltage1.system = 0;
			this->shunt.voltage2.value = 0;
			this->shunt.voltage2.OCS = 0;
			this->shunt.voltage2.this_result = 0;
			this->shunt.voltage2.any_result = 0;
			this->shunt.voltage2.system = 0;
			this->shunt.voltage3.value = 0;
			this->shunt.voltage3.OCS = 0;
			this->shunt.voltage3.this_result = 0;
			this->shunt.voltage3.any_result = 0;
			this->shunt.voltage3.system = 0;
			this->shunt.temperature.value = 0;
			this->shunt.temperature.OCS = 0;
			this->shunt.temperature.this_result = 0;
			this->shunt.temperature.any_result = 0;
			this->shunt.temperature.system = 0;
			this->shunt.power.value = 0;
			this->shunt.power.OCS = 0;
			this->shunt.power.this_result = 0;
			this->shunt.power.any_result = 0;
			this->shunt.power.system = 0;
			this->shunt.As.value = 0;
			this->shunt.As.OCS = 0;
			this->shunt.As.this_result = 0;
			this->shunt.As.any_result = 0;
			this->shunt.As.system = 0;
			this->shunt.Wh.value = 0;
			this->shunt.Wh.OCS = 0;
			this->shunt.Wh.this_result = 0;
			this->shunt.Wh.any_result = 0;
			this->shunt.Wh.system = 0;
		break;
	}
}
