/*
    Copyright (C) 2020  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#ifndef NEMAS_COMMS_H
#define NEMAS_COMMS_H

#include <FlexCAN.h>
#include "../../include/LinkedList.h"

struct SdoRequest {
    volatile bool received;
    volatile uint32_t id;
    volatile uint16_t index;
    volatile uint8_t subindex;
    CAN_message_t frame;
};

struct PdoTxMapping{
    volatile uint16_t statusword;
    volatile int8_t mode;
    volatile int32_t position_actual_value;
};

struct EMCYMessage{
    volatile uint8_t error_code[2];
    volatile uint8_t error_register;
    volatile uint8_t manufacturer_error_code[5];
};

struct BootUpMessage{
    volatile bool online;
};

class NemasComms : public CANListener {
public:
    //! Constructor
    NemasComms();
    
    void SendMessage(const CAN_message_t &message);
    
    void RecvMessage(CAN_message_t &frame);

    bool frameHandler(CAN_message_t &frame, int mailbox, uint8_t controller);

    LinkedList<CAN_message_t> buffer;

    bool get_frame(uint32_t response_id, uint16_t index, uint8_t subindex, CAN_message_t &frame);

    SdoRequest sdorequest;

    PdoTxMapping pdotxmapping_nemas1;
    PdoTxMapping pdotxmapping_nemas2;
    PdoTxMapping pdotxmapping_nemas3;

    EMCYMessage emcy_nemas1;
    EMCYMessage emcy_nemas2;
    EMCYMessage emcy_nemas3;

    BootUpMessage boot_nemas1;
    BootUpMessage boot_nemas2;   
    BootUpMessage boot_nemas3;

private:
};
//Nemas IDS
#define NEMAS1_ID 1
#define NEMAS2_ID 2
#define NEMAS3_ID 3

/*****CANOPEN PDO TX*******/
#define TX_PDO_MAPPING1 0x180
#define TX_PDO_MAPPING2 0x280
#define TX_PDO_MAPPING3 0x380
#define TX_PDO_MAPPING4 0x480

/*****CANOPEN EMCY*******/
#define CANOPEN_EMCY_ID 0x80

/*****CANOPEN EMCY*******/
#define CANOPEN_BOOTUP_ID 0x700

#endif
// EOF