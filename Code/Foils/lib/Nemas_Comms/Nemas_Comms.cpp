/*
    Copyright (C) 2020  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include "Nemas_Comms.h"
/**
 * NemasComms::NemasComms
 * 
 * Constructor for communication object
 * 
 */
NemasComms::NemasComms(){
    this->buffer = LinkedList<CAN_message_t>();
    this->sdorequest.received=false;
    this->boot_nemas1.online=false;
    this->boot_nemas2.online=false;
    this->boot_nemas3.online=false;
}

/**
 * NemasComms 
 * 
 * Function to send a message
 * 
 * @param  {CAN_message_t} message : message to be sent
 */
void NemasComms::SendMessage(const CAN_message_t &message){
    Can1.write(message);
}

/**
 * NemasComms 
 * 
 * Function to Receive a message
 * 
 * @param  {CAN_message_t} frame : message received
 */
void NemasComms::RecvMessage(CAN_message_t &frame){
    //Debug
    //Serial.print("ID: ");
    //Serial.print(frame.id,HEX);
    //Serial.print(" Index: ");
    //Serial.print(frame.buf[2],HEX);
    //Serial.println(frame.buf[1],HEX);
    if(sdorequest.id == frame.id && (sdorequest.index & 0xFF) == frame.buf[1] && (sdorequest.index >> 8) == frame.buf[2] && sdorequest.subindex == frame.buf[3]){
        this->sdorequest.frame=frame;
        this->sdorequest.received=true;
    }
    /*}else{
        switch (frame.id){
            case TX_PDO_MAPPING1 + NEMAS1_ID:
                //statusword
                pdotxmapping_nemas1.statusword= ((uint16_t) frame.buf[1]) << 8 | ((uint16_t) frame.buf[0]);
                //mode_of_operation
                pdotxmapping_nemas1.mode= frame.buf[2];
                break;
            case TX_PDO_MAPPING2 + NEMAS1_ID:
                //position_actual_value
                pdotxmapping_nemas1.position_actual_value= ((uint32_t) frame.buf[3]) << 24 | ((uint32_t) frame.buf[2]) << 16 | ((uint32_t) frame.buf[1]) << 8 | ((uint32_t) frame.buf[0]);
                break;
            case TX_PDO_MAPPING3 + NEMAS1_ID:
                break;
            case TX_PDO_MAPPING4 + NEMAS1_ID:
                break;
            case TX_PDO_MAPPING1 + NEMAS2_ID:
                //statusword
                pdotxmapping_nemas2.statusword = ((uint16_t) frame.buf[1]) << 8 | ((uint16_t) frame.buf[0]);
                //mode_of_operation
                pdotxmapping_nemas2.mode =frame.buf[2];
                break;
            case TX_PDO_MAPPING2 + NEMAS2_ID:
                //position_actual_value
                pdotxmapping_nemas2.position_actual_value = ((uint32_t) frame.buf[3]) << 24 | ((uint32_t) frame.buf[2]) << 16 | ((uint32_t) frame.buf[1]) << 8 | ((uint32_t) frame.buf[0]);
                break;
            case TX_PDO_MAPPING3 + NEMAS2_ID:
                break;
            case TX_PDO_MAPPING4 + NEMAS2_ID:
                break;
            case TX_PDO_MAPPING1 + NEMAS3_ID:
                //statusword
                pdotxmapping_nemas3.statusword = ((uint16_t) frame.buf[1]) << 8 | ((uint16_t) frame.buf[0]);
                //mode_of_operation
                pdotxmapping_nemas3.mode = frame.buf[2];
                break;
            case TX_PDO_MAPPING2 + NEMAS3_ID:
                //position_actual_value
                pdotxmapping_nemas3.position_actual_value = ((uint32_t) frame.buf[3]) << 24 | ((uint32_t) frame.buf[2]) << 16 | ((uint32_t) frame.buf[1]) << 8 | ((uint32_t) frame.buf[0]);
                break;
            case TX_PDO_MAPPING3 + NEMAS3_ID:
                break;
            case TX_PDO_MAPPING4 + NEMAS3_ID:
                break;
            case CANOPEN_EMCY_ID + NEMAS1_ID:
                emcy_nemas1.error_code[0]=frame.buf[1];
                emcy_nemas1.error_code[1]=frame.buf[0];
                emcy_nemas1.error_register=frame.buf[2];
                emcy_nemas1.manufacturer_error_code[4]=frame.buf[7];
                emcy_nemas1.manufacturer_error_code[3]=frame.buf[6];
                emcy_nemas1.manufacturer_error_code[2]=frame.buf[5];
                emcy_nemas1.manufacturer_error_code[1]=frame.buf[4];
                emcy_nemas1.manufacturer_error_code[0]=frame.buf[3];
                break;
            case CANOPEN_EMCY_ID + NEMAS2_ID:
                emcy_nemas2.error_code[0]=frame.buf[1];
                emcy_nemas2.error_code[1]=frame.buf[0];
                emcy_nemas2.error_register=frame.buf[2];
                emcy_nemas2.manufacturer_error_code[4]=frame.buf[7];
                emcy_nemas2.manufacturer_error_code[3]=frame.buf[6];
                emcy_nemas2.manufacturer_error_code[2]=frame.buf[5];
                emcy_nemas2.manufacturer_error_code[1]=frame.buf[4];
                emcy_nemas2.manufacturer_error_code[0]=frame.buf[3];
                break;
            case CANOPEN_EMCY_ID + NEMAS3_ID:
                emcy_nemas3.error_code[0]=frame.buf[1];
                emcy_nemas3.error_code[1]=frame.buf[0];
                emcy_nemas3.error_register=frame.buf[2];
                emcy_nemas3.manufacturer_error_code[4]=frame.buf[7];
                emcy_nemas3.manufacturer_error_code[3]=frame.buf[6];
                emcy_nemas3.manufacturer_error_code[2]=frame.buf[5];
                emcy_nemas3.manufacturer_error_code[1]=frame.buf[4];
                emcy_nemas3.manufacturer_error_code[0]=frame.buf[3];
                break;
            case CANOPEN_BOOTUP_ID + NEMAS1_ID:
                boot_nemas1.online=true;
                break;
            case CANOPEN_BOOTUP_ID + NEMAS2_ID:
                boot_nemas2.online=true;
                break;
            case CANOPEN_BOOTUP_ID + NEMAS3_ID:
                boot_nemas3.online=true;
                break;
            default:
                break;
        }
    }
    */
    //add frame to the end of the list
    //this->buffer.add(frame);
}

/**
 * NemasComms 
 * 
 *  Frame Handler
 * 
 * @param  {CAN_message_t} frame : 
 * @param  {int} mailbox         : 
 * @param  {uint8_t} controller  : 
 * @return {bool}                : 
 */
bool NemasComms::frameHandler(CAN_message_t &frame, int mailbox, uint8_t controller){
    //Serial.println("Handler");
    RecvMessage(frame);
    return true;
}
/*
bool NemasComms::get_frame(uint32_t response_id, uint16_t index, uint8_t subindex, CAN_message_t &frame){
    CAN_message_t aux_frame;
    Can1.interrupt_lock();
    for (int i = buffer.size()-1; i >= 0; i--){
        aux_frame = buffer.get(i);
        if(response_id == aux_frame.id && (index & 0xFF) == aux_frame.buf[1] && (index >> 8) == aux_frame.buf[2] && subindex == aux_frame.buf[3]){
            frame=aux_frame;
            this->buffer.remove(i);
            Can1.interrupt_release();
            return true;
        }
    }
    Can1.interrupt_release();
    return false;
}
*/