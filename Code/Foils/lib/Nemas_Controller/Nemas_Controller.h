/*
    Copyright (C) 2020  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/


#ifndef NEMAS_CONTROLLER_H
#define NEMAS_CONTROLLER_H

#include "Arduino.h"
#include <string>
#include <cstring>
#include <math.h>
#include <time.h>
#include "FlexCAN.h"
#include "CAN_TSB.h"
#include "Nemas_Comms.h"
#include "buffer.h"
#include <stdint.h>
#include "LQR.h"

struct PdoRXMapping{
    volatile uint16_t controlword;
    volatile int8_t mode_operation;
    volatile uint32_t motor_drive_submode;
    volatile int32_t target_position;
    volatile uint32_t profile_velocity;
};

class NemasController
{
    public:               
        NemasController(uint8_t nemas_id, NemasComms* comms_node);
        bool WriteSDO(uint16_t index,uint8_t subIndex,uint8_t data_size,int32_t data);
        bool ReadSDO(uint16_t index,uint8_t subIndex,uint8_t* data);
        bool stateMachine(uint8_t goal_state);
        bool getState(uint8_t &state);
        bool setOperatingMode(int32_t mode);
        bool setProfilePosition(int32_t position);
        bool instant_setPosition(int32_t position);
        bool completed_setPosition(int32_t position);
        bool completed_relative_setPosition(int32_t position);
        bool startHoming(int zsearch_rpm, int zsearch_current, int final_rpm, int final_current);
        bool stopHoming();
        bool getActualPosition(int32_t &position);
        bool getCurrent_Iq(int32_t &current);
        void WriteNMT(uint8_t state);
        void WriteSYNC();
        bool WritePDO(uint8_t mapping);
        bool pdo_instant_setPosition(int32_t position);
        void pdo_rx_config();
        void pdo_tx_config();
        void WriteNodeguarding();
        bool WriteToN5(uint8_t subindex,int32_t value);
        bool ReadFromN5(uint8_t subindex,int32_t &value);
        bool backfoilHoming();

        PdoRXMapping pdorxmapping;

        float actualPosition;
        float current_Iq;
    private:
        NemasComms* node;
        uint8_t id;
};
//Message Sizes
#define CANOPEN_MSG_SIZE_8_BITS    0X01
#define CANOPEN_MSG_SIZE_16_BITS   0X02
#define CANOPEN_MSG_SIZE_24_BITS   0x03
#define CANOPEN_MSG_SIZE_32_BITS   0X04

/*****CANOPEN NMT*******/
#define CANOPEN_NMT_ID   0x000

//States CMDs
#define CANOPEN_NMT_SWITCH_OPERATIONAL      0x01
#define CANOPEN_NMT_SWITCH_STOP             0x02
#define CANOPEN_NMT_SWITCH_PREOPERATIONAL   0x80
#define CANOPEN_NMT_RESET_NODE              0x81
#define CANOPEN_NMT_RESET_COMMUNICATION     0x82

/*****CANOPEN SYNC*******/
#define CANOPEN_SYNC_ID 0x80

/*****CANOPEN PDO RX*******/
#define CANOPEN_RX_PDO_MAPPING1_CONFIG 0x1400
#define CANOPEN_RX_PDO_MAPPING2_CONFIG 0x1401
#define CANOPEN_RX_PDO_MAPPING3_CONFIG 0x1402
#define CANOPEN_RX_PDO_MAPPING4_CONFIG 0x1403

#define CANOPEN_RX_PDO_SUBINDEX_TRANSMISSION_TYPE 0x02
#define CANOPEN_RX_PDO_TRANSMISSION_TYPE_ASSYNC 0xFF
#define CANOPEN_RX_PDO_TRANSMISSION_TYPE_SYNC 0x00

#define CANOPEN_RX_PDO_MAPPING1 0x200
#define CANOPEN_RX_PDO_MAPPING2 0x300
#define CANOPEN_RX_PDO_MAPPING3 0x400
#define CANOPEN_RX_PDO_MAPPING4 0x500

//PDO_IDS
#define PDO_RX_MAPPING1 0x01
#define PDO_RX_MAPPING2 0x02
#define PDO_RX_MAPPING3 0x03
#define PDO_RX_MAPPING4 0x04


/*****CANOPEN SDO*******/
#define CANOPEN_SDO_DOWNLOAD_REQUEST    0x600
#define CANOPEN_SDO_UPLOAD_REQUEST      0x600
#define CANOPEN_SDO_DOWNLOAD_REPLY      0x580
#define CANOPEN_SDO_UPLOAD_REPLY        0x580

//CANOPEN SDO CMDs
#define CANOPEN_SDO_CMD_1_BYTE 0x2F
#define CANOPEN_SDO_CMD_2_BYTE 0x2B
#define CANOPEN_SDO_CMD_3_BYTE 0x27
#define CANOPEN_SDO_CMD_4_BYTE 0x23

//CANOPEN SDO REPLY
#define CANOPEN_SDO_REPLY_OK     0x60
#define CANOPEN_SDO_REPLY_ERROR  0x80

//CANOPEN SDO REPLY CMD
#define CANOPEN_SDO_REPLY_1_BYTE 0x4F
#define CANOPEN_SDO_REPLY_2_BYTE 0x4B
#define CANOPEN_SDO_REPLY_3_BYTE 0x47
#define CANOPEN_SDO_REPLY_4_BYTE 0x43

//States Of Operation
#define STATE_MASK              0x6F
#define SWITCH_ON_DISABLED      1
#define READY_TO_SWITCH_ON      2
#define SWITCHED_ON	            3
#define OPERATION_ENABLED       4

//Controller States
#define CANOPEN_NOT_READY_TO_SWITCH_ON	0x00
#define CANOPEN_SWITCH_ON_DISABLED	    0x40
#define CANOPEN_READY_TO_SWITCH_ON 	    0x21
#define CANOPEN_SWITCHED_ON		        0x23
#define CANOPEN_OPERATION_ENABLED	    0x27
#define CANOPEN_QUICK_STOP_ACTIVE	    0x07
#define CANOPEN_FAULT_REACTION_ACTIVE   0x0F
#define CANOPEN_FAULT 			        0x08

// Transition Commands			     
#define CANOPEN_DISABLE_VOLTAGE		0x00 
#define CANOPEN_SWITCH_ON		0x07 
#define CANOPEN_SHUTDOWN		0x06 
#define CANOPEN_QUICK_STOP		0x02 
#define CANOPEN_DISABLE_OPERATION	0x07 
#define CANOPEN_ENABLE_OPERATION	0x0F 
#define CANOPEN_FAULT_RESET		0x80 

//TIMEOUTS
#define CANOPEN_SDO_REPLY_TIMEOUT   50 //5 ms
#define STATE_MACHINE_TIMEOUT       0x32 //50 ms
#define TIMEOUT_HOMING              15000 //15000 ms

//OPERATION MODES
#define CANOPEN_OPERATION_MODE_PROFILE_POSITION 1
#define CANOPEN_OPERATION_MODE_VELOCITY 	2
#define CANOPEN_OPERATION_MODE_PROFILE_VELOCITY 3
#define CANOPEN_OPERATION_MODE_HOMING 6

//MODES SET GET
#define CANOPEN_SET_MODES_OF_OPERATIONS		0x6060
#define CANOPEN_GET_MODES_OF_OPERATIONS		0x6061

//PROFILE POSITION
#define CANOPEN_TARGET_POSITION 0x607A

//POSITION ACTUAL VALUE
#define CANOPEN_POSITION_ACTUAL_VALUE 0x6064

//STATUS WORDS
#define CANOPEN_CONTROL_WORD    0x6040
#define CANOPEN_STATUS_WORD     0x6041

//PROFILE VELOCITY
#define CANOPEN_PROFILE_VELOCITY 0x6081

//MAX CURRENT
#define CANOPEN_MAX_CURRENT 0x2031

//Motor Drive Submode Select
#define CANOPEN_MOTOR_SUBMODE_SELECT 0x3202

//Customer Storage Area
#define CUSTOMER_STORAGE_AREA_ID 0x2701

//Functions
int32_t buffer_toint32(uint8_t *buffer);
double buffer_todouble(uint8_t *buffer);
void init_CAN_messages();
void send_CAN(float speed);
void read_ultrasonic();
void enable_ultrasonic_sensors();
void disable_ultrasonic_sensors();
void controll();
int32_t angle_to_steps(float angle);
float steps_to_angle(int32_t steps);
float steps_to_angle_rear(int32_t steps);
void send_CAN();
void init_CAN_messages();
void logdata();
void full_homing();
int32_t backfoil_angle_to_steps(float angle);
#endif
