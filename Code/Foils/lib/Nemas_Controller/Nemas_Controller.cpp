/*
    Copyright (C) 2020  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include "Nemas_Controller.h"

CAN_message_t msg;


/**
 * NemasController::NemasController 
 * 
 * Constructor for controller object
 * 
 * @param  {uint8_t} nemas_id       : id of the controller
 * @param  {NemasComms*} comms_node : communication node
 */
NemasController::NemasController(uint8_t nemas_id,NemasComms* comms_node){
    this->node = comms_node;
    this->id = nemas_id;
    this->actualPosition = 0;
}

/**
 * NemasController 
 * 
 * Function to write SDO requests
 * 
 * @param  {uint16_t} index    : sdo index
 * @param  {uint8_t} subindex  : sdo subindex
 * @param  {uint8_t} data_size : sdo data size
 * @param  {int32_t} data      : sdo data to write
 * @return {bool}              : sdo request success info
 */
bool NemasController::WriteSDO(uint16_t index, uint8_t subindex, uint8_t data_size, int32_t data){
    //8 bytes frame
    msg.ext=0;

    //Data Length 8 bytes
    msg.len = 8;

    //CAN ID
    msg.id = CANOPEN_SDO_DOWNLOAD_REQUEST + id;

    //CMD Byte0
    switch(data_size){
        case CANOPEN_MSG_SIZE_8_BITS : msg.buf[0] = CANOPEN_SDO_CMD_1_BYTE; break;
        case CANOPEN_MSG_SIZE_16_BITS : msg.buf[0] = CANOPEN_SDO_CMD_2_BYTE; break;
        case CANOPEN_MSG_SIZE_24_BITS: msg.buf[0] = CANOPEN_SDO_CMD_3_BYTE; break;
        case CANOPEN_MSG_SIZE_32_BITS : msg.buf[0] = CANOPEN_SDO_CMD_4_BYTE; break;
        default: return false;
    }

    //Index Byte1 >> Index LSB, Byte2 >> Index MSB
    msg.buf[1] = index & 0xFF;
    msg.buf[2] = index >> 8;

    //Sub-Index Byte 3
    msg.buf[3] = subindex;

    //Data Byte4 >> Data LSB, Byte7 >> Data MSB
    msg.buf[4] = data & 0xFF;
    msg.buf[5] = (data >> 8) & 0xFF;
    msg.buf[6] = (data >> 16) & 0xFF;
    msg.buf[7] = (data >> 24) & 0xFF;

    node->sdorequest.id=CANOPEN_SDO_DOWNLOAD_REPLY+id;
    node->sdorequest.index=index;
    node->sdorequest.subindex=subindex;

	//Send frame
    node->SendMessage(msg);

    //wait for response
    unsigned long send_time = millis();
    while(!node->sdorequest.received){
        if(millis()-send_time > CANOPEN_SDO_REPLY_TIMEOUT){
            Serial.print("Timeout Write SDO");
            Serial.println(id);
            return false;
        }
    }

    //received
    node->sdorequest.received=false;
    msg=node->sdorequest.frame;

    //check for errors
    switch(msg.buf[0]){
        case CANOPEN_SDO_REPLY_OK: return true;
        case CANOPEN_SDO_REPLY_ERROR:
            /*
            Serial.print("Error SDO WRITE: | ");
            Serial.print(msg.buf[7]);
            Serial.print(" | ");
            Serial.print(msg.buf[6]);
            Serial.print(" | ");
            Serial.print(msg.buf[5]);
            Serial.print(" | ");
            Serial.print(msg.buf[4]);
            Serial.println(" |");
            */
            return false;
        default: return false;
    }
}

/**
 * NemasController 
 * 
 * Function to read SDO requests
 * 
 * @param  {u_int16_t} index   : sdo index
 * @param  {u_int8_t} subindex : sdo subindex
 * @param  {u_int8_t*} data    : sdo data read
 * @return {bool}              : sdo request success info
 */
bool NemasController::ReadSDO(u_int16_t index, u_int8_t subindex, u_int8_t* data){
    //8 bytes frame
    msg.ext=0;

    //Data Length 8 bytes
    msg.len = 8;

    //CAN ID
    msg.id = CANOPEN_SDO_UPLOAD_REQUEST + id;

    //CMD Byte0 >> 0x40
    msg.buf[0] =  0x40;

    //Index Byte1 >> Index LSB, Byte2 >> Index MSB
    msg.buf[1] = index & 0xFF;
    msg.buf[2] = index >> 8;

    //Sub-Index Byte 3
    msg.buf[3] = subindex;

    //Data Byte4-Byte7 >> 0x00
    msg.buf[4] = 0x00;
    msg.buf[5] = 0x00;
    msg.buf[6] = 0x00;
    msg.buf[7] = 0x00;

    //post a new request
    node->sdorequest.id=CANOPEN_SDO_UPLOAD_REPLY+id;
    node->sdorequest.index=index;
    node->sdorequest.subindex=subindex;

	//Send frame
    node->SendMessage(msg);
    //Serial.println("Sent");

    //wait for response
    unsigned long send_time = millis();
    while(!(node->sdorequest.received)){
        if(millis()-send_time > CANOPEN_SDO_REPLY_TIMEOUT){
            Serial.print("Timeout READ SDO ");
            Serial.println(id);
            return false;
        }
    }

    //received
    node->sdorequest.received=false;
    msg=node->sdorequest.frame;

    //check for errors
    switch(msg.buf[0]){
        case CANOPEN_SDO_REPLY_1_BYTE:
            data[0] = 0x00;
            data[1] = 0x00;
            data[2] = 0x00;
            data[3] = msg.buf[4];
            return true;
        case CANOPEN_SDO_REPLY_2_BYTE:
            data[0] = 0x00;
            data[1] = 0x00;
            data[2] = msg.buf[5];
            data[3] = msg.buf[4];
            return true;
        case CANOPEN_SDO_REPLY_3_BYTE:
            data[0] = 0x00;
            data[1] = msg.buf[6];
            data[2] = msg.buf[5];
            data[3] = msg.buf[4];
            return true;
        case CANOPEN_SDO_REPLY_4_BYTE:
            data[0] = msg.buf[7];
            data[1] = msg.buf[6];
            data[2] = msg.buf[5];
            data[3] = msg.buf[4];
            return true;
        case CANOPEN_SDO_REPLY_ERROR:
            /*
            Serial.print("Error SDO READ: | ");
            Serial.print(msg.buf[7]);
            Serial.print(" | ");
            Serial.print(msg.buf[6]);
            Serial.print(" | ");
            Serial.print(msg.buf[5]);
            Serial.print(" | ");
            Serial.print(msg.buf[4]);
            Serial.println(" |");
            */
            return false;
        default: return false;
    }
}

/**
 * NemasController 
 * 
 * Function that changes the controller state based on a the controllers state machine
 * 
 * @param  {uint8_t} goal_state : controller desired state
 * @return {bool}               : state change success info
 */
bool NemasController::stateMachine(uint8_t goal_state){
    uint8_t state=0;
    uint8_t actual_state = 0;
    unsigned long start_time=0;

    start_time=millis();
    while(!(millis()-start_time > STATE_MACHINE_TIMEOUT)){
        if(getState(state)){
            switch(state){
                case CANOPEN_SWITCH_ON_DISABLED:
                    actual_state = SWITCH_ON_DISABLED;
                    if(actual_state==goal_state) return true;
                    if(goal_state>actual_state){
                        if(!WriteSDO(CANOPEN_CONTROL_WORD,0x00,CANOPEN_MSG_SIZE_16_BITS,CANOPEN_SHUTDOWN)) return false;
                    }
                    break;
                case CANOPEN_READY_TO_SWITCH_ON:
                    actual_state = READY_TO_SWITCH_ON;
                    if(actual_state==goal_state) return true;
                    if(goal_state>actual_state){
                        if(!WriteSDO(CANOPEN_CONTROL_WORD,0x00,CANOPEN_MSG_SIZE_16_BITS,CANOPEN_SWITCH_ON)) return false;
                    }
                    if(goal_state<actual_state){
                        if(!WriteSDO(CANOPEN_CONTROL_WORD,0x00,CANOPEN_MSG_SIZE_16_BITS,CANOPEN_SWITCH_ON_DISABLED)) return false;
                    }
                    break;
                case CANOPEN_SWITCHED_ON:
                    actual_state = SWITCHED_ON;
                    if(actual_state==goal_state) return true;
                    if(goal_state>actual_state){
                        if(!WriteSDO(CANOPEN_CONTROL_WORD,0x00,CANOPEN_MSG_SIZE_16_BITS,CANOPEN_ENABLE_OPERATION)) return false;
                    }
                    if(goal_state<actual_state){
                        if(!WriteSDO(CANOPEN_CONTROL_WORD,0x00,CANOPEN_MSG_SIZE_16_BITS,CANOPEN_READY_TO_SWITCH_ON)) return false;
                    }
                    break;
                case CANOPEN_OPERATION_ENABLED:
                    actual_state = OPERATION_ENABLED;
                    if(actual_state==goal_state) return true;
                    if(goal_state<actual_state){
                        if(!WriteSDO(CANOPEN_CONTROL_WORD,0x00,CANOPEN_MSG_SIZE_16_BITS,CANOPEN_SWITCH_ON)) return false;
                    }
                    break;
                default:
                    if(!WriteSDO(CANOPEN_CONTROL_WORD,0x00,CANOPEN_MSG_SIZE_16_BITS,CANOPEN_SWITCH_ON_DISABLED)) return false;
                    break;
            }

        }else return false;
    }
    return false;
}
/**
 * NemasController 
 * 
 * Function that gets the current state of the controller
 * 
 * @param  {uint8_t} state : controller state
 * @return {bool}          : operation success info
 */
bool NemasController::getState(uint8_t &state){
    uint8_t data[4];
    if(ReadSDO(CANOPEN_STATUS_WORD,0x00,data)){
        state=(buffer_toint32(data) & STATE_MASK);
        return true;
    }
    return false;
}
/**
 * NemasController 
 * 
 * Function that gets the Iq current of the controller
 * 
 * @param  {int32_t} current : controller iq current
 * @return {bool}            : operation success info
 */
bool NemasController::getCurrent_Iq(int32_t &current){
    uint8_t data[4];
    if(ReadSDO(0x2039,0x02,data)){
        current = buffer_toint32(data);
        return true;
    }
    return false;
}
/**
 * NemasController 
 * 
 * Function that gets the controller actual position
 * 
 * @param  {int32_t} position : controller position
 * @return {bool}             : operation success info
 */
bool NemasController::getActualPosition(int32_t &position){
    uint8_t data[4];
    if(ReadSDO(0x6064,0x00,data)){
        position = buffer_toint32(data);
        return true;
    }
    return false;
}
/**
 * NemasController 
 * 
 * Function that sets the controller operating mode 
 * 
 * @param  {int32_t} mode : controller desired mode
 * @return {bool}         : operation success info
 */
bool NemasController::setOperatingMode(int32_t mode){
    uint8_t data[4];
    if(!stateMachine(SWITCH_ON_DISABLED)) return false;
    if(!WriteSDO(CANOPEN_SET_MODES_OF_OPERATIONS,0x00,CANOPEN_MSG_SIZE_8_BITS,mode)) return false;
    //READ SDO (confirm if the operation mode is correctly set)
    if(ReadSDO(CANOPEN_GET_MODES_OF_OPERATIONS,0x00,data)){
        if(mode == buffer_toint32(data)) return true;
    }
    return false;
}
/**
 * NemasController 
 * 
 * Function that sets the controller profile position
 * 
 * @param  {int32_t} position : controller desired profile position
 * @return {bool}             : operation success info
 */
bool NemasController::setProfilePosition(int32_t position){
    if(!WriteSDO(CANOPEN_TARGET_POSITION,0x00,CANOPEN_MSG_SIZE_32_BITS,position)) return false;
    return true;
}
/**
 * NemasController 
 * 
 * Function that sets the controller absolute position instantly
 * 
 * @param  {int32_t} position : controller desired instant absolute position
 * @return {bool}             : operation success info
 */
bool NemasController::instant_setPosition(int32_t position){
    if(!setProfilePosition(position)) return false;
    if(!WriteSDO(CANOPEN_CONTROL_WORD,0x00,CANOPEN_MSG_SIZE_16_BITS,0x3F)) return false;
    if(!WriteSDO(CANOPEN_CONTROL_WORD,0x00,CANOPEN_MSG_SIZE_16_BITS,0x0F)) return false;
    return true;
}
/**
 * NemasController
 * 
 * Function that sets the controller absolute position, waiting for other position changes to complete
 * 
 * @param  {int32_t} position : controller desired complete absolute position
 * @return {bool}             : operation success info
 */
bool NemasController::completed_setPosition(int32_t position){
    if(!setProfilePosition(position)) return false;
    if(!WriteSDO(CANOPEN_CONTROL_WORD,0x00,CANOPEN_MSG_SIZE_16_BITS,0x1F)) return false;
    if(!WriteSDO(CANOPEN_CONTROL_WORD,0x00,CANOPEN_MSG_SIZE_16_BITS,0x0F)) return false;
    return true;
}
/**
 * NemasController 
 * 
 * Function that sets the controller relative position, waiting for other position changes to complete
 * 
 * @param  {int32_t} position :  controller desired complete relative position
 * @return {bool}             :  operation success info
 */
bool NemasController::completed_relative_setPosition(int32_t position){
    if(!setProfilePosition(position)) return false;
    if(!WriteSDO(CANOPEN_CONTROL_WORD,0x00,CANOPEN_MSG_SIZE_16_BITS,0x5F)) return false;
    if(!WriteSDO(CANOPEN_CONTROL_WORD,0x00,CANOPEN_MSG_SIZE_16_BITS,0x4F)) return false;
    return true;
}
/**
 * NemasController 
 * 
 * Function that performs the controller homing routine
 * 
 * @param  {int} zsearch_rpm     : motor rpm when searching for z index
 * @param  {int} zsearch_current : motor current when searching for z index
 * @param  {int} final_rpm       : motor rpm after the homing is performed
 * @param  {int} final_current   : motor current after the homing is performed
 * @return {bool}                : operation success info 
 */
bool NemasController::startHoming(int zsearch_rpm, int zsearch_current, int final_rpm, int final_current){
    uint8_t data[4];

    bool homing = false;
    bool index_found = false;

    unsigned long last_time=0;

    unsigned long time=0;

    //check if openloop is available bit15 (bit 8 data[2])
    ReadSDO(CANOPEN_STATUS_WORD,0x00,data);
    if((data[2] & 0x80) == 0x80){ //if closed loop available enter here
        //set closed loop
        WriteSDO(CANOPEN_MOTOR_SUBMODE_SELECT,0x00,CANOPEN_MSG_SIZE_32_BITS,0x01);
        //homing command
        WriteSDO(CANOPEN_CONTROL_WORD,0x00,CANOPEN_MSG_SIZE_16_BITS,0x1F);
        //Debug
        //Serial.println("closed loop enabled! homing straight away");
    }else{
        //Debug
        //Serial.println("closed loop not enabled! let's find Z");
        stateMachine(SWITCH_ON_DISABLED);
        //set open loop 
        WriteSDO(CANOPEN_MOTOR_SUBMODE_SELECT,0x00,CANOPEN_MSG_SIZE_32_BITS,0x00);
        //change motor max current
        WriteSDO(CANOPEN_MAX_CURRENT,0x00,CANOPEN_MSG_SIZE_32_BITS,zsearch_current);
        //change profile position speed
        WriteSDO(CANOPEN_PROFILE_VELOCITY,0x00,CANOPEN_MSG_SIZE_32_BITS,zsearch_rpm);

        //profile position
        setOperatingMode(CANOPEN_OPERATION_MODE_PROFILE_POSITION);
		stateMachine(OPERATION_ENABLED);
        //1 rotation left
        completed_relative_setPosition(360);
        //1 rotation right
        completed_relative_setPosition(-360);

        last_time=millis();

        //check open loop
        while(!index_found){
            ReadSDO(CANOPEN_STATUS_WORD,0x00,data);
            if(((data[2] & 0x80) == 0x80)) {
                //Debug
                //Serial.println("Z found!");
                index_found=true;
            }
            time=millis();
		    if(time - last_time > 10000) { //10 seconds before timeout
                //Debug
                //Serial.println("timeout searching for Z");
                stateMachine(SWITCH_ON_DISABLED);
                // reset max current 
                WriteSDO(CANOPEN_MAX_CURRENT,0x00,CANOPEN_MSG_SIZE_32_BITS,final_current);
                //reset profile position speed
                WriteSDO(CANOPEN_PROFILE_VELOCITY,0x00,CANOPEN_MSG_SIZE_32_BITS,final_rpm);
                return false;
            }
        }
        stateMachine(SWITCH_ON_DISABLED);
        //set closed loop
        WriteSDO(CANOPEN_MOTOR_SUBMODE_SELECT,0x00,CANOPEN_MSG_SIZE_32_BITS,0x01);

        // reset max current 
        WriteSDO(CANOPEN_MAX_CURRENT,0x00,CANOPEN_MSG_SIZE_32_BITS,final_current);
        //reset profile position speed
        WriteSDO(CANOPEN_PROFILE_VELOCITY,0x00,CANOPEN_MSG_SIZE_32_BITS,final_rpm);



        setOperatingMode(CANOPEN_OPERATION_MODE_HOMING);
        stateMachine(OPERATION_ENABLED);
        WriteSDO(CANOPEN_CONTROL_WORD,0x00,CANOPEN_MSG_SIZE_16_BITS,0x1F);
    }

    last_time=millis();
    //Debug
    //Serial.println("homing...");
    while(!homing){
        //homing cycle
        ReadSDO(CANOPEN_STATUS_WORD,0x00,data);
        if((data[2] & 0x04) == 0x04) {
            homing=true;
            //Debug
            //Serial.println("homing done!");
        }
        time=millis();
		if(time - last_time > TIMEOUT_HOMING) {
            //Debug
            //Serial.println("homing timeout");
            return false;
        }

    }
    return true;
}
/**
 * NemasController 
 * 
 * Function to interrupt homing
 * 
 * @return {bool}  : operation success info
 */
bool NemasController::stopHoming(){
    if(!WriteSDO(CANOPEN_CONTROL_WORD,0x00,CANOPEN_MSG_SIZE_16_BITS,0x0F)) return false;
    return true;
}
/**
 * NemasController 
 * 
 * Function to write NMT state
 * 
 * @param  {uint8_t} state : controller desired NMT state
 */
void NemasController::WriteNMT(uint8_t state){
    //8 bytes frame
    msg.ext=0;

    //Data Length 8 bytes
    msg.len = 2;

    //CAN ID
    msg.id = CANOPEN_NMT_ID;

    //CMD
    msg.buf[0] = state;

    //Node ID
    msg.buf[1] = id;

    //Send frame
    node->SendMessage(msg);
}
/**
 * NemasController 
 * 
 * Function to write a SYNC message
 * 
 */
void NemasController::WriteSYNC(){
    //8 bytes frame
    msg.ext=0;

    //Data Length 8 bytes
    msg.len = 0;

    //CAN ID
    msg.id = CANOPEN_SYNC_ID + id;

    //Send frame
    node->SendMessage(msg);
}
/**
 * NemasController 
 * 
 * Function to write a PDO request
 * 
 * @param  {uint8_t} mapping : request mapping
 * @return {bool}            : operation success info
 */
bool NemasController::WritePDO(uint8_t mapping){
    //8 bytes frame
    msg.ext=0;

    //Data Length 8 bytes
    msg.len = 8;

    //CAN ID
    switch(mapping){
        case PDO_RX_MAPPING1:
            msg.len = 7;
            msg.id=CANOPEN_RX_PDO_MAPPING1 + id;
            //controlword
            msg.buf[0] = pdorxmapping.controlword & 0xFF;
            msg.buf[1] = (pdorxmapping.controlword >> 8) & 0xFF;
            //mode of operation
            msg.buf[2] = pdorxmapping.mode_operation & 0xFF;
            //mode drive submode select
            msg.buf[3] = pdorxmapping.mode_operation & 0xFF;
            msg.buf[4] = (pdorxmapping.mode_operation >> 8) & 0xFF;
            msg.buf[5] = (pdorxmapping.mode_operation >> 16) & 0xFF;
            msg.buf[6] = (pdorxmapping.mode_operation >> 24) & 0xFF;
            break;
        case PDO_RX_MAPPING2:
            msg.id=CANOPEN_RX_PDO_MAPPING2 + id;
            //target position
            msg.buf[0] = pdorxmapping.target_position & 0xFF;
            msg.buf[1] = (pdorxmapping.target_position >> 8) & 0xFF;
            msg.buf[2] = (pdorxmapping.target_position >> 16) & 0xFF;
            msg.buf[3] = (pdorxmapping.target_position >> 24) & 0xFF;
            //profile velocity
            msg.buf[4] = pdorxmapping.profile_velocity & 0xFF;
            msg.buf[5] = (pdorxmapping.profile_velocity >> 8) & 0xFF;
            msg.buf[6] = (pdorxmapping.profile_velocity >> 16) & 0xFF;
            msg.buf[7] = (pdorxmapping.profile_velocity >> 24) & 0xFF;
            break;
        case PDO_RX_MAPPING3:
            msg.id=CANOPEN_RX_PDO_MAPPING3 + id;
            break;
        case PDO_RX_MAPPING4:
            msg.id=CANOPEN_RX_PDO_MAPPING4 + id;
            break;
        default:
            return false;
    }
    //Send frame
    node->SendMessage(msg);
    return true;
}
/**
 * NemasController 
 * 
 * Function that changes the controller position with a pdo request
 * 
 * @param  {int32_t} position : controller desired position
 * @return {bool}             : operation success info
 */
bool NemasController::pdo_instant_setPosition(int32_t position){
    //variables that will not be changed
    pdorxmapping.profile_velocity=0x07D0;
    pdorxmapping.mode_operation=0x01;
    pdorxmapping.motor_drive_submode=0x01;
    //variables that will be changed
    pdorxmapping.target_position=position;
    if(!WritePDO(PDO_RX_MAPPING2)) return false;
    pdorxmapping.controlword=0x3F;
    if(!WritePDO(PDO_RX_MAPPING1)) return false;
    pdorxmapping.controlword=0x0F;
    if(!WritePDO(PDO_RX_MAPPING1)) return false;
    return true;
}
/**
 * NemasController 
 * 
 * Function that configures the pdo rx mappings
 * 
 */
void NemasController::pdo_rx_config(){
    WriteSDO(CANOPEN_RX_PDO_MAPPING1_CONFIG,CANOPEN_RX_PDO_SUBINDEX_TRANSMISSION_TYPE,CANOPEN_MSG_SIZE_8_BITS,CANOPEN_RX_PDO_TRANSMISSION_TYPE_ASSYNC);
    WriteSDO(CANOPEN_RX_PDO_MAPPING2_CONFIG,CANOPEN_RX_PDO_SUBINDEX_TRANSMISSION_TYPE,CANOPEN_MSG_SIZE_8_BITS,CANOPEN_RX_PDO_TRANSMISSION_TYPE_ASSYNC);
    WriteSDO(CANOPEN_RX_PDO_MAPPING3_CONFIG,CANOPEN_RX_PDO_SUBINDEX_TRANSMISSION_TYPE,CANOPEN_MSG_SIZE_8_BITS,CANOPEN_RX_PDO_TRANSMISSION_TYPE_ASSYNC);
    WriteSDO(CANOPEN_RX_PDO_MAPPING4_CONFIG,CANOPEN_RX_PDO_SUBINDEX_TRANSMISSION_TYPE,CANOPEN_MSG_SIZE_8_BITS,CANOPEN_RX_PDO_TRANSMISSION_TYPE_ASSYNC);
}
/**
 * NemasController 
 * 
 * Function that configures the pdo tx mappings
 * 
 */
void NemasController::pdo_tx_config(){
}
/**
 * NemasController 
 * 
 * Function that sends a node guarding message
 * 
 */
void NemasController::WriteNodeguarding(){
    //8 bytes frame
    msg.ext=0;

    //Data Length 8 bytes
    msg.len = 0;

    //RTR
    msg.flags.remote=1;

    //CAN ID
    msg.id = CANOPEN_BOOTUP_ID + id;

    //Send frame
    node->SendMessage(msg);
}
/**
 * NemasController 
 * 
 * Function that writes a value to a user variable in the controller memory
 * 
 * @param  {uint8_t} subindex : subindex of the variable to be written
 * @param  {int32_t} value    : variable value
 * @return {bool}             : operation success info
 */
bool NemasController::WriteToN5(uint8_t subindex,int32_t value){
    if(!WriteSDO(CUSTOMER_STORAGE_AREA_ID,subindex,CANOPEN_MSG_SIZE_32_BITS,value)) return false;
    return true;
}
/**
 * NemasController 
 * 
 * Function that reads a value from a user variable in the controller memory
 * 
 * @param  {uint8_t} subindex : subindex of the variable to be read
 * @param  {int32_t} value    : variable value
 * @return {bool}             : operation success info
 */
bool NemasController::ReadFromN5(uint8_t subindex,int32_t &value){
    uint8_t data[4];
    if(ReadSDO(CUSTOMER_STORAGE_AREA_ID,subindex,data)){
        value = buffer_toint32(data);
        return true;
    }
    return false;
}



/**
 * NemasController 
 * 
 * Function for homing with open loop control and a limit switch
 * 
 * @return {bool}  : operation success info
 */
bool NemasController::backfoilHoming(){
    uint8_t data[4];

    bool homing = false;

    unsigned long last_time=0;

    WriteSDO(CANOPEN_CONTROL_WORD,0x00,CANOPEN_MSG_SIZE_16_BITS,0x1F);
    delay(2000);

    last_time=millis();

    Serial.println("homing...");
    while(!homing){
        //homing cycle
        ReadSDO(CANOPEN_STATUS_WORD,0x00,data);
        if((data[2] & 0x04) == 0x04) {
            homing=true;
            Serial.println("homing done!");
        }
		if(millis()- last_time > TIMEOUT_HOMING) {
            Serial.println("homing timeout");
            return false;
        }

    }
    return true;
}

/*Auxiliary functions*/

/**
 * 
 * Function that converts an array of bytes into a int32_t variable
 * 
 * @param  {uint8_t*} buffer : 
 * @return {int32_t}         : 
 */
int32_t buffer_toint32(uint8_t *buffer){
    int32_t ind = 0;
    return buffer_get_int32(buffer,&ind);
}
/**
 * 
 * Function that converts an array of bytes into a double variable
 * 
 * @param  {uint8_t*} buffer : 
 * @return {double}          : 
 */
double buffer_todouble(uint8_t *buffer){
    int32_t ind = 0;
    return buffer_get_double64(buffer,1e1,&ind);
}
/**
 * 
 * Function that enables power to the ultrassonic sensors
 * 
 */
void enable_ultrasonic_sensors(){
    digitalWrite(24,HIGH);
    digitalWrite(25,LOW);
}
/**
 * 
 * Function that disables power to the ultrassonic sensors
 * 
 */
void disable_ultrasonic_sensors(){
    digitalWrite(24,LOW);
    digitalWrite(25,HIGH);
}
/**
 *
 * Function to convert angle of attack into motor position
 * 
 * @param  {float} angle : 
 * @return {int32_t}     : 
 */
int32_t angle_to_steps(float angle){
    int32_t mapped_steps = map(angle,MIN_COM_FOILANGLE-DIF_FOILANGLE,MAX_COM_FOILANGLE+DIF_FOILANGLE,MAX_MOTOR_POSITION,MIN_MOTOR_POSITION);
    if (mapped_steps > MAX_MOTOR_POSITION) mapped_steps = MAX_MOTOR_POSITION;
    else if (mapped_steps < MIN_MOTOR_POSITION) mapped_steps = MIN_MOTOR_POSITION;
    return mapped_steps;
}
/**
 *
 * Function to convert motor position into angle of attack (front foils)
 * 
 * @param  {int32_t} steps : 
 * @return {float}         : 
 */
float steps_to_angle(int32_t steps){
    // The cast is needed to make sure MAP returns a float, otherwise it will only return an integer
    return map((float)steps,MAX_MOTOR_POSITION,MIN_MOTOR_POSITION,MIN_COM_FOILANGLE-DIF_FOILANGLE,MAX_COM_FOILANGLE+DIF_FOILANGLE);
}
/**
 *
 * Function to convert motor position to angle of attack (rear foil)
 * 
 * @param  {int32_t} steps : 
 * @return {float}         : 
 */
float steps_to_angle_rear(int32_t steps){
    // The cast is needed to make sure MAP returns a float, otherwise it will only return an integer
    return map((float)steps,MIN_REARMOTOR_POSITION,MAX_REARMOTOR_POSITION,MIN_REAR_FOILANGLE,MAX_REAR_FOILANGLE);
}

/**
 *
 * Function to convert angle of attack to motor position for rear foil
 * 
 * @param  {float} angle : 
 * @return {int32_t}     : 
 */
int32_t backfoil_angle_to_steps(float angle){
    int32_t mapped_steps = map(angle,MIN_REAR_FOILANGLE,MAX_REAR_FOILANGLE,MIN_REARMOTOR_POSITION,MAX_REARMOTOR_POSITION);
    if (mapped_steps > MAX_REARMOTOR_POSITION) mapped_steps = MAX_REARMOTOR_POSITION;
    else if (mapped_steps < MIN_REARMOTOR_POSITION) mapped_steps = MIN_REARMOTOR_POSITION;

    return mapped_steps;
}
