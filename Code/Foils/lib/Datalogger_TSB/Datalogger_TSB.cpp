/*
    Copyright (C) 2020  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include "Datalogger_TSB.h"

#if defined(__MK64FX512__) || defined(__MK66FX1M0__) // For Teensy 3.5/3.6 use faster SdFatSdioEX
    TSB_SD::TSB_SD(){
        time_t time = 0;
        Teensy3Clock.set(time); // set the RTC
	    setTime(time);
        if (!sdEx.begin()) {
            this->sd_insert = false;
        }
        else{
            sdEx.chvol();
        } 
    }
#else // For others use SdFat
    TSB_SD::TSB_SD(uint8_t SCK, uint8_t CHIP_SELECT){
        time_t time = 0;
        SPI.setSCK(SCK);
        Teensy3Clock.set(time); // set the RTC
	    setTime(time);
        if (!sd.begin(CHIP_SELECT)) {
            this->sd_insert = false;
        }
        else{
            sd.chvol();
        }
    }
#endif

TSB_Datalogger::TSB_Datalogger(String system_name, bool sd_insert){
    this->System_Name = system_name;
    this->sd_insert = sd_insert;
    setSyncProvider(TSB_Datalogger::getTeensy3Time);
}


void TSB_Datalogger::initFileName(){
    TSB_Datalogger::getTime();
    this->logname=this->System_Name+"_"+String(day())+"_"+String(month())+"-"+String(hour())+"_"+String(minute())+"_"+String(second())+".csv";
}

time_t TSB_Datalogger::getTeensy3Time()
{
  return Teensy3Clock.get();
}

void TSB_Datalogger::getTime(){
    String seconds = second();
    String minutes = minute();
	String hours = hour();
	String mday = day();
    String month_ = month();

    if( second() < 10 ) seconds = "0" + seconds;
	if( minute() < 10 ) minutes = "0" + minutes;
	if( hour() < 10 ) hours = "0" + hours;
	if( day() < 10 ) mday = "0" + mday;
    if( month() < 10 ) month_ = "0" + month_;

	this->timeString = hours + ":" + minutes + ":" + seconds;
	this->dateString = mday + "/" + month_ + "/" + String( year() );
}

void TSB_Datalogger::initLog(String header){
    noInterrupts(); // Disables the timer so that there is no file writing will we are changing file
    this->header = header;
    TSB_Datalogger::initFileName();
    if (this->sd_insert == false) return;
	this->file.open(logname.c_str(), O_WRITE | O_CREAT | O_AT_END );
	this->file.println(this->header);
	this->file.close();
    interrupts(); // Re-enables interrups
}

void TSB_Datalogger::createNewFile(){
    if (this->sd_insert == false) return;
    this->file.close();
    TSB_Datalogger::initFileName();
    this->file.open(this->logname.c_str(), O_WRITE | O_CREAT | O_AT_END );
	this->file.println(this->header);
	this->file.close();
}

void TSB_Datalogger::writeToFile(String data){
    String logEntry;
    if (this->sd_insert == false) return;
	this->file.open(logname.c_str(),  O_WRITE | O_AT_END);
	TSB_Datalogger::getTime();
	logEntry = String(this->entryId)+";"+this->timeString+";"+this->dateString+";"+data;
	this->entryId ++;
	this->file.println(logEntry);
	this->file.close();
}

