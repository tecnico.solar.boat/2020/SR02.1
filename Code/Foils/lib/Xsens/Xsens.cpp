/*
    Copyright (C) 2020  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include "Xsens.h"

XsensMsgHEX AHRSHex;
XsensMsg AHRS;

#define PACKET_START1 	0xFA
#define PACKET_START2 	0xFF
#define MTDATA			0x36

unsigned long timeout_xsens=0;

int readXsensMsg(){
	byte inByte = 0x00;
	int size = 0, index = 0, checksum=0;
	bool receiving = false, msg_begin = false, msg_got_mtdata = false, msg_good = false;
	XsensSerial.clear();
	timeout_xsens=millis();
	while(!msg_good){
		if(XsensSerial.available()){
			// Serial.println("Xsens Available");
			inByte = XsensSerial.read();
			if(receiving && msg_begin && msg_got_mtdata){
				size = inByte;
				msg_good = true;
				break;
			}
			else if((inByte == MTDATA) && receiving && msg_begin){
				msg_got_mtdata = true;
			}
			else if((inByte == PACKET_START2) && receiving){
				msg_begin = true;
			}
			else if(inByte == PACKET_START1 && !receiving){
				receiving = true;
			}
		}
		if(millis() - timeout_xsens > 30){
			// Serial.println("Xsens TimeOut");
			return -1;
		} 
	}

	if(msg_good){
		byte msg[size];
		checksum += 0xFF + 0x36 + size;
		while(index < size + 1){
			if(XsensSerial.available()){
				inByte = XsensSerial.read();
				msg[index++] = inByte;
				checksum += inByte;
			}
		}

		if ( ( checksum & 0xF ) == 0 ) {
			parseXsensMsg(msg, size);
			return 0;
		}
		else{
			// Serial.println("Xsens Failed Checksum");
			return -1;
		}
	}
	// Serial.println("Xsens Message not good");
	return -1;
}

void parseXsensMsg( byte *msg, int size){
	int i = 0;

	// Serial.println(size);
	// for(uint8_t k = 0; k < size +1; k++)
	// {
	// 	Serial.print(msg[k], HEX);
	// 	Serial.print(" ");
	// }
	// Serial.println();
	// Serial.println();

	while (i < size){
		uint16_t dataID = (msg[i] << 8) + msg[i+1];
		// Serial.println(i);
		switch ( dataID ){
			case (uint16_t)XDI::UtcTime:
				AHRSHex.ns = (msg[i+3] << 24) + (msg[i+4] << 16) + (msg[i+5] << 8) + msg[i+6];

				AHRS.ns = AHRSHex.ns;

				i = i + msg[i+2] + 3;
				break;
			case (uint16_t)XDI::EulerAngles:
				AHRSHex.roll = (msg[i+3] << 24) + (msg[i+4] << 16) + (msg[i+5] << 8) + msg[i+6];

				AHRSHex.pitch = (msg[i+7] << 24) + (msg[i+8] << 16) + (msg[i+9] << 8) + msg[i+10];

				AHRSHex.yaw = (msg[i+11] << 24) + (msg[i+12] << 16) + (msg[i+13] << 8) + msg[i+14];


				AHRS.roll = *(float*)& AHRSHex.roll;
				AHRS.pitch = *(float*)& AHRSHex.pitch;
				AHRS.yaw = *(float*)& AHRSHex.yaw;

				i = i + msg[i+2] + 3;
				break;
			case (uint16_t)XDI::Acceleration:
				AHRSHex.accX = (msg[i+3] << 24) + (msg[i+4] << 16) + (msg[i+5] << 8) + msg[i+6];

				AHRSHex.accY = (msg[i+7] << 24) + (msg[i+8] << 16) + (msg[i+9] << 8) + msg[i+10];

				AHRSHex.accZ = (msg[i+11] << 24) + (msg[i+12] << 16) + (msg[i+13] << 8) + msg[i+14];


				AHRS.accX = *(float*)& AHRSHex.accX;
				AHRS.accY = *(float*)& AHRSHex.accY;
				AHRS.accZ = *(float*)& AHRSHex.accZ;

				i = i + msg[i+2] + 3;
				break;
			case (uint16_t)XDI::RateOfTurn:
				AHRSHex.gyrX = (msg[i+3] << 24) + (msg[i+4] << 16) + (msg[i+5] << 8) + msg[i+6];

				AHRSHex.gyrY = (msg[i+7] << 24) + (msg[i+8] << 16) + (msg[i+9] << 8) + msg[i+10];

				AHRSHex.gyrZ = (msg[i+11] << 24) + (msg[i+12] << 16) + (msg[i+13] << 8) + msg[i+14];


				AHRS.gyrX = *(float*)& AHRSHex.gyrX;
				AHRS.gyrY = *(float*)& AHRSHex.gyrY;
				AHRS.gyrZ = *(float*)& AHRSHex.gyrZ;

				i = i + msg[i+2] + 3;
				break;
			case (uint16_t)XDI::LatLon:
				AHRSHex.lat = (msg[i+3] << 24) + (msg[i+4] << 16) + (msg[i+5] << 8) + msg[i+6];

				AHRSHex.lon = (msg[i+7] << 24) + (msg[i+8] << 16) + (msg[i+9] << 8) + msg[i+10];


				AHRS.lat = *(float*)& AHRSHex.lat;
				AHRS.lon = *(float*)& AHRSHex.lon;

				i = i + msg[i+2] + 3;
				break;
			case (uint16_t)XDI::VelocityXYZ:
				AHRSHex.velX = (msg[i+3] << 24) + (msg[i+4] << 16) + (msg[i+5] << 8) + msg[i+6];

				AHRSHex.velY = (msg[i+7] << 24) + (msg[i+8] << 16) + (msg[i+9] << 8) + msg[i+10];

				AHRSHex.velZ = (msg[i+11] << 24) + (msg[i+12] << 16) + (msg[i+13] << 8) + msg[i+14];

				AHRS.velX = *(float*)& AHRSHex.velX;
				AHRS.velY = *(float*)& AHRSHex.velY;
				AHRS.velZ = *(float*)& AHRSHex.velZ;
				AHRS.vel = sqrt( pow(AHRS.velX,2) + pow(AHRS.velY,2) + pow(AHRS.velZ,2) ); // Computes actual velocy in m/s
				i = i + msg[i+2] + 3;
				break;

			default:
				Serial.println("Default Xsens.cpp parseXsensMsg()"); // Mandar para o cartao SD
				Serial.println(dataID, HEX);
				// for(uint8_t k = 0; k < 60; k++)
				// {
				// 	Serial.print(msg[k], HEX);
				// 	Serial.print(" ");
				// }
				// Serial.println();
				// Serial.println();

				i = i + msg[i+2] + 3;
				break;
		}
	}
}

void inclination_reset(){
	XsensSerial.write(0xFA);
	XsensSerial.write(0xFF);
	XsensSerial.write(0xA4);
	XsensSerial.write(0x02);
	XsensSerial.write(0x00);
	XsensSerial.write(0x03);
	XsensSerial.write(0x58);
}

void alignment_reset(){
	XsensSerial.write(0xFA);
	XsensSerial.write(0xFF);
	XsensSerial.write(0xA4);
	XsensSerial.write(0x02);
	XsensSerial.write(0x00);
	XsensSerial.write(0x04);
	XsensSerial.write(0x57);
}

void printXsens(){
	Serial.println();
	Serial.println(" AHRS output:");
	Serial.print("Time (ns): "); Serial.println(AHRS.ns);
	Serial.print("Yaw: "); Serial.println(AHRS.yaw, 10);
	Serial.print("Pitch: "); Serial.println(AHRS.pitch, 10);
	Serial.print("Roll: "); Serial.println(AHRS.roll, 10);
	Serial.print("AccX: "); Serial.println(AHRS.accX, 10);
	Serial.print("AccY: "); Serial.println(AHRS.accY, 10);
	Serial.print("AccZ: "); Serial.println(AHRS.accZ, 10);
	Serial.print("GyroX: "); Serial.println(AHRS.gyrX, 10);
	Serial.print("GyroY: "); Serial.println(AHRS.gyrY, 10);
	Serial.print("GyroZ: "); Serial.println(AHRS.gyrZ, 10);
	Serial.print("Lat: "); Serial.println(AHRS.lat, 10);
	Serial.print("Lon: "); Serial.println(AHRS.lon, 10);
	Serial.print("VelX: "); Serial.println(AHRS.velX, 10);
	Serial.print("VelY: "); Serial.println(AHRS.velY, 10);
	Serial.print("VelZ: "); Serial.println(AHRS.velZ, 10);
	Serial.println();
}

String createAHRSdata(){
	String Data;
	Data=String(AHRS.yaw, 7)+";"+String( - AHRS.pitch, 7)+";"+String(AHRS.roll, 7)+";"
		+String(AHRS.accX, 7)+";"+String(AHRS.accY, 7)+";"+String(AHRS.accZ, 7)+";"+String(AHRS.gyrX, 7)+";"
		+String(AHRS.gyrY, 7)+";"+String(AHRS.gyrZ, 7)+";"+String(AHRS.lat, 10)+";"+String(AHRS.lon, 10)+";"
		+String(AHRS.velX, 7)+";"+String(AHRS.velY, 7)+";"+String(AHRS.velZ, 7)+";"+String(AHRS.vel, 7);
	return Data;
}
