/*
    Copyright (C) 2020  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#ifndef _Xsens_h
#define _Xsens_h

#include <Arduino.h>

// Serial to use with the Xsens AHRS
#define XsensSerial Serial2

#define Preamble 0xFA
#define BID 0xFF
#define MTData2 0x36

enum XDI{UtcTime = 0x1010, EulerAngles = 0x2030, Acceleration = 0x4020, 
RateOfTurn = 0x8020, LatLon = 0x5040, VelocityXYZ = 0xD010};


typedef struct xsensMessageHEX{
	uint32_t ns;
	int32_t yaw;		// Float
	int32_t pitch;		// Float
	int32_t roll;		// Float
	int32_t accX;		// Float
	int32_t accY;		// Float
	int32_t accZ;		// Float
	int32_t gyrX;		// Float
	int32_t gyrY;		// Float
	int32_t gyrZ;		// Float
	int32_t lat;		// Float
	int32_t lon;		// Float
	int32_t velX;		// Float
	int32_t velY;		// Float
	int32_t velZ;		// Float
	uint8_t fixType;
}XsensMsgHEX;


typedef struct xsensMessage{
	uint32_t ns;
	float yaw;		// Float
	float pitch;	// Float
	float roll;		// Float
	float accX;		// Float
	float accY;		// Float
	float accZ;		// Float
	float gyrX;		// Float
	float gyrY;		// Float
	float gyrZ;		// Float
	float lat;		// Float
	float lon;		// Float
	float velX;		// Float
	float velY;		// Float
	float velZ;		// Float
	float vel;		// Floar
	uint8_t fixType;
}XsensMsg;


// Global variables to hold the data that comes from the AHRS
// extern XsensMsgHEX AHRSHex;
// extern XsensMsg AHRS;

int readXsensMsg();
void parseXsensMsg( byte *msg, int size);
void printXsens();

String createAHRSdata();
void init_Xsensmessages();
void send_Xsensmessages();
void inclination_reset();
void alignment_reset();

#endif