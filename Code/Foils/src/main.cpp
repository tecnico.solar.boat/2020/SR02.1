/*
    Copyright (C) 2020  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/


#include "Nemas_Controller.h"
#include "Datalogger_TSB.h"
#include "RunningMedian.h"

/**
 * Object that implements the running median functions
 * @param  {11} Array size:  
 */
RunningMedian ultrasonic_samples = RunningMedian(11);

/**
 * Object that handles the CAN communication with nemas controllers
 */
NemasComms controller_comms;

/**
 * Objects that implement the controll functions of each nema controller
 * @param  {NEMAS*_ID} ID of the controller:  
 * @param  {controller_comms} The CAN communication object: 
 */

NemasController nemas1(NEMAS1_ID,&controller_comms);
NemasController nemas2(NEMAS2_ID,&controller_comms);
NemasController nemas3(NEMAS3_ID,&controller_comms);

/**
 * TSB data logger Objects
 */
TSB_SD SdCard; // For Teensy 3.5/3.6 there is no need to specify any pins
TSB_Datalogger file1("foils",SdCard.sd_insert); // This specifies the system name, it will be the prefix of the file name

/**
 * Object that implements the handler for receiving general CAN messages
 */
TSB_CAN canMessageHandler;

/**
 * Objects that implement the interval timers
 */
IntervalTimer sonic_timer;
IntervalTimer print_timer;
IntervalTimer foilcontroll;
IntervalTimer send_can_timer;

/**
 * Object that implements the Foils Controller
 */
LQRController lqrcontroller;

/**
 * Global variables
 */
int LED_pin = 13;
uint8_t incomingByte;
bool sonic=false;
bool send_sonic_flag=false;
bool flag_rear_stepper_active=false;
int32_t position=0;
int motor_turns=0;
unsigned long start_heave_time=0;
bool led_state = LOW;
uint32_t lastPosMillis = 0;
unsigned long timer_prints=0;
unsigned long timer_disable_rear_stepper=0;

/**
 * CAN messages definition (can0)
 */
CAN_message_t AHRSData0;
CAN_message_t AHRSData1;
CAN_message_t AHRSData2;
CAN_message_t us_angle_state_mode;

/**
 * Macros definition
 */

//Number of modes
#define N_MODES 4

//Modes of controller
#define MANUALMODE 0
#define ROLLMODE 1
#define HEAVEMODE 2
#define FULLMODE 3

//States of controller
#define OFF 0
#define HOMING 1
#define READY 2
#define ON 3
#define ERROR 4

//Ultrassonic sensors ids
#define USENSOR1 1
#define USENSOR2 2

//boat max height reference
#define MAX_HEIGHT_REF 80

/**
 * Initializations are done here
 * Code run only once
 */
void setup() {
	//Led pin setup
	pinMode(LED_pin, OUTPUT);

	//Led pin initialization on OFF state
  	digitalWrite(LED_pin,LOW);

	//Mosfets pins setup
	pinMode(24,OUTPUT);
	pinMode(25,OUTPUT);

	//Mosfets initialization on OFF state
	digitalWrite(24,LOW);
	digitalWrite(25,HIGH);

	//Initialization of the Serial ports
	Serial.begin(9600); //Serial port for usb debug
	//Initialization of the Serial1 port to communicate with the ultrassonic sensor
	SenixSerial.begin(9600);
	SenixSerial.clear();
	pinMode(SERIAL1_DIR,OUTPUT);
	//Initialization of the Serial1 port to communicate with Xsens AHRS
    XsensSerial.begin(2000000);
    XsensSerial.clear();

	//Wait 2 seconds to let the hardware settle
	delay(2000);

	//SD card header definition and initialization
	file1.initLog("ID;Time;Date;Yaw;Pitch;Roll;AccX;AccY;AccZ;gyroX;gyroY;gyroZ;Latitude;Longitude;VelX;VelY;VelZ;Velocity;Heave;State;Mode;Angle_Left;Angle_Right;Real_Angle_Left;Real_Angle_Right;Real_Rear_Angle;Com_mode;Dif_mode;H_ref;Roll_ref;Yaw_filt;Iq_left;Iq_right");

	//Initialization of the CAN ports
	Can0.begin(1000000);
	Can1.begin(1000000);

	//Can ports are attached to to the respective handlers
	Can1.attachObj(&controller_comms);
    Can0.attachObj(&canMessageHandler);
	canMessageHandler.attachGeneralHandler();
	controller_comms.attachGeneralHandler();

	//Initialization of the Can messages (can0 only)
    init_CAN_messages();

	//Timers initialization
	send_can_timer.begin(send_CAN,0.1*1e6);
	//print_timer.begin(logdata,0.1*1e6);
	foilcontroll.begin(controll,CONTROL_RATE*1e6);

	//Wait 10 seconds before startup
	delay(10000);

	//Send an SDO request to reset all fault conditions on the nemas controllers
	nemas1.WriteSDO(CANOPEN_CONTROL_WORD,0,CANOPEN_MSG_SIZE_16_BITS,0x80);
	nemas2.WriteSDO(CANOPEN_CONTROL_WORD,0,CANOPEN_MSG_SIZE_16_BITS,0x80);
	nemas3.WriteSDO(CANOPEN_CONTROL_WORD,0,CANOPEN_MSG_SIZE_16_BITS,0x80);

	//Code not used, implements a routine to see if all the nemas controllers are online
	//nemas3.WriteSDO(0x607D,2,CANOPEN_MSG_SIZE_32_BITS,MAX_REARMOTOR_POSITION);
	/*
	//see if all controllers are online
	elapsedMillis boot_time;
	while(!(controller_comms.boot_nemas1.online && controller_comms.boot_nemas2.online)){
		if(boot_time > 10000){
			lqrcontroller.state=ERROR;
		}
	}

	if(controller_comms.boot_nemas1.online && controller_comms.boot_nemas2.online){
		lqrcontroller.mode=FULLMODE;
		lqrcontroller.state=OFF;
	}
	*/

	//Initialize the foils controller mode and state
	lqrcontroller.mode=FULLMODE;
	lqrcontroller.state=OFF;

	//Gets the value of the rear foil angle from CAN into the foils controller object
	lqrcontroller.angle_rear = canMessageHandler.screen.angle_setpoint;

	//Sets the nemas controller NMT mode to operational (Interval of 1 second between each message)
	delay(1000);
	nemas1.WriteNMT(CANOPEN_NMT_SWITCH_OPERATIONAL); 
	delay(1000);
	nemas2.WriteNMT(CANOPEN_NMT_SWITCH_OPERATIONAL); 
	delay(1000);

	//Sets the nemas controller state to switch on disabled
	nemas1.stateMachine(SWITCH_ON_DISABLED);
	delay(1000);
	nemas2.stateMachine(SWITCH_ON_DISABLED);
	delay(1000);
	nemas3.stateMachine(SWITCH_ON_DISABLED);

	//Translates the foil angle to motor turns 
	motor_turns = angle_to_steps(DEFAULT_ANGLE);

	/*
	* Read a user variable from the nemas controller with id 1
	* This variable indicates if the homing has already been done
	* on all the nemas controllers
	*/
	int32_t homing_from_N5=0;
	nemas1.ReadFromN5(0x01,homing_from_N5);
	lqrcontroller.homing_done=homing_from_N5;

	/* 
	* If the homing was already done, switch the nemas controllers with id 1 and 2
	* to the switched on state, change the mode to profile position and then set
	* the state to operation enabled
	*/
	if(lqrcontroller.homing_done){
		nemas1.stateMachine(SWITCHED_ON);
		nemas1.setOperatingMode(CANOPEN_OPERATION_MODE_PROFILE_POSITION);
		nemas1.stateMachine(OPERATION_ENABLED);
		nemas2.stateMachine(SWITCHED_ON);
		nemas2.setOperatingMode(CANOPEN_OPERATION_MODE_PROFILE_POSITION);
		nemas2.stateMachine(OPERATION_ENABLED);
	}

	timer_prints=millis();

	//This next code is part of the watchdog timer (not being used)

	// Serial.println("Reason for last Reset: ");

    // if (RCM_SRS1 & RCM_SRS1_SACKERR)   Serial.println("Stop Mode Acknowledge Error Reset");
    // if (RCM_SRS1 & RCM_SRS1_MDM_AP)    Serial.println("MDM-AP Reset");
    // if (RCM_SRS1 & RCM_SRS1_SW)        Serial.println("Software Reset");                   // reboot with SCB_AIRCR = 0x05FA0004
    // if (RCM_SRS1 & RCM_SRS1_LOCKUP)    Serial.println("Core Lockup Event Reset");
    // if (RCM_SRS0 & RCM_SRS0_POR)       Serial.println("Power-on Reset");                   // removed / applied power
    // if (RCM_SRS0 & RCM_SRS0_PIN)       Serial.println("External Pin Reset");               // Reboot with software download
    // if (RCM_SRS0 & RCM_SRS0_WDOG)      Serial.println("Watchdog(COP) Reset");              // WDT timed out
    // if (RCM_SRS0 & RCM_SRS0_LOC)       Serial.println("Loss of External Clock Reset");
    // if (RCM_SRS0 & RCM_SRS0_LOL)       Serial.println("Loss of Lock in PLL Reset");
    // if (RCM_SRS0 & RCM_SRS0_LVD)       Serial.println("Low-voltage Detect Reset");
    // Serial.println();


	// // Setup WDT
    // noInterrupts();                                         // don't allow interrupts while setting up WDOG
    // WDOG_UNLOCK = WDOG_UNLOCK_SEQ1;                         // unlock access to WDOG registers
    // WDOG_UNLOCK = WDOG_UNLOCK_SEQ2;
    // delayMicroseconds(1);                                   // Need to wait a bit..

    // // we will use 10 second WDT timeout (e.g. you must reset it in < 10 sec or a boot occurs)
    // WDOG_TOVALH = 0x2AEA;
    // WDOG_TOVALL = 0x5400;

    // // This sets prescale clock so that the watchdog timer ticks at 7.2MHz
    // WDOG_PRESC  = 0x400;

    // // Set options to enable WDT. You must always do this as a SINGLE write to WDOG_CTRLH
    // WDOG_STCTRLH |= WDOG_STCTRLH_ALLOWUPDATE |
    //     WDOG_STCTRLH_WDOGEN | WDOG_STCTRLH_WAITEN |
    //     WDOG_STCTRLH_STOPEN | WDOG_STCTRLH_CLKSRC;
    // interrupts();
}

/**
 * Where the magic happens!
 */
void loop() {

	//Get the boat speed from the Xsens AHRS object
	lqrcontroller.boat_speed = AHRS.vel;

	//Get height reference from CAN bus
	lqrcontroller.h_ref = -canMessageHandler.screen.dist_setpoint;

	//Check if the received height reference value is between the limits, if not, the closest limit is imposed
	if (lqrcontroller.h_ref < -MAX_HEIGHT_REF) lqrcontroller.h_ref = -MAX_HEIGHT_REF;
	else if (lqrcontroller.h_ref > 0) lqrcontroller.h_ref = 0;

	/*
	* If the foils controller is not ready and if the foils are not enabled,
	* process the screen mode change received from the CAN bus
	*/
	if(!lqrcontroller.ready && !lqrcontroller.foils_enabled && canMessageHandler.screen.mode_setpoint){
		//Change mode
		lqrcontroller.mode++;
		lqrcontroller.mode = lqrcontroller.mode%N_MODES;
		canMessageHandler.screen.mode_setpoint=0;
	}

	/*
	* If the rear foil angle reference received from CAN is different from the one being used
	* in the foils controller, change the controller value to the received one.
	* Then, if the foils controller is on the heave mode or the full mode, the foils are not enabled
	* and the homing has already been done, change the rear foil angle.
	*/
	if(lqrcontroller.angle_rear != canMessageHandler.screen.angle_setpoint){
		lqrcontroller.angle_rear = canMessageHandler.screen.angle_setpoint;
		if( (lqrcontroller.mode==HEAVEMODE || lqrcontroller.mode==FULLMODE) && !lqrcontroller.foils_enabled && lqrcontroller.homing_done ) {
			nemas3.setOperatingMode(CANOPEN_OPERATION_MODE_PROFILE_POSITION);
			nemas3.stateMachine(OPERATION_ENABLED);
			nemas3.completed_setPosition(backfoil_angle_to_steps(lqrcontroller.angle_rear));
			timer_disable_rear_stepper=millis();
			flag_rear_stepper_active = true;			
		}
	}

	/*
	* After 3 seconds, since the rear foil angle was changed, if the controller is in operation enable
	* and foils are not enabled, disable the rear foil.
	*/
	if( (millis()-timer_disable_rear_stepper>3000) && !lqrcontroller.foils_enabled && flag_rear_stepper_active == true){
		nemas3.stateMachine(SWITCH_ON_DISABLED);
		flag_rear_stepper_active = false;
	}

	/*
	* If a state change is received in the CAN bus, the following conditions 
	* change the foils controller state to the OFF or READY state.
	*/
	if(canMessageHandler.screen.controller_state){
		/*
		* If the homing has not been done, do homing.
		*/
		if(!lqrcontroller.homing_done){
			full_homing();
		}

		/*
		* If the foils controller is not ready, and the homing has been done
		* the following conditions process the foils controller state change
		* from OFF to READY
		*/
		if(!lqrcontroller.ready && lqrcontroller.homing_done){
			/*
			* If the foils controller mode is HEAVEMODE or FULLMODE enable the ultrassonic sensor.
			*/
			if(lqrcontroller.mode==HEAVEMODE || lqrcontroller.mode==FULLMODE){
				enable_ultrasonic_sensors();
				sonic_timer.begin(read_ultrasonic,0.1*pow10(6));
				lqrcontroller.ultrasonic_connected=true;
				delay(1000);
				//if the communication with ultrassonic sensor is not working, disable ultrassonic sensor
				if(lqrcontroller.heave==0){
					disable_ultrasonic_sensors();
					sonic_timer.end();
					lqrcontroller.ultrasonic_connected=false;
				}
			}
			/*
			* If the communication with the ultrassonic sensor is working or the foils controller mode is MANUALMODE 
			* OR ROLLMODE, the foils controller ready flag is set to true, the controller reset is done, the front nemas
			* controllers move to the default angle and the controller state is changed from OFF to READY.
			*/
			if(lqrcontroller.ultrasonic_connected || lqrcontroller.mode==MANUALMODE || lqrcontroller.mode==ROLLMODE){
				lqrcontroller.ready=true;
				//alignment_reset();
				lqrcontroller.reset();
				delay(1000);
				nemas1.stateMachine(OPERATION_ENABLED);
				nemas2.stateMachine(OPERATION_ENABLED);
				nemas1.instant_setPosition(angle_to_steps(DEFAULT_ANGLE));
				nemas2.instant_setPosition(angle_to_steps(DEFAULT_ANGLE));
				lqrcontroller.state=READY;

			}
		}

		/*
		* If the foils controller is ready, and the foils are not enabled
		* the following conditions process the foils controller state change
		* from READY to OFF
		*/
		else if(lqrcontroller.ready && !lqrcontroller.foils_enabled){
			//switch mode operation
			if(lqrcontroller.mode==HEAVEMODE || lqrcontroller.mode==FULLMODE){
				disable_ultrasonic_sensors();
				sonic_timer.end();
				lqrcontroller.heave = 0;
			}
			lqrcontroller.ready=false;
			nemas1.stateMachine(SWITCH_ON_DISABLED);
			nemas2.stateMachine(SWITCH_ON_DISABLED);
			lqrcontroller.state=OFF;
		}
		canMessageHandler.screen.controller_state=false;
	}

	/*
	* If a homing command is received in the CAN bus, the foils controller is not ready, and the foils are not enabled
	* the following condition calls a function to perform homing on all nemas controllers
	*/
	if(canMessageHandler.screen.homing && !lqrcontroller.ready && !lqrcontroller.foils_enabled ){
		full_homing();
	}

	/*
	* If the foils controller is ready, the foils controller is not enabled and the boat speed is greater or equal to the
	* foils start speed the following condition changes the foils controller state from READY to ON
	*/
	if(!lqrcontroller.foils_enabled && lqrcontroller.ready && (lqrcontroller.boat_speed >= FOILS_START_SPEED)){
		lqrcontroller.foils_enabled=true;
		lqrcontroller.state=ON;
	}

	/*
	* If the the foils controller is enabled and the boat speed is less that the
	* foils stop speed the following condition changes the foils controller state from ON to READY.
	* The front nemas controllers also move to the default angle.
	*/
	if(lqrcontroller.foils_enabled && lqrcontroller.boat_speed < FOILS_STOP_SPEED){
		lqrcontroller.foils_enabled=false;
		lqrcontroller.reset();
		lqrcontroller.state=READY;
		nemas1.instant_setPosition(angle_to_steps(lqrcontroller.angle_left));
		nemas2.instant_setPosition(angle_to_steps(lqrcontroller.angle_right));

	}

	/*
	* If the foils controller is ready and not enabled and also the foils controller mode is either MANUALMODE or ROLLMODE the 
	* following conditions enable the manual controll, by the pilot, of the front nemas controllers angle.
	*/
	if(lqrcontroller.ready && !lqrcontroller.foils_enabled && (lqrcontroller.mode == MANUALMODE || lqrcontroller.mode == ROLLMODE)){

		//The left and right angles are received from the CAN bus
		lqrcontroller.angle_left = canMessageHandler.screen.angle_setpoint;
		lqrcontroller.angle_right = canMessageHandler.screen.angle_setpoint;

		//Connditions to impose the max and min front foils angles
		if (lqrcontroller.angle_left > MAX_COM_FOILANGLE) lqrcontroller.angle_left = MAX_COM_FOILANGLE;
		else if (lqrcontroller.angle_left < MIN_COM_FOILANGLE) lqrcontroller.angle_left = MIN_COM_FOILANGLE;
		if (lqrcontroller.angle_right > MAX_COM_FOILANGLE) lqrcontroller.angle_right = MAX_COM_FOILANGLE;
		else if (lqrcontroller.angle_right < MIN_COM_FOILANGLE) lqrcontroller.angle_right = MIN_COM_FOILANGLE;

		//The foils controller auto common angle is set, since left and right angles are the same, the right angle is used
		lqrcontroller.auto_com_angle = lqrcontroller.angle_right;

		//The front nemas controllers move accordingly to the angle manually set
		nemas1.instant_setPosition(angle_to_steps(lqrcontroller.angle_left));
		nemas2.instant_setPosition(angle_to_steps(lqrcontroller.angle_right));
	}

	/*
	* If a new log file command is received in the CAN bus, the following condition 
	* creates a new log file in the sd card
	*/
	if (canMessageHandler.screen.newFile == true){
		// Code to create new log file
		noInterrupts();
		file1.createNewFile(); // Creates a new log file
		canMessageHandler.screen.newFile = false;
		//Debug Information
		//Serial.println("New file created!");
		interrupts();
	}

	/*
	* If the foils controller is not enabled the following code reads data from the nemas controllers using SDOs.
	*/
	if(!lqrcontroller.foils_enabled){
		//variables that are going to be read
		int32_t actual_position_nemas1=0;
		int32_t actual_position_nemas2=0;
		int32_t actual_position_nemas3=0;
		int32_t current_Iq_nema1;
		int32_t current_Iq_nema2;
		//int32_t current_Iq_nema3;

		//Get the Iq currents from front nemas controllers
		nemas1.getCurrent_Iq(current_Iq_nema1);
		nemas2.getCurrent_Iq(current_Iq_nema2);
		//nemas3.getCurrent_Iq(current_Iq_nema3);

		//Get the actual position of all nemas controllers, front and back
		nemas1.getActualPosition(actual_position_nemas1);
		nemas2.getActualPosition(actual_position_nemas2);
		nemas3.getActualPosition(actual_position_nemas3);

		//Process the received position into angles and store this data into the foils controller variables
		nemas1.actualPosition=steps_to_angle(actual_position_nemas1);
		nemas2.actualPosition=steps_to_angle(actual_position_nemas2);
		nemas3.actualPosition=steps_to_angle_rear(actual_position_nemas3);
		nemas1.current_Iq = current_Iq_nema1;
		nemas2.current_Iq = current_Iq_nema2;

		//Debug Information
		/*if(millis()-timer_prints>1000){
			Serial.print("actual position 3: ");
			Serial.println(nemas3.actualPosition);
			timer_prints=millis();
		}*/
		
	}

	/*
	* DEBUG AND TEST 
	* The following commented code is only to be used in debug or test scenarios (normally when the boat is not on water).
	* It enables the foils movement controll using the computer keyboard (USE WITH CAUTION!)
	*/
	
	/*
	if (Serial.available()) {
		incomingByte = Serial.read();
		Serial.println((char)incomingByte);
	}

	// test command for z search and homing
	if(incomingByte=='h'){
		nemas2.stateMachine(SWITCH_ON_DISABLED);
		delay(1000);
		nemas2.setOperatingMode(CANOPEN_OPERATION_MODE_HOMING);
		delay(1000);
		nemas2.stateMachine(OPERATION_ENABLED);
		delay(2000);
		nemas2.stateMachine(SWITCH_ON_DISABLED);
		delay(1000);
		nemas2.setOperatingMode(CANOPEN_OPERATION_MODE_PROFILE_POSITION);
		delay(1000);
		nemas2.stateMachine(OPERATION_ENABLED);
		Serial.println("homing");
		
		// if (nemas3.startHoming()) {
		// 	lqrcontroller.homing_done = true;
		// 	Serial.println("z point found and homing done");
		// } else {
		// 	lqrcontroller.homing_done = false;
		// 	lqrcontroller.state = ERROR;
		// 	Serial.println("homing error (timeout for z search or homing)");
		// }
		incomingByte=0;
	}

	// switch on profile position
	if(incomingByte=='p'){
		nemas1.stateMachine(SWITCH_ON_DISABLED);
		nemas1.setOperatingMode(CANOPEN_OPERATION_MODE_PROFILE_POSITION);
		nemas1.stateMachine(OPERATION_ENABLED);
		Serial.println("operation enabled");
		incomingByte=0;
	}

	// press esc key to stop nema
	if(incomingByte==27){
		if(!nemas1.stateMachine(SWITCH_ON_DISABLED)) Serial.println("Left: Error on switch on disabled");
		else Serial.println("Left: Safe");
		if(!nemas2.stateMachine(SWITCH_ON_DISABLED)) Serial.println("Right: Error on switch on disabled");
		else Serial.println("Right: Safe");
		incomingByte=0;
	}


	// press q to rise boat speed
	if(incomingByte=='q'){
		lqrcontroller.boat_speed++;
		Serial.print("speed: ");
		Serial.println(lqrcontroller.boat_speed);
		incomingByte=0;
	}

	// press a to lower boat speed
	if(incomingByte=='a'){
		lqrcontroller.boat_speed--;
		Serial.print("speed: ");
		Serial.println(lqrcontroller.boat_speed);
		incomingByte=0;
	}

	if(incomingByte=='p'){
		motor_turns++;
		nemas1.instant_setPosition(motor_turns);
		//nemas2.instant_setPosition(angle_to_steps(lqrcontroller.angle_right));
		Serial.print("valor no stepper: ");
		Serial.println(motor_turns);
		incomingByte=0;
	}

	if(incomingByte=='s'){
		motor_turns--;
		nemas1.instant_setPosition(motor_turns);
		//nemas2.instant_setPosition(angle_to_steps(lqrcontroller.angle_right));
		Serial.print("valor no stepper: ");
		Serial.println(motor_turns);
		incomingByte=0;
	}

	if(incomingByte=='b'){
		if(!nemas1.stateMachine(SWITCH_ON_DISABLED)) Serial.println("Left: Error on switch on disabled");
		if(!nemas1.setOperatingMode(CANOPEN_OPERATION_MODE_PROFILE_POSITION)) Serial.println("Left: Error setting profile position");
		if(!nemas1.stateMachine(OPERATION_ENABLED)) Serial.println("Left: Error on operation enabled");
		else Serial.println("Left: Operation enabled");
		if(!nemas2.stateMachine(SWITCH_ON_DISABLED)) Serial.println("Right: Error on switch on disabled");
		if(!nemas2.setOperatingMode(CANOPEN_OPERATION_MODE_PROFILE_POSITION)) Serial.println("Right: Error setting profile position");
		if(!nemas2.stateMachine(OPERATION_ENABLED)) Serial.println("Right: Error on operation enabled");
		else Serial.println("Right: Operation enabled");
		incomingByte=0;
	}

	if(incomingByte=='m'){

		int32_t actual_position_nemas1=0;
		int32_t actual_position_nemas2=0;

		if(!nemas1.getActualPosition(actual_position_nemas1)) Serial.println("Error Left");
		if(!nemas2.getActualPosition(actual_position_nemas2)) Serial.println("Error Right");

		Serial.print("Left: ");
		Serial.print(actual_position_nemas1);
		Serial.print(" Right: ");
		Serial.println(actual_position_nemas2);
		incomingByte=0;
	}

	if(incomingByte=='s'){
		nemas1.WriteSYNC();
		nemas2.WriteSYNC();
		incomingByte=0;
	}

	if(incomingByte=='i'){
		uint8_t data_softlimit[4];

		nemas1.ReadSDO(0x6083,0x00,data_softlimit);
		delay(500);
		Serial.print("profile acceleration 1: ");
		Serial.println(buffer_toint32(data_softlimit));

		nemas2.ReadSDO(0x6083,0x00,data_softlimit);
		delay(500);
		Serial.print("profile acceleration 2: ");
		Serial.println(buffer_toint32(data_softlimit));



		nemas1.ReadSDO(0x6084,0x00,data_softlimit);
		delay(500);
		Serial.print("profile deceleration 1: ");
		Serial.println(buffer_toint32(data_softlimit));
		
        nemas2.ReadSDO(0x6084,0x00,data_softlimit);
		delay(500);
		Serial.print("profile deceleration 2: ");
        Serial.println(buffer_toint32(data_softlimit));


		nemas1.ReadSDO(0x608F,0x01,data_softlimit);
		delay(500);
		Serial.print("encoder increments 1: ");
		Serial.println(buffer_toint32(data_softlimit));
		
        nemas2.ReadSDO(0x608F,0x01,data_softlimit);
		delay(500);
		Serial.print("encoder increments 2: ");
        Serial.println(buffer_toint32(data_softlimit));


		nemas1.ReadSDO(0x608F,0x02,data_softlimit);
		delay(500);
		Serial.print("motor revolutions 1: ");
		Serial.println(buffer_toint32(data_softlimit));
		
        nemas2.ReadSDO(0x608F,0x02,data_softlimit);
		delay(500);
		Serial.print("motor revolutions 2: ");
        Serial.println(buffer_toint32(data_softlimit));


		nemas1.ReadSDO(0x607D,0x01,data_softlimit);
		delay(500);
		Serial.print("min software limit 1: ");
		Serial.println(buffer_toint32(data_softlimit));
		
        nemas2.ReadSDO(0x607D,0x01,data_softlimit);
		delay(500);
		Serial.print("min software limit 2: ");
        Serial.println(buffer_toint32(data_softlimit));


		nemas1.ReadSDO(0x607D,0x02,data_softlimit);
		delay(500);
		Serial.print("max software limit 1: ");
		Serial.println(buffer_toint32(data_softlimit));
		
        nemas2.ReadSDO(0x607D,0x02,data_softlimit);
		delay(500);
		Serial.print("max software limit 2: ");
        Serial.println(buffer_toint32(data_softlimit));


		nemas1.ReadSDO(0x60F2,0x00,data_softlimit);
		delay(500);
		Serial.print("positioning option code 1: ");
		Serial.println(buffer_toint32(data_softlimit));
		
        nemas2.ReadSDO(0x60F2,0x00,data_softlimit);
		delay(500);
		Serial.print("positioning option code 2: ");
        Serial.println(buffer_toint32(data_softlimit));


		nemas1.ReadSDO(0x2031,0x00,data_softlimit);
		delay(500);
		Serial.print("max current 1: ");
		Serial.println(buffer_toint32(data_softlimit));
		
        nemas2.ReadSDO(0x2031,0x00,data_softlimit);
		delay(500);
		Serial.print("max current 2: ");
        Serial.println(buffer_toint32(data_softlimit));

	
		incomingByte=0;
	}
	*/
	
	// // Reset whatch dog timer
    // noInterrupts();
    // WDOG_REFRESH = 0xA602;
    // WDOG_REFRESH = 0xB480;
    // interrupts();


}

/**
 * The following function reads the ultrasonic sensor value 
 */
void read_ultrasonic(){
	//The sensor outputs a positive value of height, but since the controller needs a negative value, a negative value retreived
	float sensor_reading = -readSensor(USENSOR1);

	//Filter the sensor readings
	ultrasonic_samples.add(sensor_reading);
	lqrcontroller.heave = ultrasonic_samples.getMedian();

	//lqrcontroller.heave = -readSensor(USENSOR1);

	//calculate heave at center of mass
	//lqrcontroller.heave = -( readSensor(USENSOR1) - DIST_US_TO_CM*sin(-AHRS.pitch*PI/180) );

}

/**
 * Where the magic really happens!
 * Interruption code that controlls the boat foils.
 */
void controll(){
	//Read data from AHRS
	readXsensMsg();

	// if(lqrcontroller.boat_speed < HEAVE_STOP_SPEED) {
	// 	lqrcontroller.DIF_FOILANGLE = 3;
	// } else {
	// 	lqrcontroller.DIF_FOILANGLE = 1.5;
	// }

	/**
	* If the foils controller is enabled the following code controlls the boat foils
	*/
	if(lqrcontroller.foils_enabled){
		/**
		* If the foils controller is on ROLLMODE or FULLMODE the controller roll mode function is called
		*/
		if(lqrcontroller.mode == ROLLMODE || lqrcontroller.mode == FULLMODE) {
			//noInterrupts();
			lqrcontroller.roll_mode();
			//interrupts();
		}
		/**
		* If the foils controller does not have the heave mode enabled, 
		* the foils controller mode is either HEAVEMODE or FULLMODE and the boat speed is
		* greater than the heave start speed, the heave mode is enabled.
		* The rear foil is also enabled.
		*/
		if(!lqrcontroller.heave_enabled && (lqrcontroller.mode == HEAVEMODE || lqrcontroller.mode == FULLMODE) && lqrcontroller.boat_speed > HEAVE_START_SPEED) {
			lqrcontroller.heave_enabled = true;
			lqrcontroller.flag_first_loop = true;
			//start rear stepper
			nemas3.setOperatingMode(CANOPEN_OPERATION_MODE_PROFILE_POSITION);
			nemas3.stateMachine(OPERATION_ENABLED);
			start_heave_time = millis();
		}
		/**
		* If the foils controller has the heave mode enabled and the boat speed is
		* less than the heave start speed, the heave mode is disabled.
		* The rear foil is also disabled.
		*/
		if (lqrcontroller.heave_enabled && lqrcontroller.boat_speed < HEAVE_STOP_SPEED) {
			lqrcontroller.heave_enabled = false;
			lqrcontroller.reset_heave();
			//stop rear stepper
			nemas3.stateMachine(SWITCH_ON_DISABLED);
		}
		/**
		* If the foils controller has the heave mode enabled and 1 second has elapsed from heave being enabled,
		* the heave mode function is called.
		*/
		if (lqrcontroller.heave_enabled && (millis()-start_heave_time)>1000 ) {
			//noInterrupts();
			lqrcontroller.heave_mode();
			//interrupts();
		}

		//The left and right foil angles are calculated
		lqrcontroller.angle_left = lqrcontroller.auto_com_angle + lqrcontroller.auto_dif_angle;
		lqrcontroller.angle_right = lqrcontroller.auto_com_angle - lqrcontroller.auto_dif_angle;

		//The nemas controllers move to the angles calculated by the foils controller
		nemas3.instant_setPosition(backfoil_angle_to_steps(lqrcontroller.angle_rear));
		nemas2.instant_setPosition(angle_to_steps(lqrcontroller.angle_right));
		nemas1.instant_setPosition(angle_to_steps(lqrcontroller.angle_left));
		
		//The following code reads data from the nemas controllers using SDOs.
		int32_t actual_position_nemas1=0;
		int32_t actual_position_nemas2=0;
		int32_t actual_position_nemas3=0;
		int32_t current_Iq_nema1=0;
		int32_t current_Iq_nema2=0;

		nemas1.getActualPosition(actual_position_nemas1);
		nemas2.getActualPosition(actual_position_nemas2);
		nemas3.getActualPosition(actual_position_nemas3);
		nemas1.getCurrent_Iq(current_Iq_nema1);
		nemas2.getCurrent_Iq(current_Iq_nema2);

		nemas1.actualPosition=steps_to_angle(actual_position_nemas1);
		nemas2.actualPosition=steps_to_angle(actual_position_nemas2);
		nemas3.actualPosition=steps_to_angle_rear(actual_position_nemas3);
		nemas1.current_Iq = current_Iq_nema1;
		nemas2.current_Iq = current_Iq_nema2;

		//Debug Information
		/*if(millis()-timer_prints>1000){
			Serial.print("Left nemas Current: ");
			Serial.println(nemas1.current_Iq);
			Serial.print("Left aux Current: ");
			Serial.println(current_Iq_nema1);
			timer_prints=millis();
		}*/
	}
}

/**
 * Function that does the setup of all the CAN messages sent by the foils controller, the definition of ~
 * message id and message length are done in this function.
 */
void init_CAN_messages(){
	AHRSData0.ext = 0;
	AHRSData1.ext = 0;
	AHRSData2.ext = 0;
	us_angle_state_mode.ext = 0;
    AHRSData0.id = 0x602;
	AHRSData1.id = 0x60A;
	AHRSData2.id = 0x61A;
	us_angle_state_mode.id = 0x612;
	AHRSData0.len = 8;
	AHRSData1.len = 8;
	AHRSData2.len = 8;
	us_angle_state_mode.len = 8;
}

/**
 * Funtion that sends the CAN messages to the boat CAN bus
 */
void send_CAN(){
	int32_t ind = 0;

	//ahrs data0
    buffer_append_float16(AHRSData0.buf, AHRS.vel,1e2, &ind);
	buffer_append_float16(AHRSData0.buf, AHRS.pitch,1e2, &ind);
	buffer_append_float16(AHRSData0.buf, AHRS.roll,1e2, &ind);
	buffer_append_float16(AHRSData0.buf, AHRS.yaw,1e2, &ind);
	Can0.write(AHRSData0);

	//ahrs data1
	ind=0;
	buffer_append_float32_auto(AHRSData1.buf, AHRS.lat, &ind);
	buffer_append_float32_auto(AHRSData1.buf, AHRS.lon, &ind);
	Can0.write(AHRSData1);

	//ahrs data2
	ind=0;
	buffer_append_float16(AHRSData2.buf, AHRS.gyrX,1e2, &ind);
	buffer_append_float16(AHRSData2.buf, AHRS.gyrY,1e2, &ind);
	buffer_append_float16(AHRSData2.buf,nemas3.actualPosition,1e2,&ind);
	AHRSData2.buf[ind++]=(uint8_t)-lqrcontroller.h_ref;
	AHRSData2.buf[ind++]=canMessageHandler.screen.angle_setpoint*10;
	Can0.write(AHRSData2);

	//us_angle, mode and angles
	ind=0;
    buffer_append_float16(us_angle_state_mode.buf, -1*lqrcontroller.heave, 1e2, &ind);
	buffer_append_float16(us_angle_state_mode.buf, nemas1.actualPosition, 1e2, &ind);
	buffer_append_float16(us_angle_state_mode.buf, nemas2.actualPosition, 1e2, &ind);
	us_angle_state_mode.buf[ind++]=lqrcontroller.state;
	us_angle_state_mode.buf[ind++]=lqrcontroller.mode;
    Can0.write(us_angle_state_mode);

	//write data to the current log file
	logdata();
}

/**
 * Funtion that writes the data on the log file stored in the sd card
 */
void logdata() {
	//Led blink to show data is being writen
	led_state = !led_state;
   	digitalWrite(13,led_state);

	//Create line
	String data = createAHRSdata() + ";" + String(lqrcontroller.heave) + ";" + String(lqrcontroller.state) + ";" +
		String(lqrcontroller.mode) + ";" + String(lqrcontroller.angle_left) + ";" +
		String(lqrcontroller.angle_right) + ";" + String(nemas1.actualPosition) + ";" + String(nemas2.actualPosition) + ";" +
		String(nemas3.actualPosition) + ";" +
		String(lqrcontroller.auto_com_angle) + ";" + String(lqrcontroller.auto_dif_angle) + ";" +
		String(lqrcontroller.h_ref) + ";" + String(lqrcontroller.roll_reference) + ";" +
		String(lqrcontroller.yawrate_filt) + ";" + 
		String((int32_t)nemas1.current_Iq) + ";" + String((int32_t)nemas2.current_Iq);

	//Write the line to the file
	file1.writeToFile(data);
}

/**
 * Function that performs homing on all the nemas controllers
 */
void full_homing(){
	//change the foils controller state to HOMING
	lqrcontroller.state=HOMING;

	//Perform homing on nemas controller with id 1
	nemas1.stateMachine(SWITCH_ON_DISABLED);
	nemas1.setOperatingMode(CANOPEN_OPERATION_MODE_HOMING);
	nemas1.stateMachine(OPERATION_ENABLED);

	if (nemas1.startHoming(ZSEARCH_RPM,ZSEARCH_CURRENT,MAX_RPM,MAX_CURRENT)) {
		lqrcontroller.homing_done = true;
		nemas1.stateMachine(SWITCH_ON_DISABLED);
		nemas1.setOperatingMode(CANOPEN_OPERATION_MODE_PROFILE_POSITION);
		nemas1.stateMachine(OPERATION_ENABLED);
		nemas1.completed_setPosition(angle_to_steps(MIN_COM_FOILANGLE-DIF_FOILANGLE));
		nemas1.completed_setPosition(angle_to_steps(DEFAULT_ANGLE));
		delay(2000);
	} else {
		lqrcontroller.homing_done = false;
		lqrcontroller.state = ERROR;
	}
	nemas1.stateMachine(SWITCH_ON_DISABLED);

	//Perform homing on nemas controller with id 2
	nemas2.stateMachine(SWITCH_ON_DISABLED);
	nemas2.setOperatingMode(CANOPEN_OPERATION_MODE_HOMING);
	nemas2.stateMachine(OPERATION_ENABLED);

	if (nemas2.startHoming(ZSEARCH_RPM,ZSEARCH_CURRENT,MAX_RPM,MAX_CURRENT) && lqrcontroller.homing_done) {
		nemas2.stateMachine(SWITCH_ON_DISABLED);
		nemas2.setOperatingMode(CANOPEN_OPERATION_MODE_PROFILE_POSITION);
		nemas2.stateMachine(OPERATION_ENABLED);
		nemas2.completed_setPosition(angle_to_steps(MIN_COM_FOILANGLE-DIF_FOILANGLE));
		nemas2.completed_setPosition(angle_to_steps(DEFAULT_ANGLE));
		delay(2000);
		//lqrcontroller.state=OFF;
	} else {
		lqrcontroller.homing_done = false;
		lqrcontroller.state = ERROR;
	}
	nemas2.stateMachine(SWITCH_ON_DISABLED);

	//Perform homing on nema controller with id 3 (back)
	nemas3.stateMachine(SWITCH_ON_DISABLED);
	nemas3.setOperatingMode(CANOPEN_OPERATION_MODE_HOMING);
	nemas3.stateMachine(OPERATION_ENABLED);

	if (nemas3.startHoming(ZSEARCH_REARRPM,ZSEARCH_REARCURRENT,MAX_REARRPM,MAX_REARCURRENT) && lqrcontroller.homing_done) {
		nemas3.stateMachine(SWITCH_ON_DISABLED);
		nemas3.setOperatingMode(CANOPEN_OPERATION_MODE_PROFILE_POSITION);
		nemas3.stateMachine(OPERATION_ENABLED);
		nemas3.completed_setPosition(backfoil_angle_to_steps(DEFAULT_REARANGLE));
		delay(8000); // this waits fot the completed_setPosition() to finish
		lqrcontroller.state=OFF;
	} else {
		lqrcontroller.homing_done = false;
		lqrcontroller.state = ERROR;
	}
	nemas3.stateMachine(SWITCH_ON_DISABLED);

	canMessageHandler.screen.homing=false;

	//write on nemas controller variable that the homing was performed correctly
	if(lqrcontroller.homing_done){
		nemas1.WriteToN5(0x01,0x01);
	}
}