# Foils

This is the code for the Foils' controller.

Copyright (C) 2020  Técnico Solar Boat

This repository and its contents  is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.
The content of this repository is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/.
You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
or via our facebook page at https://fb.com/tecnico.solarboat


This Repository holds the code to put the boat ✈️

### This code was developed based on a Master Thesis developed by our collegue Basile Belime which is available [here](https://fenix.tecnico.ulisboa.pt/cursos/meec/dissertacao/1409728525633024).

### Controllers Ethernet IP Addresses

| ID |    IP    |  Subnet Mask  |
|:--:|:--------:|:-------------:|
| 1  | 10.1.1.1 | 255.255.255.0 |
| 2  | 10.1.1.2 | 255.255.255.0 |
| 3  | 10.1.1.3 | 255.255.255.0 |
