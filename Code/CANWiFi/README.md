# CANWiFi

This is the code for the CANWiFi.

Copyright (C) 2020  Técnico Solar Boat

This repository and its contents  is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.
The content of this repository is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/.
You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
or via our facebook page at https://fb.com/tecnico.solarboat

This repo contains the code used in the CAN sniffer to read the BUS and publish, every X time units, the data to the server using MQTT via Wifi. The [server](https://gitlab.com/tecnicosb/tsb-es/2020/mqtt-database-communication-rpi), which in our case runs in a RPi, will then subscribe to the topics published and save the data in our database.

![Example Diagram](images/CAN.png)

## Requirements

- Teensy 3.5
- ESP8266
- PubSubClient library (MQTT)
- WiFiEsp-2.2.2 (ESP communication)

## Connections

| Teensy| ESP |
| --- | --- |
| 3.3v  |  VCC |
| GND  | GND  |
| RX PIN 0 | TX  |
| TX PIN 1  | RX  |

## Configuration

### WiFi

In order for this code to work you need to set the credentials of the WiFi [here](lib/WIFI_MQTT_TSB/MQTT_TSB.h).

### MQTT Broker

Right now we are using a public broker, from the Hive organization, in case you want to setup your own broker, since while using the public broker everyone has access to the messages you can do so follwing this [tutorial](https://obrienlabs.net/how-to-setup-your-own-mqtt-broker/) and then change the credentials [here](lib/WIFI_MQTT_TSB/MQTT_TSB.h). 

### Adding new components

If you want to add more components to the system you need to follow this steps

1. Add the new component class to the CAN TSB library and implement the toJson function.
2. Setup a topic in [MQTT_TSB.h](lib/WIFI_MQTT_TSB/MQTT_TSB.h) following the form TSB/\<vessel>\/\<component>
3. Call the mqtt publish function in [main.cpp](src/main.cpp) like it is done for the other components.

### Changing the Publishing interval

The sniffer reads all the messages sent to the CAN BUS but the interval between messages to the server is controlled by the delay in the [main.cpp](src/main.cpp),
