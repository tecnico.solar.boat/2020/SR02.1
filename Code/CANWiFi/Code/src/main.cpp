/*
    Copyright (C) 2020  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include <MQTT_TSB.h>
#include "Datalogger_TSB.h"

bool ledState = false;
MQTT_TSB mqttHandler;

//IntervalTimer blink_timer;
IntervalTimer time_timer;
IntervalTimer log_timer_all;
IntervalTimer log_timer_BMS;
IntervalTimer send_timer;

// CANWiFi CAN Messages:
TSB_CAN canBus;
CAN_message_t time_msg;

// Log related stuff
TSB_SD SdCard(SPI_SCK, SD_CS_PIN); // For Teensy 3.2 SPI_SCK and SD_CS_PIN must be specified, SCK defaults to 13, SD_CD_PIN defaults to SS
TSB_Datalogger BMS_File("BMS",SdCard.sd_insert); // This is specifies the system name, it will be the prefix of the file name
TSB_Datalogger CAN_WIFI_FILE("CAN_WIFI",SdCard.sd_insert); // This is specifies the system name, it will be the prefix of the file name

const signed long DEFAULT_TIME_MAIN = 1357041600; // Jan 1 2013

void blink_led(){
	ledState = !ledState;
	digitalWrite(LED_BUILTIN, ledState);
}

void initCAN_Messages(){
	time_msg.ext = 0;
	time_msg.id = 0x0C;
	time_msg.len = 4;
}

void sendTime(){
	unsigned long time_now = now();
	int32_t ind = 0;
	if (time_now > DEFAULT_TIME_MAIN){
		buffer_append_uint32(time_msg.buf, time_now, &ind);
		Can0.write(time_msg);
	}

}

void logDataAll(){
	CAN_WIFI_FILE.writeToFile(
		String(canBus.bms.voltage) + ";" +
		String(canBus.bms.current) + ";" +
		String(canBus.bms.solar_current) + ";" +

		String(canBus.motor1.input_current) + ";" +
		String(canBus.motor1.rpm/5) + ";" +
		String(canBus.motor2.input_current) + ";" +
		String(canBus.motor2.rpm/5) + ";" +
		String(canBus.throttle.asked_current) + ";" +
		String(canBus.screen.current_threshold) + ";" +

		String(canBus.foils.lat, 10) + ";" +
		String(canBus.foils.lon, 10) + ";" +
		String(canBus.foils.pitch) + ";" +
		String(canBus.foils.roll) + ";" +
		String(canBus.foils.gyroX) + ";" +
		String(canBus.foils.gyroY) + ";" +
		String(canBus.foils.veloc) + ";" +
		String(canBus.foils.dist) + ";" +
		String(canBus.foils.state) + ";" +
		String(canBus.foils.mode) + ";" +
		String(canBus.foils.left_angle) + ";" +
		String(canBus.foils.right_angle) + ";" +
		String(canBus.screen.dist_setpoint)

	);
}

void logDataBMS(){
	BMS_File.writeToFile( String(canBus.bms.status) + ";" + String(canBus.bms.ltc_status) + ";" + String(canBus.bms.cells_balanc) + ";" +
	String(canBus.bms.warnings) + ";" + String(canBus.bms.soc) + ";" + String(canBus.bms.voltage) + ";" +  String(canBus.bms.current) + ";" +
	String(canBus.bms.solar_current) + ";" + String(canBus.bms.cellvoltages[0]) + ";" + String(canBus.bms.cellvoltages[1]) + ";" +
	String(canBus.bms.cellvoltages[2]) + ";" + String(canBus.bms.cellvoltages[3]) + ";" + String(canBus.bms.cellvoltages[4]) + ";" + String(canBus.bms.cellvoltages[5]) + ";" +
	String(canBus.bms.cellvoltages[6]) + ";" + String(canBus.bms.cellvoltages[7]) + ";" + String(canBus.bms.cellvoltages[8]) + ";" + String(canBus.bms.cellvoltages[9]) + ";" + 
	String(canBus.bms.cellvoltages[10]) + ";" + String(canBus.bms.cellvoltages[11]) + ";" + String(canBus.bms.solar_voltages[0]) + ";" + String(canBus.bms.solar_voltages[1]) + ";" +
	String(canBus.bms.solar_voltages[2]) + ";" + String(canBus.bms.solar_voltages[3]) + ";" + String(canBus.bms.solar_voltages[4]) + ";" + String(canBus.bms.temperatures[0]) + ";" + 
	String(canBus.bms.temperatures[1]) + ";" + String(canBus.bms.temperatures[2]) + ";" + String(canBus.bms.temperatures[3]) + ";" + String(canBus.bms.temperatures[4]) + ";" +
	String(canBus.bms.temperatures[5]) + ";" + String(canBus.bms.temperatures[6]) + ";" + String(canBus.bms.temperatures[7]) + ";" + String(canBus.bms.temperatures[8]) + ";" +
	String(canBus.bms.temperatures[9]) + ";" + String(canBus.bms.temperatures[10]) + ";" + String(canBus.bms.temperatures[11]) + ";" + String(canBus.bms.temperatures[12]) + ";" +
	String(canBus.bms.temperatures[13]) + ";" + String(canBus.bms.temperatures[14]) + ";" + String(canBus.bms.temperatures[15]) + ";" + String(canBus.bms.temperatures[16]) );

	blink_led();
}

void sendData(){
	// Ensure the connection to the MQTT server is alive (this will make the first
	// connection and automatically reconnect when disconnected).
	mqttHandler.MQTT_connect();

	// Publish the data obtained from the CAN
	mqttHandler.MQTT_publishMessage(BMS_TOPIC, canBus.bms.toJson());
	mqttHandler.MQTT_publishMessage(MOTOR_1_TOPIC, canBus.motor1.toJson());
	mqttHandler.MQTT_publishMessage(MOTOR_2_TOPIC, canBus.motor2.toJson());
	mqttHandler.MQTT_publishMessage(THROTTLE_TOPIC,canBus.throttle.toJson());
	mqttHandler.MQTT_publishMessage(FOILS_TOPIC, canBus.foils.toJson());
}


void setup() {
	//Setup serial for USB communication
	Serial.begin(115200);

	
	pinMode(RESET_ESP, OUTPUT);
	digitalWriteFast(RESET_ESP, LOW);


	// Setup the CAN
	initCAN_Messages();
	Can0.begin(1000000);
	Can0.attachObj(&canBus);
	canBus.attachGeneralHandler();

	// Setup Teensy State LED
	pinMode(LED_BUILTIN, OUTPUT);
	digitalWriteFast(LED_BUILTIN, HIGH);

	// Connect to the Internet
	mqttHandler.WiFi_connect();

	// Get time from the internet
	mqttHandler.WiFi_getTime();

	// // Setup the MQTT handler
	mqttHandler.MQTT_setup();

	time_timer.begin(sendTime, 1*1e6);
	// Send time during 10 seconds
	uint32_t now = millis();
	while (millis() < now + 10000){
		/* code */
	}
	time_timer.update(10*1e6);

	// Log inicializations
	BMS_File.initLog("BMS;Time;Date;Status;LTC_Status;Cells_Balance;Warnings;SOC;Voltage;Current;Solar_Current;\
Cell1;Cell2;Cell3;Cell4;Cell5;Cell6;Cell7;Cell8;Cell9;Cell10;Cell11;Cell12;Solar1;Solar2;Solar3;Solar4;Solar5;\
T1;T2;T3;T4;T5;T6;T7;T8;T9;T10;T11;T12;T_Internal;T_Ambient;T_HS1;T_HS2;T_LTC");


	CAN_WIFI_FILE.initLog("ID;Time;Date;Voltage;Current;Solar_Current;\
M1_current_in;M1_rpm;M2_current_in;M2_rpm;asked_current;dual_motor_threshold;\
Latitude;Longitude;Pitch;Roll;gyroX;gyroY;Velocity;Heave;State;Mode;Angle_Left;Angle_Right;H_ref");

	// Start logging
	log_timer_all.begin(logDataAll, 0.1*1e6);
	log_timer_BMS.begin(logDataBMS, 1*1e6);

	// // Setup WDT
    // noInterrupts();                                         // don't allow interrupts while setting up WDOG
    // WDOG_UNLOCK = WDOG_UNLOCK_SEQ1;                         // unlock access to WDOG registers
    // WDOG_UNLOCK = WDOG_UNLOCK_SEQ2;
    // delayMicroseconds(1);                                   // Need to wait a bit..

    // // for this demo, we will use 10 seconds WDT timeout (e.g. you must reset it in < 1 sec or a boot occurs)
    // WDOG_TOVALH = 0x044a;
    // WDOG_TOVALL = 0xa200;

    // // This sets prescale clock so that the watchdog timer ticks at 7.2MHz
    // WDOG_PRESC  = 0x400;

    // // Set options to enable WDT. You must always do this as a SINGLE write to WDOG_CTRLH
    // WDOG_STCTRLH |= WDOG_STCTRLH_ALLOWUPDATE |
    //     WDOG_STCTRLH_WDOGEN | WDOG_STCTRLH_WAITEN |
    //     WDOG_STCTRLH_STOPEN | WDOG_STCTRLH_CLKSRC;
    // interrupts(); 

	send_timer.begin(sendData, 5e5);
	// ALL inicialization OK star blinking
	pinMode(LED_BUILTIN, OUTPUT);
	//blink_timer.priority(255);
	//blink_timer.begin(blink_led, 1000000);
}



void loop() {
	// Reset whatch dog timer
    noInterrupts();
    WDOG_REFRESH = 0xA602;
    WDOG_REFRESH = 0xB480;
    interrupts();

	if ( canBus.screen.newFile == true){
		noInterrupts();
		CAN_WIFI_FILE.createNewFile(); // Creates a new log file
		BMS_File.createNewFile();
		canBus.screen.newFile = false;
		Serial.println("new file created");
		interrupts();
	}

	
	if( now() < DEFAULT_TIME_MAIN) { // check the value is a valid time (greater than Jan 1 2013)
       // Get time from the internet
		mqttHandler.WiFi_getTime();
		if ( now() > DEFAULT_TIME_MAIN ){
			noInterrupts();
			CAN_WIFI_FILE.createNewFile(); // Creates a new log file
			BMS_File.createNewFile();
			Serial.println("new file created");
			interrupts();
		}	
    }

	canBus.check_valid_data();

}
