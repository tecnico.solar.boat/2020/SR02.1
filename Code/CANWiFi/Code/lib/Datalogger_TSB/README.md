# TSB Datalogger Libray Code

This is the code for the TSB Datalogger Libray used to store telemetry on micro-SD cards.

Copyright (C) 2020  Técnico Solar Boat

This repository and its contents  is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.
The content of this repository is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/.
You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
or via our facebook page at https://fb.com/tecnico.solarboat


This is the TSB datalogger library. This library supports the following:

*  Inicialize the SD Card `TSB_SD`
*  Inicialize the files to write to `TSB_Datalogger`
*  Do the actual logging `TSB_Datalogger`
*  Method to create a new file `TSB_Datalogger`

`TSB_SD` has no public methods besides its own contructor. It has a public bool varibale `sd_insert` that need to be passed to the `TSB_Datalogger` constructor. This class serves only as an inicializer for the SD Card system.

`TSB_Datalogger` is the class that deals with the actual file writting stuff. 

**See the header file for more docummentation on how this library works.**

# Teensy 3.5/3.6 example:

```c++

#include <Arduino.h>
#include "Datalogger_TSB.h"
#include "CAN_TSB.h"

TSB_CAN canMessageHandler;

TSB_SD SdCard; // For Teensy 3.5/3.6 there is no need to specify any pins
TSB_Datalogger file1("FILE1",SdCard.sd_insert); // This is specifies the system name, it will be the prefix of the file name
TSB_Datalogger file2("FILE2",SdCard.sd_insert); // This is specifies the system name, it will be the prefix of the file name
bool firsTime = true;

void setup() {
  Serial.begin(115200);

  file1.initLog("File1;Time;Date;test_data;"); // This string specifies the firs line of the CSV file and thus it's format
  file2.initLog("File2;Time;Date;test_data;"); // This string specifies the firs line of the CSV file and thus it's format
}

void loop() {
  file1.writeToFile("1;cenas;cenas;cenas");
  file2.writeToFile("2;cenas;cenas;cenas");
  if ( canMessageHandler.screen.newFile == true){
    noInterrups();
    file1.createNewFile(); // Creates a new log file
    file2.createNewFile(); // Creates a new log file
    canMessageHandler.screen.newFile = false;
    Serial.println("new file created");
    interrupts();
  }
  Serial.println("escrevi");
  delay(500);
}

```


# Teensy 3.2 example:

```c++

#include <Arduino.h>
#include "Datalogger_TSB.h"

#define SD_CS_PIN 9
#define SPI_SCK 14

TSB_SD SdCard(SPI_SCK, SD_CS_PIN); // For Teensy 3.2 SPI_SCK and SD_CS_PIN must be specified, SCK defaults to 13, SD_CD_PIN defaults to SS
TSB_Datalogger file1("FILE1",SdCard.sd_insert); // This is specifies the system name, it will be the prefix of the file name
TSB_Datalogger file2("FILE2",SdCard.sd_insert); // This is specifies the system name, it will be the prefix of the file name
bool firsTime = true;

void setup() {
  Serial.begin(115200);

  file1.initLog("File1;Time;Date;test_data;"); // This string specifies the firs line of the CSV file and thus it's format
  file2.initLog("File2;Time;Date;test_data;"); // This string specifies the firs line of the CSV file and thus it's format
}

void loop() {
  file1.writeToFile("1;cenas;cenas;cenas");
  file2.writeToFile("2;cenas;cenas;cenas");
  if ( canMessageHandler.screen.newFile == true){
    noInterrups();
    file1.createNewFile(); // Creates a new log file
    file2.createNewFile(); // Creates a new log file
    canMessageHandler.screen.newFile = false;
    Serial.println("new file created");
    interrupts();
  }
  Serial.println("escrevi");
  delay(500);
}

```