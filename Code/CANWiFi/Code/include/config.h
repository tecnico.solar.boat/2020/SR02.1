/*
    Copyright (C) 2020  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

/************************* WiFi Access Point *********************************/

#define WLAN_SSID       "SSID"
#define WLAN_PASS       "WLAN_PASS"

/************************* Mqtt Broker Setup *********************************/

#define MQTT_BROKER_PORT  1883                   // use 8883 for SSL
#define MQTT_USER    "MQTT_USER"
#define MQTT_PASS    "MQTT_PASS"

#define BMS_TOPIC "TSB/SR02/bms\0"
#define MOTOR_1_TOPIC "TSB/SR02/motor1\0"
#define MOTOR_2_TOPIC "TSB/SR02/motor2\0"
#define THROTTLE_TOPIC "TSB/SR02/throttle\0"
#define FOILS_TOPIC "TSB/SR02/foils\0"
 
/**************************** ESP8266 Setup **********************************/
#define ESP_SERIAL Serial1
#define ESP_BAUDRATE  2000000

/**************************** Serial Setup **********************************/
#define USB_BAUDRATE 115200

/**************************** NTP Protocol stuff **********************************/
#define NTP_PACKET_SIZE 48          // NTP timestamp is in the first 48 bytes of the message
#define UDP_TIMEOUT 2000            // timeout in miliseconds to wait for an UDP packet to arrive
#define	localPort 2390              // local port to listen for UDP packets

/**************************** Teensy pins **********************************/
#define RESET_ESP 2
#define SD_CS_PIN 9
#define SPI_SCK 14


