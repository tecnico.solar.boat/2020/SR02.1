# TSB BMS

This is the code for TSB BMS.

Copyright (C) 2020  Técnico Solar Boat

This repository and its contents  is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.
The content of this repository is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/.
You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
or via our facebook page at https://fb.com/tecnico.solarboat

⚠️ This BMS code is for the BMS V1 PCB, the one that connects to the battery with Pogo Pins and has 90º degress corners ⚠️

⚠️ This Code is compatible with [this BMS GUI](https://gitlab.com/tecnicosb/tsb-es/2019/gui) ⚠️

‼️ **This BMS has major power consumption problems (drains a full 1.5 kWh battery in 60 days) it shoul NOT be connected to an unateended battery** ‼️

Be sure to check this project's [Wiki Page](https://gitlab.com/tecnico.solar.boat/2020/electrical-systems/-/wikis/home)!
