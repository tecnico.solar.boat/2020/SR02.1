/*
    Copyright (C) 2020  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#ifndef BMS_CONFIG_H
#define BMS_CONFIG_H

#include <Arduino.h>  // typedefs use types defined in this header file.
#include "LTC681x.h"


typedef struct{
    uint8_t ambient=0;
    uint8_t heatshink_1=0;
    uint8_t heatshink_2=0;
    uint8_t internal=0;
    uint8_t LTC=0;
    uint8_t cells[12]={0};
    uint8_t maxTemp=0;
} Temperatures;

typedef struct{
    int16_t batLow;
    int16_t batHigh;
    int16_t bat;
    uint16_t solar;    
} Currents;

/*****************************************
 ************* Teensy Pins  **************
*****************************************/
#define EXTERNAL_LED 0
#define FANS 5
#define BUZZER 6
#define CS_SD 9
#define SOLAR_V A1
#define MS0 16
#define MS1 17
#define MS2 18
#define CHARGER_RELAY 19
#define MAIN_RELAY 20
#define SOLAR_RELAY 21
#define SENSE_MOTOR 22
#define SENSE_CHARGER 23
#define HEATSHINK_1 A10
#define HEATSHINK_2 A11
#define AMBIENT A14

/**********************************************
 ************* Hardcoded Values **************
***********************************************/
#define CLOSE_SOLAR_CURRENT_THRESHOLD 50 // Means 5 A
#define FANS_SPIN_CURRENT_THRESHOLD 20 // Means 2 A
#define FANS_SPIN_TEMP_THERSHOLD 30 // Means 30 ºC

/**********************************************
 ************* EEPROM Addresses ***************
***********************************************/
#define BALANCING_ALLOWED_ADDR 0
#define BALANCING_THRES_ADDR 1
#define IMBALANCE_THRES_ADDR 3
#define OV_THRES_ADDR 5
#define UV_THRES_ADDR 7
#define FCV_THRES_ADDR 9
#define FDV_THRES_ADDR 11
#define WARN_TEMP_ADDR 13
#define DOC_ADDR 14
#define COC_ADDR 16
#define REFRESH_RATE_ADDR 18
#define EEPROM_CRC_ADDR 20

/*****************************************
 ********** Global  Variables  ***********
*****************************************/
// SOC
extern bool isFull;
extern bool chargingAllowed;

// Balancing
extern bool timeToBalance;
extern bool balancingAllowed;
extern bool isBalancing;
extern uint16_t minimumCellVoltage;
extern uint16_t balancingThreshold;
extern uint16_t imbalance_thershold;


// Voltage sensors
extern bool hasVoltageCharger;
extern bool hasVoltageMotor;

// Discharge
extern bool motorRelayON;

// Temperatures
extern Temperatures temps;

// Solar Panels voltage:
extern uint16_t solarVoltage[5];

/*****************************************
 ********** Refresh Rate BMS *************
*****************************************/
extern uint16_t refresh_rate; // Unit is ms

/*****************************************
 ********** Balancing Defines ************
*****************************************/
#define MAX_CELLS_BALANCING 6

/*****************************************
 ************** OV/UV Defines ************
*****************************************/
#define NONE 0
#define OVER_VOLTAGE 1
#define UNDER_VOLTAGE 2

/*****************************************
 ********** LTC6811-2 Chip Select ********
*****************************************/
#define LTC_CS 10


/*****************************************
 *********** LTC6811-2 Parameters ********
*****************************************/

#define ENABLED 1
#define DISABLED 0
#define DATALOG_ENABLED 1
#define DATALOG_DISABLED 0

const uint8_t TOTAL_IC = 1; // number of ICs in the daisy chain

const uint16_t MEASUREMENT_LOOP_TIME = 500; //milliseconds(mS)

/*****************************************
 *********** ADC configurations **********
*****************************************/
// ADC_OPT and ADC_CONVERSION_MODE sets the ADC mode
const uint8_t ADC_OPT = ADC_OPT_0; // Selects Modes 27kHz, 7kHz, 422Hz or 26Hz with MD[1:0] Bits in ADC Conversion Commands

// Sets to 7 kHz (normal mode) since ADC_OPT = 0
const uint8_t ADC_CONVERSION_MODE = MD_7KHZ_3KHZ;

// Do not permit discharge while measuring
const uint8_t ADC_DCP = DCP_DISABLED; 

// Measure all cells 
const uint8_t CELL_CH_TO_CONVERT = CELL_CH_ALL; 

// Measure all  GPIOS and 2nd reference
const uint8_t AUX_CH_TO_CONVERT = AUX_CH_ALL; 

// Measure SC (Sum of all cells), Internal temperature, Analog supply voltage
// and Digital supply voltage
const uint8_t STAT_CH_TO_CONVERT = STAT_CH_ALL;

/*******************************************
 *********** Under / Over voltage **********
 ************** configuration **************
********************************************/
extern uint16_t OV_THRESHOLD; // Over voltage threshold ADC Code. LSB = 0.0001
extern uint16_t UV_THRESHOLD; // Under voltage threshold ADC Code. LSB = 0.0001
extern uint16_t FCV;  //Fully Charged Voltage
extern uint16_t FDV;  //Fully Discharged Voltage
extern const uint16_t FULL_BATTERY; // Full charged battery voltage SOC = REGISTER * 0.0001 * 20

// Warning temperature threshold
extern uint8_t warning_temp;
extern uint8_t heatshink_warn_temp;

// Control if warning happened
extern bool anyWarning;

// Maximum allowed current
extern uint16_t DOC;
extern uint16_t COC;

// Relays
extern bool solarRelayON;

//Loop Measurement Setup These Variables are ENABLED or DISABLED Remember ALL CAPS
const uint8_t WRITE_CONFIG = DISABLED; // This is ENABLED or DISABLED
const uint8_t READ_CONFIG = DISABLED; // This is ENABLED or DISABLED
const uint8_t MEASURE_CELL = ENABLED; // This is ENABLED or DISABLED
const uint8_t MEASURE_AUX = DISABLED; // This is ENABLED or DISABLED
const uint8_t MEASURE_STAT = DISABLED; //This is ENABLED or DISABLED
const uint8_t PRINT_PEC = DISABLED; //This is ENABLED or DISABLED

/*****************************************
 ***************** Macros ****************
*****************************************/
//! Set "pin" low
//! @param pin pin to be driven LOW
#define output_low(pin)   digitalWriteFast(pin, LOW)
//! Set "pin" high
//! @param pin pin to be driven HIGH
#define output_high(pin)  digitalWriteFast(pin, HIGH)
//! Return the state of pin "pin"
//! @param pin pin to be read (HIGH or LOW).
//! @return the state of pin "pin"
#define input(pin)        digitalReadFast(pin)
//! Set "pin" as output
//! @param pin pin to be set as output
#define setOutput(pin)    pinMode(pin, OUTPUT)
//! Set "pin" as input
//! @param pin pin to be set as input
#define setInput(pin)    pinMode(pin, INPUT)


#endif  // BMS_CONFIG_H