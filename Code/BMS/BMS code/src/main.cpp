/*
    Copyright (C) 2020  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include <Arduino.h>
#include <SPI.h>
#include <stdint.h>
#include "../include/BMS_config.h"
#include "LT_SPI.h"
#include "LTC681x.h"
#include "LTC6811.h"
#include "BMS.h"
#include "tsb_serial.h"
#include "CAN_TSB.h"
#include "BMS_EEPROM.h"

/*****************************************
 ********** Global Variables*  ***********
*****************************************/
// SOC
bool isFull = true;
bool chargingAllowed = true;
bool isCharging = false;

// Balancing
bool timeToBalance = false;
bool isBalancing = true;
uint16_t minimumCellVoltage = OV_THRESHOLD; // Just for initializing BMS_needs_balance() changes this value

// Balancing parameters configurable over the GUI 
bool balancingAllowed = false;
uint16_t balancingThreshold = 35000; // Voltage above which balance is allowed
uint16_t imbalance_thershold = 100; // 500 means 50 mV

// MUX Bits
uint8_t currentTemp = 0;
bool S0[8] = {0,1,0,1,0,1,0,1};
bool S1[8] = {0,0,1,1,0,0,1,1};
bool S2[8] = {0,0,0,0,1,1,1,1};

/*******************************************
 *********** Under / Over voltage **********
 ************** configuration **************
********************************************/
uint16_t OV_THRESHOLD = 42000; // Over voltage threshold ADC Code. LSB = 0.0001
uint16_t UV_THRESHOLD = 34000; // Under voltage threshold ADC Code. LSB = 0.0001
uint16_t FCV = 42300;  //Fully Charged Voltage
uint16_t FDV = 30000;  //Fully Discharged Voltage
const uint16_t FULL_BATTERY = OV_THRESHOLD*12/20; // Full charged battery voltage SOC = REGISTER * 0.0001 * 20

// Warning temperature threshold
uint8_t warning_temp = 55; // configurable over the GUI
uint8_t heatshink_warn_temp = 70;

// Control if warning happened
bool anyWarning = false;

// Maximum allowed current configurable over the GUI
uint16_t DOC = 3000; // Means 300 A
uint16_t COC = 336; // Means 33,6 A

/*****************************************
 ********** Refresh Rate BMS *************
*****************************************/
uint16_t refresh_rate = 1000; // Unit is ms

// Voltage sensors
bool hasVoltageCharger = false;
bool hasVoltageMotor = false;

// Relays
bool motorRelayON = false;
bool chargerRelayON = false;
bool solarRelayON = false;

// Temperatures
Temperatures temps;

// Currents
Currents currents;

// LTC Status Variable
uint16_t LTC_Status = 0;

// BMS Status
uint8_t bms_status = 0;

// BMS Warning Variable
uint8_t warnings = 0;

// Solar Panels voltage:
uint16_t solarVoltage[5];

// BMS Timer
IntervalTimer BMS_Timer;
volatile bool shouldRun = false; // Variable that tells loop to run BMS_callback()

// Pre-charge Timer
IntervalTimer preCharge_Timer;

// Charger Timer
IntervalTimer charger_timer;


// BMS Serial Messages:
TSB_SERIAL serial_interface;
tsb_serial_msg_t status;
tsb_serial_msg_t SOC_Voltage_Currents;
tsb_serial_msg_t cellvoltages0;
tsb_serial_msg_t cellvoltages1;
tsb_serial_msg_t cellvoltages2;
tsb_serial_msg_t SolarPanelVoltages0;
tsb_serial_msg_t SolarPanelVoltages1;
tsb_serial_msg_t temperatures0;
tsb_serial_msg_t temperatures1;
tsb_serial_msg_t temperatures2;
tsb_serial_msg_t cellParameters0;
tsb_serial_msg_t cellParameters1;
tsb_serial_msg_t securityParameters;

// BMS CAN Messages:
TSB_CAN m_can;
CAN_message_t status_balanc_warnings;
CAN_message_t can_cell0;
CAN_message_t can_cell1;
CAN_message_t can_cell2;
CAN_message_t can_solarVoltage0;
CAN_message_t can_solarVoltage1;
CAN_message_t can_temp0;
CAN_message_t can_temp1;
CAN_message_t can_temp2;
CAN_message_t can_SOC_Volt_Cur;

 

/******************************************************
 *** Global Battery Variables received from 681x commands
 These variables store the results from the LTC6811
 register reads and the array lengths must be based
 on the number of ICs on the stack
 ******************************************************/

cell_asic bms_ic[TOTAL_IC];

void chargerISR();
void charger_Timer_callback();
void motorISR();
void PreCharge_Timer_callback();
void BMS_Timer_callback();
void BMS_callback();
void initSerialMsg();
void initCANMsg();
void updateSerialMsg();
void updateMessages();
void send_can_messages();
void send_serial_messages();

/*!**********************************************************************
 \brief  Initializes hardware and variables
 ***********************************************************************/
void setup()
{
	Serial.begin(9600);
	initSerialMsg();
	readFromEEPROM();
	// setDefaults();

	BMS_init(bms_ic);
	BMS_selfCheck(bms_ic, &LTC_Status);
	if ( LTC_Status != 0 ) {
		// Mandar erro Serial
		// Buzzer e o crl
	}
	
	
	initCANMsg();
	Can0.begin(1000000);
	//Handler for CAN communication
	Can0.attachObj(&m_can);
	m_can.attachGeneralHandler();

	BMS_Timer.begin(BMS_Timer_callback, refresh_rate * 1000);
	attachInterrupt(digitalPinToInterrupt(SENSE_MOTOR), motorISR, CHANGE);
	attachInterrupt(digitalPinToInterrupt(SENSE_CHARGER), chargerISR, RISING);
}

/*!*********************************************************************
	\brief main loop
***********************************************************************/
uint32_t current_time = millis();
uint32_t last_time = 0;
void loop(){
	current_time = millis();
	if (current_time - last_time > 1000 ) { // Send serial messages, refresh rate is fixed at 1s otherwise GUI will crash
		send_serial_messages();
		last_time = current_time;
	}

	if(incomimingMessage.valid_msg == true){ // incomingMessage is a global tsb_serial_msg_t variable
		noInterrupts();
		switch (incomimingMessage.data_id)
        {
            case 0x0C: // Cell parameters0
            OV_THRESHOLD = (incomimingMessage.data[0] << 8) | incomimingMessage.data[1];
			UV_THRESHOLD = (incomimingMessage.data[2] << 8) | incomimingMessage.data[3];
			balancingThreshold = (incomimingMessage.data[4] << 8) | incomimingMessage.data[5];
			imbalance_thershold = (incomimingMessage.data[6] << 8) | incomimingMessage.data[7];
			BMS_set_OV_UV( bms_ic );
            break;
            
            case 0x0D: // Cell parameters1
            FCV = (incomimingMessage.data[0] << 8) | incomimingMessage.data[1];
			FDV = (incomimingMessage.data[2] << 8) | incomimingMessage.data[3];
            break;

			case 0x0E: // Security parameters
			DOC = (incomimingMessage.data[0] << 8) | incomimingMessage.data[1];
			COC = (incomimingMessage.data[2] << 8) | incomimingMessage.data[3];
			warning_temp = incomimingMessage.data[4];
			balancingAllowed = incomimingMessage.data[5];
			refresh_rate = (incomimingMessage.data[6] << 8) | incomimingMessage.data[7];
			BMS_Timer.update(refresh_rate * 1000);
			break;
        }
		saveToEEPROM();
        incomimingMessage.valid_msg = false;
		interrupts();
    }

	if ( m_can.screen.solarRelay == true ){ // Code to toogle solar relays
		noInterrupts();
		if( solarRelayON ){ // Turn off solar
			output_low(SOLAR_RELAY);
			solarRelayON = false;
			// Charging wont't be allowed
			chargingAllowed = false;
		}
		else{ // Turn on solar
			if( !chargerRelayON ){ // Makes sure the boat is not charging with shore power
				output_high(SOLAR_RELAY);
				solarRelayON = true;
				// Charging wont't be allowed
				chargingAllowed = true;
			}
		}
		m_can.screen.solarRelay = false;
		interrupts();
	}
	
	if (temps.maxTemp > 45 && currents.bat > 10 ){ // No charging if above safe temperature
		noInterrupts();
		// Turn off charging
		output_low(SOLAR_RELAY);
		solarRelayON = false;
		// Charging wont't be allowed
		chargingAllowed = false;
		interrupts();
	}

	if (shouldRun){ // Call BMS_callback() if it is time for that and send CAN Messages
		shouldRun = false;
		BMS_callback();
		updateMessages();
		send_can_messages();
	}
}

void chargerISR(){
	cli();
	if (digitalReadFast(SENSE_CHARGER) && chargingAllowed && !isBalancing ) // Charger was connected
	{
		output_low(SOLAR_RELAY);
		output_high(FANS);
		solarRelayON = false;
		output_high(CHARGER_RELAY);
		charger_timer.begin(charger_Timer_callback, 3000000);
		chargerRelayON = true;
		isCharging = true;
	}
	sei();
}

void charger_Timer_callback(){
	cli();
	if (chargerRelayON){
		if (currents.bat < 0 )
		{
			output_low(CHARGER_RELAY);
			chargerRelayON = false;
			output_low(FANS);
			charger_timer.end();
		}
	}
	sei();
}

void motorISR(){
	cli();
	if (digitalReadFast(SENSE_MOTOR)) // Kill Switch is closed pre-charge relay closed
	{
		//preCharge_Timer.begin(PreCharge_Timer_callback, 2000000);
		digitalWriteFast(MAIN_RELAY, HIGH);
		motorRelayON = true;
	}
	else{ // Kill Switch is open pre-charge relay open
		digitalWriteFast(MAIN_RELAY, LOW);
		motorRelayON = false;
		preCharge_Timer.end();
	}
	sei();

}

void PreCharge_Timer_callback(){
	cli();
	digitalWriteFast(MAIN_RELAY, HIGH);
	motorRelayON = true;
	sei();
}

void BMS_Timer_callback(){
	shouldRun = true;
}

void BMS_callback(){
	// digitalWriteFast(EXTERNAL_LED, !digitalReadFast(EXTERNAL_LED));
	int8_t error = 0;
	uint8_t OV_UV = NONE;

	// Read Cells and current sensors
	error = BMS_readCells_GPIO_1_2(bms_ic, &LTC_Status, &currents);
	while (error != 0) {
		error = BMS_readCells_GPIO_1_2(bms_ic, &LTC_Status, &currents);
		//CRC error detected handle this shit
	}
	LTC6811_rdcfg(TOTAL_IC,bms_ic);
	
	// Read SOC, LTC temperature, VregA, VregD and GPIO 3 to 5
	digitalWrite(MS0, S0[currentTemp]);
	digitalWrite(MS1, S1[currentTemp]);
	digitalWrite(MS2, S2[currentTemp]);
	BMS_read_aux( bms_ic, &LTC_Status, &currents);
	
	// Read temperatures e solar voltage
	BMS_read_temps( &temps, bms_ic, currentTemp);
	BMS_read_solar( solarVoltage, bms_ic, currentTemp);
	currentTemp++;
	if (currentTemp > 7) currentTemp = 0;

	

	// Checks if values are ok
	if ( (OV_UV = BMS_OV_UV( bms_ic, &warnings, &LTC_Status)) != NONE) { // OV or UV conditions
		anyWarning = true;
		// Emergency shit
		if (OV_UV == OVER_VOLTAGE ) {
			// Turn off charging
			output_low(CHARGER_RELAY);
			output_low(SOLAR_RELAY);
			chargerRelayON = false;
			solarRelayON = false;
			// Charging wont't be allowed
			chargingAllowed = false;
			// Turn on discharge on OV cells
			// BMS_discharge_OV( bms_ic );
			// BMS_any_balancing( bms_ic, &status.data[3], &status.data[4] );
			warnings ^= (-1 ^ warnings) & (1UL << 2);	
		}
		else{ // Undervoltage condition
			output_low(MAIN_RELAY);
			motorRelayON = false;
			chargingAllowed = true;
			output_high(SOLAR_RELAY);
			solarRelayON = true;
			warnings ^= (-1 ^ warnings) & (1UL << 3);
		}
		tone(BUZZER,1300);
		// reset WDT
		return;
	}
	else if(OV_UV == NONE && !isBalancing && !isCharging){
		anyWarning = false;
		BMS_clear_discharge(bms_ic);
		chargingAllowed = true;
		// output_high(SOLAR_RELAY);   ESTE TAVA COMENTADO
		// solarRelayON = true;	
	}
	if( BMS_check_temperatures() ){ // Check Temperatures
		anyWarning = true;
		warnings ^= (-1 ^ warnings) & (1UL << 1);
		BMS_clear_discharge(bms_ic);
		tone(BUZZER,1300);
		// reset WDT
		return;
	}
	if( currents.bat < (- DOC) ){ // Check discharge current threshold
		anyWarning = true;
		output_low(MAIN_RELAY);
		motorRelayON = false;
		warnings ^= (-1 ^ warnings) & (1UL << 0);
		tone(BUZZER,1300);
		// reset WDT
		return;
	}
	// Prevents from starting the competitions with Solar relays open
	else if( currents.bat < -(CLOSE_SOLAR_CURRENT_THRESHOLD)){
		chargingAllowed = true;
		output_high(SOLAR_RELAY);
		solarRelayON = true;
	}
	if (currents.bat > COC){ // Check charge current threshold
		anyWarning = true;
		output_low(CHARGER_RELAY);
		output_low(SOLAR_RELAY);
		chargerRelayON = false;
		solarRelayON = false;
		warnings ^= (-1 ^ warnings) & (1UL << 0);
		tone(BUZZER,1300);
		// reset WDT
		return;
	}
	if (abs( currents.bat )  > FANS_SPIN_CURRENT_THRESHOLD || temps.maxTemp > FANS_SPIN_TEMP_THERSHOLD){ // Control fans state based on Battery current
		output_high(FANS);
	}
	else{
		output_low(FANS);
	}
	
	if( !anyWarning ){
		// clear Warnings
		noTone(BUZZER);
		warnings ^= (-0 ^ warnings) & (1UL << 0);
		warnings ^= (-0 ^ warnings) & (1UL << 1);
		warnings ^= (-0 ^ warnings) & (1UL << 2);
		warnings ^= (-0 ^ warnings) & (1UL << 3);

		// Update timeToBalance flag
		BMS_needs_balance( bms_ic, &minimumCellVoltage );
		if(minimumCellVoltage > balancingThreshold && minimumCellVoltage != OV_THRESHOLD) 
			timeToBalance = true;
		else if( isBalancing && minimumCellVoltage < balancingThreshold)
			timeToBalance = false;
		// else if( !isBalancing )
		// 	timeToBalance = false;

		// Is full?
		if ( bms_ic[0].stat.stat_codes[0] >= FULL_BATTERY || BMS_is_cell_charged( bms_ic ) ) {
			isFull = true;
			chargingAllowed = false;
			output_low(CHARGER_RELAY);
			output_low(SOLAR_RELAY);
			chargerRelayON = false;
			solarRelayON = false;
			return;
		}
		else{
			isFull = false;
			chargingAllowed = true;
			// Is balancing?
			if ( isBalancing == true ) { // If balancing then check balancing status
				if (!balancingAllowed){
					BMS_clear_discharge(bms_ic);
					isBalancing = false;
					BMS_any_balancing( bms_ic, &status.data[3], &status.data[4] );
					return;
				}
				
				BMS_check_balance( bms_ic, minimumCellVoltage );
				if ( BMS_any_balancing( bms_ic, &status.data[3], &status.data[4] ) == false) {
					isBalancing = false;
					output_low(FANS);
				}
				return;
			}
			else{ // If not balancing then check if balancing is needed
				if (timeToBalance && balancingAllowed) {
					if ( BMS_needs_balance( bms_ic, &minimumCellVoltage ) ) {
						output_low(CHARGER_RELAY);
						output_low(SOLAR_RELAY);
						output_high(FANS);
						chargerRelayON = false;
						solarRelayON = false;
						isBalancing = true;
						BMS_balance( bms_ic, minimumCellVoltage );
						return;
					}
					return;
				}
				else{ // It's not time to balance yet or balancing is disabled
					// BMS_clear_discharge(bms_ic);
					// isBalancing = false;
					if (chargingAllowed == false) { // Charging is not allowed 
						output_low(CHARGER_RELAY);
						output_low(SOLAR_RELAY);
						chargerRelayON = false;
						solarRelayON = false;
						return;
					}
					else{ // Was not charging but charging is allowed
						if ( isCharging == false ) {
							output_high(SOLAR_RELAY);
							solarRelayON = true;
							isCharging = true;
							return;
						}
						return;
					}
					return;	
				}
			}
		}	
	}
	anyWarning = false;
}


void initSerialMsg(){
	status.addr = 0x01;
	SOC_Voltage_Currents.addr = 0x01;
	cellvoltages0.addr = 0x01;
	cellvoltages1.addr = 0x01;
	cellvoltages2.addr = 0x01;
	SolarPanelVoltages0.addr = 0x01;
	SolarPanelVoltages1.addr = 0x01;
	temperatures0.addr = 0x01;
	temperatures1.addr = 0x01;
	temperatures2.addr = 0x01;
	cellParameters0.addr = 0x01;
	cellParameters1.addr = 0x01;
	securityParameters.addr = 0x01;

	status.data_id = 0x01;
	SOC_Voltage_Currents.data_id = 0x02;
	cellvoltages0.data_id = 0x03;
	cellvoltages1.data_id = 0x04;
	cellvoltages2.data_id = 0x05;
	SolarPanelVoltages0.data_id = 0x06;
	SolarPanelVoltages1.data_id = 0x07;
	temperatures0.data_id = 0x08;
	temperatures1.data_id = 0x09;
	temperatures2.data_id = 0x0A;
	cellParameters0.data_id = 0x0C;
	cellParameters1.data_id = 0x0D;
	securityParameters.data_id = 0x0E;


	status.data_size = 6;
	SOC_Voltage_Currents.data_size = 8;
	cellvoltages0.data_size = 8;
	cellvoltages1.data_size = 8;
	cellvoltages2.data_size = 8;
	SolarPanelVoltages0.data_size = 8;
	SolarPanelVoltages1.data_size = 2;
	temperatures0.data_size = 8;
	temperatures1.data_size = 8;
	temperatures2.data_size = 4;
	cellParameters0.data_size = 8;
	cellParameters1.data_size = 4;;
	securityParameters.data_size = 8;
}

void initCANMsg(){
	status_balanc_warnings.ext = 0;
	status_balanc_warnings.id = 0x609;
	status_balanc_warnings.len = 8;

	can_SOC_Volt_Cur.ext = 0;
	can_SOC_Volt_Cur.id = 0x611;
	can_SOC_Volt_Cur.len = 8;

	can_cell0.ext = 0;	
	can_cell0.id = 0x619;
	can_cell0.len = 8;

	can_cell1.ext = 0;	
	can_cell1.id = 0x621;
	can_cell1.len = 8;

	can_cell2.ext = 0;	
	can_cell2.id = 0x629;
	can_cell2.len = 8;

	can_solarVoltage0.ext = 0;
	can_solarVoltage0.id = 0x631;
	can_solarVoltage0.len = 8;

	can_solarVoltage1.ext = 0;
	can_solarVoltage1.id = 0x639;
	can_solarVoltage1.len = 2;

	can_temp0.ext = 0;	
	can_temp0.id = 0x641;
	can_temp0.len = 8;

	can_temp1.ext = 0;	
	can_temp1.id = 0x649;
	can_temp1.len = 5;

	can_temp2.ext = 0;	
	can_temp2.id = 0x651;
	can_temp2.len = 4;
}

void updateMessages(){
	bms_status ^= (-isCharging ^ bms_status) & (1UL << 0);
	bms_status ^= (-(currents.bat < 0) ^ bms_status) & (1UL << 1);
	// REGENERATING ?!?!?!
	bms_status ^= (-isBalancing ^ bms_status) & (1UL << 3);
	bms_status ^= (-motorRelayON ^ bms_status) & (1UL << 4);
	bms_status ^= (-solarRelayON ^ bms_status) & (1UL << 5);
	bms_status ^= (-chargerRelayON ^ bms_status) & (1UL << 6);
	status.data[0] = bms_status;
	status.data[1] = highByte(LTC_Status);
	status.data[2] = lowByte(LTC_Status);
	status.data[5] = warnings;

	status_balanc_warnings.buf[0] = status.data[0];
	status_balanc_warnings.buf[1] = status.data[1];
	status_balanc_warnings.buf[2] = status.data[2];
	status_balanc_warnings.buf[3] = status.data[3];
	status_balanc_warnings.buf[4] = status.data[4];
	status_balanc_warnings.buf[5] = status.data[5];
	
	SOC_Voltage_Currents.data[2] = highByte(bms_ic[0].stat.stat_codes[0]);
	SOC_Voltage_Currents.data[3] = lowByte(bms_ic[0].stat.stat_codes[0]);
	SOC_Voltage_Currents.data[4] = highByte(currents.bat);
	SOC_Voltage_Currents.data[5] = lowByte(currents.bat);
	// SOC_Voltage_Currents.data[6] = highByte(currents.solar);
	// SOC_Voltage_Currents.data[7] = lowByte(currents.solar);
	uint16_t solarAux = (m_can.motor1.input_current + m_can.motor2.input_current)*10 + currents.bat;
	SOC_Voltage_Currents.data[6] = highByte(solarAux);
	SOC_Voltage_Currents.data[7] = lowByte(solarAux);

	for(int i = 0, j = 0; i < 4 ; i++, j=j+2)
	{
		cellvoltages0.data[j] = highByte( bms_ic[0].cells.c_codes[i] );
		cellvoltages0.data[j+1] = lowByte( bms_ic[0].cells.c_codes[i] );
		can_cell0.buf[j] = highByte( bms_ic[0].cells.c_codes[i] );
		can_cell0.buf[j+1] = lowByte( bms_ic[0].cells.c_codes[i] );

		cellvoltages1.data[j] = highByte( bms_ic[0].cells.c_codes[i+4] );
		cellvoltages1.data[j+1] = lowByte( bms_ic[0].cells.c_codes[i+4] );
		can_cell1.buf[j] = highByte( bms_ic[0].cells.c_codes[i+4] );
		can_cell1.buf[j+1] = lowByte( bms_ic[0].cells.c_codes[i+4] );

		cellvoltages2.data[j] = highByte( bms_ic[0].cells.c_codes[i+8] );
		cellvoltages2.data[j+1] = lowByte( bms_ic[0].cells.c_codes[i+8] );
		can_cell2.buf[j] = highByte( bms_ic[0].cells.c_codes[i+8] );
		can_cell2.buf[j+1] = lowByte( bms_ic[0].cells.c_codes[i+8] );
	}
	
	SolarPanelVoltages0.data[0] = highByte(solarVoltage[0]);
	SolarPanelVoltages0.data[1] = lowByte(solarVoltage[0]);
	SolarPanelVoltages0.data[2] = highByte(solarVoltage[1]);
	SolarPanelVoltages0.data[3] = lowByte(solarVoltage[1]);
	SolarPanelVoltages0.data[4] = highByte(solarVoltage[2]);
	SolarPanelVoltages0.data[5] = lowByte(solarVoltage[2]);
	SolarPanelVoltages0.data[6] = highByte(solarVoltage[3]);
	SolarPanelVoltages0.data[7] = lowByte(solarVoltage[3]);

	can_solarVoltage0.buf[0] = SolarPanelVoltages0.data[0];
	can_solarVoltage0.buf[1] = SolarPanelVoltages0.data[1];
	can_solarVoltage0.buf[2] = SolarPanelVoltages0.data[2];
	can_solarVoltage0.buf[3] = SolarPanelVoltages0.data[3];
	can_solarVoltage0.buf[4] = SolarPanelVoltages0.data[4];
	can_solarVoltage0.buf[5] = SolarPanelVoltages0.data[5];
	can_solarVoltage0.buf[6] = SolarPanelVoltages0.data[6];
	can_solarVoltage0.buf[7] = SolarPanelVoltages0.data[7]; 

	can_solarVoltage1.buf[0] = SolarPanelVoltages1.data[0];
	can_solarVoltage1.buf[1] = SolarPanelVoltages0.data[1];

	SolarPanelVoltages1.data[0] = highByte(solarVoltage[4]);
	SolarPanelVoltages1.data[1] = lowByte(solarVoltage[4]);

	can_SOC_Volt_Cur.buf[2] = highByte(bms_ic[0].stat.stat_codes[0]);
	can_SOC_Volt_Cur.buf[3] = lowByte(bms_ic[0].stat.stat_codes[0]);
	can_SOC_Volt_Cur.buf[4] = SOC_Voltage_Currents.data[4];
	can_SOC_Volt_Cur.buf[5] = SOC_Voltage_Currents.data[5];
	can_SOC_Volt_Cur.buf[6] = SOC_Voltage_Currents.data[6];
	can_SOC_Volt_Cur.buf[7] = SOC_Voltage_Currents.data[7];

	temperatures0.data[0] = temps.cells[0];
	temperatures0.data[1] = temps.cells[1];
	temperatures0.data[2] = temps.cells[2];
	temperatures0.data[3] = temps.cells[3];
	temperatures0.data[4] = temps.cells[4];
	temperatures0.data[5] = temps.cells[5];
	temperatures0.data[6] = temps.cells[6];
	temperatures0.data[7] = temps.cells[7];

	can_temp0.buf[0] = temps.cells[0];
	can_temp0.buf[1] = temps.cells[1];
	can_temp0.buf[2] = temps.cells[2];
	can_temp0.buf[3] = temps.cells[3];
	can_temp0.buf[4] = temps.cells[4];
	can_temp0.buf[5] = temps.cells[5];
	can_temp0.buf[6] = temps.cells[6];
	can_temp0.buf[7] = temps.cells[7];

	temperatures1.data[0] = temps.cells[8];
	temperatures1.data[1] = temps.cells[9];
	temperatures1.data[2] = temps.cells[10];
	temperatures1.data[3] = temps.cells[11];
	temperatures1.data[4] = temps.internal;

	can_temp1.buf[0] = temps.cells[8];
	can_temp1.buf[1] = temps.cells[9];
	can_temp1.buf[2] = temps.cells[10];
	can_temp1.buf[3] = temps.cells[11];
	can_temp1.buf[4] = temps.internal;
	
	
	temperatures2.data[0] = temps.ambient;
	temperatures2.data[1] = temps.heatshink_1;
	temperatures2.data[2] = temps.heatshink_2;
	temperatures2.data[3] = (uint8_t)round(bms_ic[0].stat.stat_codes[1]*0.01333333-273);

	can_temp2.buf[0] = temps.ambient;
	can_temp2.buf[1] = temps.heatshink_1;
	can_temp2.buf[2] = temps.heatshink_2;
	can_temp2.buf[3] = temperatures2.data[3];


	cellParameters0.data[0] = highByte(OV_THRESHOLD);
	cellParameters0.data[1] = lowByte(OV_THRESHOLD);
	cellParameters0.data[2] = highByte(UV_THRESHOLD);
	cellParameters0.data[3] = lowByte(UV_THRESHOLD);
	cellParameters0.data[4] = highByte(balancingThreshold);
	cellParameters0.data[5] = lowByte(balancingThreshold);
	cellParameters0.data[6] = highByte(imbalance_thershold);
	cellParameters0.data[7] = lowByte(imbalance_thershold);

	cellParameters1.data[0] = highByte(FCV);
	cellParameters1.data[1] = lowByte(FCV);
	cellParameters1.data[2] = highByte(FDV);
	cellParameters1.data[3] = lowByte(FDV);

	securityParameters.data[0] = highByte(DOC);
	securityParameters.data[1] = lowByte(DOC);
	securityParameters.data[2] = highByte(COC);
	securityParameters.data[3] = lowByte(COC);
	securityParameters.data[4] = warning_temp;
	securityParameters.data[5] = balancingAllowed;
	securityParameters.data[6] = highByte(refresh_rate);
	securityParameters.data[7] = lowByte(refresh_rate);	
}

void send_can_messages(){
	Can0.write(status_balanc_warnings);
	Can0.write(can_SOC_Volt_Cur);
	Can0.write(can_cell0);
	Can0.write(can_cell1);
	Can0.write(can_cell2);
	Can0.write(can_solarVoltage0);
	Can0.write(can_solarVoltage1);
	Can0.write(can_temp0);
	Can0.write(can_temp1);
	Can0.write(can_temp2);
}

void send_serial_messages(){
	// In future version only send serial if there is a USB connection
	serial_interface.send_msg(status);
	serial_interface.send_msg(SOC_Voltage_Currents);
	serial_interface.send_msg(cellvoltages0);
	serial_interface.send_msg(cellvoltages1);
	serial_interface.send_msg(cellvoltages2);
	serial_interface.send_msg(SolarPanelVoltages0);
	serial_interface.send_msg(SolarPanelVoltages1);
	serial_interface.send_msg(temperatures0);
	serial_interface.send_msg(temperatures1);
	serial_interface.send_msg(temperatures2);

	serial_interface.send_msg(cellParameters0);
	serial_interface.send_msg(cellParameters1);
	serial_interface.send_msg(securityParameters);
}