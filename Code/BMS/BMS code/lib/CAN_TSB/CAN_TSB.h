/*
    Copyright (C) 2020  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#ifndef CAN_TSB_h
#define CAN_TSB_h

#include "FlexCAN.h"
#include "buffer.h"
#include "ArduinoJson.h"
#include <TimeLib.h>

#define MAXEQ(A,B) (A) = max((A), (B))

class BMS_Data{

    public:
        uint8_t status;
        uint16_t ltc_status;
        uint16_t cells_balanc;
        uint8_t warnings;

        uint16_t soc;
        float voltage;
        float current;
        float solar_current;

        float *cellvoltages;
        float *solar_voltages;
        float *temperatures;
        StaticJsonDocument<1024> toJson();
};

// --------------------------------------------------------

class Foils_Data{

    public:
        float veloc;
        float pitch;
        float roll;
        float gyroX;
        float gyroY;
        float yaw;
        float lat;
        float lon;
        float dist;
        float left_angle;
        float right_angle;
        float back_angle;
        uint8_t height_ref;
        float angle_ref;
        uint8_t state;
        uint8_t mode;

        StaticJsonDocument<512> toJson();
};

// --------------------------------------------------------

class Motor_Data{

    public:
        float motor_current;
        float input_current;
        int32_t rpm;
        float voltage;
        float duty_cycle;
        float temp_motor;
        float temp_mos_max;
        float temp_mos_1;
        float temp_mos_2;
        float temp_mos_3;
        uint8_t fault;
        uint8_t id;
        

        StaticJsonDocument<512> toJson();	
};

// --------------------------------------------------------

class Throttle_Data{
    public:
        int16_t asked_current;
        float pid_speed;

        StaticJsonDocument<256> toJson();
};

// --------------------------------------------------------

class Screen_Data{
    public:
        // Motors related stuff
        uint16_t current_threshold; // Dual motor current threshold
        float pid_speed_setpoint;
        volatile bool pid_state;
        
        // Foils related stuff
        //#ifdef NEMAS_CONTROLLER_H
        volatile bool controller_state;
        volatile bool homing;
        uint8_t dist_setpoint;
        float angle_setpoint;
        uint8_t mode_setpoint;
        volatile bool back_foil_state;
        //#endif

        // SD card related stuff
        // #ifdef  _DATALOGGER_TSB_H_
        volatile bool newFile;
        // #endif

        //BMS related stuff
        volatile bool solarRelay;
};

// --------------------------------------------------------

// Current shunt stuff:

// Struct for IVT_Msg_Result messages
typedef struct{
    bool OCS; // Set if OCS is true
    bool this_result; // Set if result is out of (spec-) range, or has reduced precision or measurement-error
    bool any_result; // Set if any result has a measurement-error
    bool system; // Set if there is a system error, sensor functionality is not ensured.
    int32_t value;
}IVT_S_Result;

class IVT_S_Data{
    public:
        IVT_S_Result current;
        IVT_S_Result voltage1;
        IVT_S_Result voltage2;
        IVT_S_Result voltage3;
        IVT_S_Result temperature;
        IVT_S_Result power;
        IVT_S_Result As;
        IVT_S_Result Wh;  
};


class TSB_CAN : public CANListener 
{
private:
    static time_t getTeensy3Time();
    bool receivedTime = false;
    
public:
    int *filters;
    CAN_message_t message;
    BMS_Data bms;
    Foils_Data foils;
    Motor_Data motor1;
    Motor_Data motor2;
    Throttle_Data throttle;
    Screen_Data screen;
    IVT_S_Data shunt;

    
    unsigned long received_time[7];

    /*
    //array of booleans, for each index=can_node_id, true if a message was receive recently, false case not
    bool valid[7];
    unsigned long can_validate_time;
    bool validate_state;
    */

    TSB_CAN();
    void send_Message(uint32_t Id, uint8_t len, uint8_t *data);
    void receive_Message(CAN_message_t &frame);
    void printFrame(CAN_message_t &frame, int mailbox);
    bool frameHandler(CAN_message_t &frame, int mailbox, uint8_t controller); //overrides the parent version so we can actually do something
    void check_valid_data();
    void reset_data(uint8_t id);
};

#endif
