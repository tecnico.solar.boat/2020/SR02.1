/*
    Copyright (C) 2020  Técnico Solar Boat

    This program is free software: you can redistribute 
    it and/or modify it under the terms of the GNU General Public License 
    as published by the Free Software Foundation, either version 3 of the 
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
    or via our facebook page at https://fb.com/tecnico.solarboat
*/

#include <BMS_EEPROM.h>

unsigned long eeprom_crc( uint16_t lenght ){
  unsigned long crc = ~0L;

  for( unsigned int index = 0 ; index < lenght  ; ++index ){
    crc = crc_table[( crc ^ EEPROM[index] ) & 0x0f] ^ (crc >> 4);
    crc = crc_table[( crc ^ ( EEPROM[index] >> 4 )) & 0x0f] ^ (crc >> 4);
    crc = ~crc;
  }
  return crc;
}


void saveToEEPROM(){
    
    EEPROM.put(BALANCING_ALLOWED_ADDR, balancingAllowed);
    EEPROM.put(BALANCING_THRES_ADDR, balancingThreshold);
    EEPROM.put(IMBALANCE_THRES_ADDR, imbalance_thershold);
    EEPROM.put(OV_THRES_ADDR, OV_THRESHOLD);
    EEPROM.put(UV_THRES_ADDR, UV_THRESHOLD);
    EEPROM.put(FCV_THRES_ADDR, FCV);
    EEPROM.put(FDV_THRES_ADDR, FDV);
    EEPROM.put(WARN_TEMP_ADDR, warning_temp);
    EEPROM.put(DOC_ADDR, DOC);
    EEPROM.put(COC_ADDR, COC);
    EEPROM.put(REFRESH_RATE_ADDR, refresh_rate);
    EEPROM.put(EEPROM_CRC_ADDR, eeprom_crc(EEPROM_CRC_ADDR));
}

void readFromEEPROM(){
    unsigned long crc = ~0L;
    EEPROM.get(EEPROM_CRC_ADDR, crc);
    if ( crc == eeprom_crc(EEPROM_CRC_ADDR) ){
        EEPROM.get(BALANCING_ALLOWED_ADDR, balancingAllowed); 
        EEPROM.get(BALANCING_THRES_ADDR, balancingThreshold); 
        EEPROM.get(IMBALANCE_THRES_ADDR, imbalance_thershold);
        EEPROM.get(OV_THRES_ADDR, OV_THRESHOLD);
        EEPROM.get(UV_THRES_ADDR, UV_THRESHOLD);
        EEPROM.get(FCV_THRES_ADDR, FCV);
        EEPROM.get(FDV_THRES_ADDR, FDV);
        EEPROM.get(WARN_TEMP_ADDR, warning_temp);
        EEPROM.get(DOC_ADDR, DOC);
        EEPROM.get(COC_ADDR, COC);
        EEPROM.get(REFRESH_RATE_ADDR, refresh_rate);
    }
    else{
        setDefaults();
        saveToEEPROM();
    }
}

void setDefaults(){
    balancingAllowed = false;
    balancingThreshold = 35000;
    imbalance_thershold = 100;
    OV_THRESHOLD = 42000; // Over voltage threshold ADC Code. LSB = 0.0001
    UV_THRESHOLD = 31000; // Under voltage threshold ADC Code. LSB = 0.0001
    FCV = 42300;  //Fully Charged Voltage
    FDV = 31000;  //Fully Discharged Voltage
    warning_temp = 55;
    DOC = 3000; // Means 300 A
    COC = 336; // Means 33,6 A
    refresh_rate = 1000; // Unit is ms
}