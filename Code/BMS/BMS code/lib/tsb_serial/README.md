# TSB Serial Library Code

This is the code for the TSB Serial Libray used communicate with the Teensy over USB Serial.

Copyright (C) 2020  Técnico Solar Boat

This repository and its contents  is free software: you can redistribute
it and/or modify it under the terms of the GNU General Public License
as published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.
The content of this repository is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/.
You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
or via our facebook page at https://fb.com/tecnico.solarboat

# Serial TSB Library

Serial Communication library to communicate via the Teensy USB port.


### **Messages format:**
| 1 Byte |  1 Byte   |  1 Byte  | 1 Byte | 8 Bytes |  1 Byte  | 1 Byte |
|:------:|:---------:|:--------:|:------:|:-------:|:--------:|:------:|
|  START | Device ID |  MSG ID  |  LEN   |   DATA  | CHECKSUM |   END  |

- START - 0xAC
- Device ID - Identifies the device that is sending the message
- MSG ID - Messages ID respective to the device **every message has a different ID**
- LEN - Messages size in Bytes (only the DATA part)
- DATA - Particular data specified by the ID.
- Checksum - CHECKSUM - XOR of all bytes (excluding START and END bytes).
- END - 0xAD
 

The DATA field may be bigger but lets limit it to 8 bytes in order for the messages to be compatible with the CAN TSB Library.

#### Device ID

| Device ID (1 Byte) |     Node    |
|:------------------:|:-----------:|
|       0x01         |  Solar BMS  |
|       0x02         |  Battery Cells TestBench      |

### Examplo
```c++
//Serial Object
TSB_SERIAL serial_interface; // This creates the interface to send and recivie messages
tsb_serial_msg_t s_message; // This holds the message, you shoul create one for each message that you what to send


void initSerialMsg(){ // Call this function once in the setup
    s_message.addr = Device ID;
    s_message.data_id = MSG ID;
    s_message.data_size = LEN;
}

void updateSerialMsg(){ // This function updates the data of the message and sends it, you should call this preodically using IntervalTimer
    s_message.data[0] = 0x21; // Fetch data from the source...
    serial_interface.send_msg(s_message);
}

// To recive message put the following code in you loop:
void loop(void)
{
    
    
    if(incomimingMessage.valid_msg == true){ // incomimingMessage is a global tsb_serial_msg_t strucer
        switch (incomimingMessage.data_id)
        {
            case 0x01:
            // Faz cenas
            break;
            
            case 0x02:
            // Faz cenas 2
            break;
        }
        incomimingMessage.valid_msg = false; // Very important, otherwise you will always think there is a new message
    }
}



```

