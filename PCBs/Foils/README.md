# Técnico Solar Boat's Foils PCB

This PCB is used to control the boat's hydrofoils. 

### This PCB was developed for our colleague Basile Belime Master Thesis which is available [here](https://fenix.tecnico.ulisboa.pt/cursos/meec/dissertacao/1409728525633024).

The PCB acquires data from an Senix ultrasonic sensor and a Xsens ARHS and actuates the foil's steepers motors from Nanotec in order to maintain a stable and leveled flight.

Copyright (C) 2020  Técnico Solar Boat

This repository and its contents  is free software: you can redistribute 
it and/or modify it under the terms of the GNU General Public License 
as published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

The content of this repository is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

You can contact Técnico Solar Boat by email at: tecnico.solarboat@gmail.com
or via our facebook page at https://fb.com/tecnico.solarboat